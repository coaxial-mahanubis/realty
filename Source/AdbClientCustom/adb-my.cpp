#include "adbdevicenode.h"

AdbDeviceNode* AdbDeviceNode::newDeviceNode(const QString& serial, const int index)
{
	AdbDeviceNode* node = new AdbDeviceNode;
	node->adb_serial = serial;
	node->node_index = index;
	return node;
}

void AdbDeviceNode::delDeviceNode(AdbDeviceNode* node)
{
	delete node;
}