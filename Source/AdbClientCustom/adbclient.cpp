#include "adbclient.h"

#include <QFile>
#include <QDateTime>

#define htoll(x) (x)
#define ltohl(x) (x)
#define MKID(a,b,c,d) ((a) | ((b) << 8) | ((c) << 16) | ((d) << 24))

#define ID_STAT MKID('S','T','A','T')
#define ID_LIST MKID('L','I','S','T')
#define ID_ULNK MKID('U','L','N','K')
#define ID_SEND MKID('S','E','N','D')
#define ID_RECV MKID('R','E','C','V')
#define ID_DENT MKID('D','E','N','T')
#define ID_DONE MKID('D','O','N','E')
#define ID_DATA MKID('D','A','T','A')
#define ID_OKAY MKID('O','K','A','Y')
#define ID_FAIL MKID('F','A','I','L')
#define ID_QUIT MKID('Q','U','I','T')

typedef union {
    unsigned id;
    struct {
        unsigned id;
        unsigned namelen;
    } req;
    struct {
        unsigned id;
        unsigned mode;
        unsigned size;
        unsigned time;
    } stat;
    struct {
        unsigned id;
        unsigned mode;
        unsigned size;
        unsigned time;
        unsigned namelen;
    } dent;
    struct {
        unsigned id;
        unsigned size;
    } data;
    struct {
        unsigned id;
        unsigned msglen;
    } status;
} syncmsg;

#define SYNC_DATA_MAX 64*1024
typedef struct {
    unsigned id;
    unsigned size;
    char data[SYNC_DATA_MAX];
} syncsendbuf;

AdbClient::AdbClient(const QString &adb_serial, const int port) :
	Logable("AdbClient", "Thread of receiving and data processing")
{
    this->_adb_serial = adb_serial;
    this->_port = port;
    this->_socket = nullptr;
}

AdbClient::~AdbClient() {
    adb_close();
}

bool AdbClient::init_tmpdir() {
    adb_cmd("shell:mkdir /data/local;mkdir /data/local/tmp;chmod 777 /data/local/tmp");

    QByteArray out = adb_cmd("shell:echo > /data/local/tmp/.adb_client_tag; echo TAG_RET=$?");
    if(out.contains("TAG_RET=0")) {
        _adb_tmpdir = "/data/local/tmp";
    } else {
        out = adb_cmd("shell:echo > /data/local/.adb_client_tag; echo TAG_RET=$?");
        if(out.contains("TAG_RET=0")) {
            _adb_tmpdir = "/data/local";
        } else {
			Logger::Log("cannot find valid tmp dir!\n", this);
            return false;
        }
    }
	Logger::Log("found tmpdir "+ _adb_tmpdir.toStdString() + "\n", this);
    return true;
}

bool AdbClient::readx(char *buf, const int len, const int timeout) const
{
    if(_socket != nullptr && _socket->state() == QTcpSocket::ConnectedState) {
        int pos = 0;
	    int retry = 0;
        while(pos < len) {
            _socket->waitForReadyRead(timeout);
	        const int n = _socket->read(buf + pos, len - pos);
            if(n > 0) {
                pos += n;
            } else if(_socket->state() != QTcpSocket::ConnectedState) {
                break;
            } else if(retry++ == 10) {
                break;
            }
        }
        if(retry > 1) {
			Logger::Log("tried " + std::to_string(retry) + " times @ timeout " + std::to_string(timeout) + "\n", this);
        }
        if(pos != len) {
			Logger::Log("read " + std::to_string(pos) + " / " + std::to_string(len) + "\n", this);
            return false;
        }
        return true;
    }
    return false;
}

bool AdbClient::writex(char *buf, const int len) const
{
    if(_socket != nullptr && _socket->state() == QTcpSocket::ConnectedState) {
        int pos = 0;
	    while(pos < len) {
		    const int n = _socket->write(buf + pos, len - pos);
            if(n > 0) {
                pos += n;
            } else {
                break;
            }
            _socket->waitForBytesWritten();
        }
        return pos == len;
    }
    return false;
}

void AdbClient::sync_quit() const
{
    syncmsg msg;
    msg.req.id = ID_QUIT;
    msg.req.namelen = 0;

    writex(reinterpret_cast<char *>(&msg.req), sizeof(msg.req));
}

bool AdbClient::adb_status() {
	char buf[8] = { 0 };
	bool ret = false;
	do {
		if (!readx(buf, 4)) {
			break;
		}

		if (!memcmp(buf, "OKAY", 4)) {
			ret = true;
			break;
		}

		if (memcmp(buf, "FAIL", 4)) {
			break;
		}

		if (!readx(buf, 4)) {
			break;
		}

		buf[4] = 0;
		const unsigned long len = strtoul(buf, nullptr, 16);
		QByteArray err = _socket->read(len);
		_adb_error = QString::fromUtf8(err.data(), err.size());
		Logger::Log("adb_err: '" + QString::fromLocal8Bit(err).toStdString() + "'\n", this);
	} while (false);
	
	return ret;
}

bool AdbClient::switch_socket_transport() {
    QString service;
    if(_adb_serial.isEmpty()) {
        service = "host:transport-any";
    } else {
        service = "host:transport:" + _adb_serial;
    }

    char tmp[8];
	const int len = service.length();
    sprintf(tmp, "%04x", len);

    if(!writex(tmp, 4) || !writex(service.toUtf8().data(), len)) {
        return false;
    }

    return adb_status();
}

bool AdbClient::adb_connect(const QString &service) {
	Logger::Log("adb_connect <" + service.toStdString() + ">\n", this);
	bool ret = false;
	char tmp[8];
	const int len = service.toUtf8().length();
	if (len < 1 || len > 1024) {
		return false;
	}
	sprintf(tmp, "%04x", len);

	adb_close();
	_socket = new QTcpSocket();
	_socket->connectToHost("127.0.0.1", _port);
	if (!_socket->waitForConnected(500)) {
		_socket->abort();
		delete _socket;
		_socket = nullptr;
		_adb_error = "cannot connect to adb host";
		return false;
	}

	do {
		if (!service.startsWith("host") && !switch_socket_transport()) {
			break;
		}

		if (!writex(tmp, 4) || !writex(service.toUtf8().data(), len)) {
			break;
		}

		ret = adb_status();
	} while (false);

	if (!ret) {
		adb_close();
	}
	return ret;
}

void AdbClient::adb_close() {
    if(_socket != nullptr) {
        _socket->disconnectFromHost();
        delete _socket;
        _socket = nullptr;
    }
}

QByteArray AdbClient::adb_query(const QString &service) {
	QByteArray ret;
	char buf[8] = { 0 };

	do {
		if (!adb_connect(service)) {
			break;
		}

		if (!readx(buf, 4)) {
			break;
		}

		buf[4] = 0;
		const unsigned long len = strtoul(buf, nullptr, 16);
		ret = _socket->read(len);
	} while (false);

	Logger::Log("adb_query got: '" + QString::fromLocal8Bit(ret.toHex()).toStdString() + "'\n", this);
	return ret;
}

QByteArray AdbClient::adb_cmd(const QString &cmd, const int timeout) {
	QByteArray ret;
	do {
		if (!adb_connect(cmd)) {
			break;
		}

		while (_socket->waitForReadyRead(timeout)) {
			ret.append(_socket->readAll());
		}
	} while (false);
	Logger::Log("adb_cmd got: '" + QString::fromLocal8Bit(ret).toStdString() + "'\n", this);
	return ret;
}

QByteArray AdbClient::pm_cmd(const QString &cmd, const int timeout) {
	const QString real_cmd = QString("shell:CLASSPATH=/system/framework/pm.jar "
                               "app_process /system/bin com.android.commands.pm.Pm ") + cmd;
    return adb_cmd(real_cmd, timeout);
}

bool AdbClient::adb_install(const QString &lpath, const QString& params) {
    bool ret = false;
    if(_adb_tmpdir.isEmpty() && !init_tmpdir()) {
        return false;
    }

    qsrand(QDateTime::currentDateTime().toTime_t());
	const int ran = qrand() % 0xFFFF;

    QString rpath = QString("%1/%2.apk").arg(_adb_tmpdir).arg(ran);
    if(!adb_push(lpath, rpath, 0644)) {
		Logger::Log("cannot push apk file to '" + rpath.toStdString() + "'!\n", this);
        return false;
    }

    QString cmd = QString("install %1 %2").arg(params, rpath);
    QByteArray out = pm_cmd(cmd);

    if(out.contains("Success")) {
        ret = true;
    } else {
        int l = out.indexOf("Failure [");
        int r = -1;
        if(l != -1) {
            l += 9;
            r = out.indexOf(']', l);
        }
        if(r != -1) {
            _adb_error = out.mid(l, r-l);
        } else {
            _adb_error = "unknown error";
        }
    }
    if(!ret) {
		Logger::Log("_adb_error: '" + _adb_error.toStdString() + "'\n", this);
    }

    cmd = QString("shell:rm %1").arg(rpath);
    adb_cmd(cmd);
    return ret;
}

bool AdbClient::adb_forward(const QString &local, const QString &remote) {
    QString service;
    if(_adb_serial.isEmpty()) {
        service = QString("host:forward:%1;%2").arg(local, remote);
    } else {
        service = QString("host-serial:%1:forward:%2;%3").arg(_adb_serial, local, remote);
    }
    return adb_connect(service) && adb_status();
}

bool AdbClient::adb_push(const QString &lpath, const QString &rpath, const int mode) {
    bool ret = false;
    syncmsg msg;
    syncsendbuf sbuf;
    char buf[512];

	QFile f(lpath);
    if(!f.open(QIODevice::ReadOnly)) {
		Logger::Log("cannot read local file!\n", this);
        return false;
    }

    if(!adb_connect("sync:")) {
        f.close();
        return false;
    }

    do {
        sprintf(buf, "%s,%d", rpath.toUtf8().data(), mode);
        int len = strlen(buf);
        msg.req.id = ID_SEND;
        msg.req.namelen = len;
        if(!writex(reinterpret_cast<char *>(&msg.req), sizeof(msg.req)) || !writex(buf, len)) {
            f.close();
            break;
        }

        ret = true;
        sbuf.id = ID_DATA;
        while((len = f.read(sbuf.data, SYNC_DATA_MAX)) > 0) {
            sbuf.size = len;
            if(!writex(reinterpret_cast<char *>(&sbuf), sizeof(unsigned)*2 + len)) {
                ret = false;
                break;
            }
        }
        f.close();

        msg.data.id = ID_DONE;
        msg.data.size = QDateTime::currentDateTime().toTime_t();
        if(!writex(reinterpret_cast<char *>(&msg.data), sizeof(msg.data))) {
            ret = false;
            break;
        }

        if(!readx(reinterpret_cast<char *>(&msg.status), sizeof(msg.status))) {
            break;
        }

        if(msg.status.id != ID_OKAY) {
            ret = false;
            if(msg.status.id == ID_FAIL) {
                len = msg.status.msglen;
                QByteArray err = _socket->read(len);
                _adb_error = QString::fromUtf8(err.data(), err.size());
            } else {
                _adb_error = "unknown error";
            }
        }
    } while(false);

    if(!ret) {
		Logger::Log("_adb_error: '" + _adb_error.toStdString() + "'\n", this);
    }

    sync_quit();
	Logger::Log("push done\n", this);
    return ret;
}

bool AdbClient::adb_pull(const QString &rpath, const QString &lpath) {
    bool ret = false;
    syncmsg msg;
    syncsendbuf sbuf;
    int len;
    unsigned id = ID_RECV;

    QFile f(lpath);
    f.remove();
    if(!f.open(QIODevice::WriteOnly)) {
		Logger::Log("cannot write local file!\n", this);
        return false;
    }

    if(!adb_connect("sync:")) {
        f.close();
        return false;
    }

    do {
        msg.req.id = ID_RECV;
        msg.req.namelen = rpath.toUtf8().size();
        if(!writex(reinterpret_cast<char *>(&msg.req), sizeof(msg.req)) || !writex(rpath.toUtf8().data(), msg.req.namelen)) {
            break;
        }

        for(;;) {
            if(!readx(reinterpret_cast<char *>(&msg.data), sizeof(msg.data))) {
                break;
            }
            id = msg.data.id;
            len = msg.data.size;

            if(id == ID_DONE) {
                f.close();
                break;
            }
            if(id != ID_DATA) {
                f.close();
                f.remove();
                break;
            }

            if(!readx(sbuf.data, len)) {
                f.close();
                break;
            }

            if(f.write(sbuf.data, len) != len) {
                f.close();
                break;
            }
        }
        ret = id == ID_DONE;
    } while(false);

    if(id == ID_FAIL) {
        len = msg.data.size;
        QByteArray err = _socket->read(len);
        _adb_error = QString::fromUtf8(err.data(), err.size());
    } else if(!ret) {
        _adb_error = "unknown error";
    }

    if(!ret) {
		Logger::Log("_adb_error: '" + _adb_error.toStdString() + "'\n", this);
    }

    sync_quit();
	Logger::Log("pull done\n", this);
    return ret;
}

bool AdbClient::adb_pushData(unsigned char *data, const int size, const QString &rpath, const int mode) {
    bool ret = false;
    syncmsg msg;
    syncsendbuf sbuf;
    char buf[512];
    int pos = 0;
    int left = size;

	if(!adb_connect("sync:")) {
        return false;
    }

    do {
        sprintf(buf, "%s,%d", rpath.toUtf8().data(), mode);
        int len = strlen(buf);
        msg.req.id = ID_SEND;
        msg.req.namelen = len;
        if(!writex(reinterpret_cast<char *>(&msg.req), sizeof(msg.req)) || !writex(buf, len)) {
            break;
        }

        ret = true;
        sbuf.id = ID_DATA;
        while(left > 0) {
            len = left > SYNC_DATA_MAX ? SYNC_DATA_MAX : left;
            memcpy(sbuf.data, data + pos, len);
            pos += len;
            left -= len;

            sbuf.size = len;
            if(!writex(reinterpret_cast<char *>(&sbuf), sizeof(unsigned)*2 + len)) {
                ret = false;
                break;
            }
        }

        msg.data.id = ID_DONE;
        msg.data.size = QDateTime::currentDateTime().toTime_t();
        if(!writex(reinterpret_cast<char *>(&msg.data), sizeof(msg.data))) {
            ret = false;
            break;
        }

        if(!readx(reinterpret_cast<char *>(&msg.status), sizeof(msg.status))) {
            break;
        }

        if(msg.status.id != ID_OKAY) {
            ret = false;
            if(msg.status.id == ID_FAIL) {
                len = msg.status.msglen;
                QByteArray err = _socket->read(len);
                _adb_error = QString::fromUtf8(err.data(), err.size());
            } else {
                _adb_error = "unknown error";
            }
        }
    } while(false);

    if(!ret) {
		Logger::Log("_adb_error: '" + _adb_error.toStdString() + "'\n", this);
    }

    sync_quit();
	Logger::Log("pushData done\n", this);
    return ret;
}

bool AdbClient::adb_pullData(const QString &rpath, QByteArray &dest) {
    bool ret = false;
    syncmsg msg;
    syncsendbuf sbuf;
    int len;
    unsigned id = ID_RECV;

    if(!adb_connect("sync:")) {
        return false;
    }

    do {
        msg.req.id = ID_RECV;
        msg.req.namelen = rpath.toUtf8().size();
        if(!writex(reinterpret_cast<char *>(&msg.req), sizeof(msg.req)) || !writex(rpath.toUtf8().data(), msg.req.namelen)) {
            break;
        }

        for(;;) {
            if(!readx(reinterpret_cast<char *>(&msg.data), sizeof(msg.data))) {
                break;
            }
            id = msg.data.id;
            len = msg.data.size;

            if(id == ID_DONE) {
                break;
            }
            if(id != ID_DATA) {
                break;
            }

            if(!readx(sbuf.data, len)) {
                break;
            }

            dest.append(static_cast<char *>(sbuf.data), len);
        }
        ret = id == ID_DONE;
    } while(false);

    if(id == ID_FAIL) {
        len = msg.data.size;
        QByteArray err = _socket->read(len);
        _adb_error = QString::fromUtf8(err.data(), err.size());
    } else if(!ret) {
        _adb_error = "unknown error";
    }

    if(!ret) {
		Logger::Log("_adb_error: '"+ _adb_error.toStdString() +"'\n", this);
    }

    sync_quit();
	Logger::Log("pullData done\n", this);
    return ret;
}

#define DDMS_RAWIMAGE_VERSION 1
typedef struct FRAMEBUFFER_HEAD {
    unsigned int version;
    unsigned int bpp;
    unsigned int size;
    unsigned int width;
    unsigned int height;
    unsigned int red_offset;
    unsigned int red_length;
    unsigned int blue_offset;
    unsigned int blue_length;
    unsigned int green_offset;
    unsigned int green_length;
    unsigned int alpha_offset;
    unsigned int alpha_length;
} FRAMEBUFFER_HEAD;

typedef struct ARGB32 {
    unsigned char b;
    unsigned char g;
    unsigned char r;
    unsigned char a;
} ARGB32;

static void convert_ARGB32(char *data, const int pixels, int ao, const int ro, const int go, const int bo) {
    ARGB32 ret;
    int src;

    int i = 0;
    int pos = 0;
    while(i++ < pixels) {
        memcpy(&src, data + pos, 4);
        ret.a = 0xff;
        ret.r = (src >> ro) & 0xff;
        ret.g = (src >> go) & 0xff;
        ret.b = (src >> bo) & 0xff;
        memcpy(data + pos, &ret, 4);

        pos += 4;
    }
}

#ifndef NO_GUI
QImage AdbClient::adb_screencap() {
    QImage ret;
    QImage::Format fmt = QImage::Format_Invalid;
    FRAMEBUFFER_HEAD head;

    do {
        if(!adb_connect("framebuffer:")) {
            break;
        }

        if(!readx(reinterpret_cast<char *>(&head), sizeof(head), 5000)) {
            break;
        }

        Logger::Log("version = "+std::to_string(head.version)+"\n", this);
		Logger::Log("A R G B = " + std::to_string(head.alpha_offset) + " " + std::to_string(head.red_offset)
			+ " " + std::to_string(head.green_offset) + " " + std::to_string(head.blue_offset) + "\n", this);
		Logger::Log("          " + std::to_string(head.alpha_length) + " " + std::to_string(head.red_length)
			+ " " + std::to_string(head.green_length) + " " + std::to_string(head.blue_length) + "\n", this);
		Logger::Log("width = " + std::to_string(head.width) + ", height = " + std::to_string(head.height) + "\n", this);
		Logger::Log("bpp = " + std::to_string(head.bpp) + ", size = " + std::to_string(head.size) + "\n", this);

        char *buf = static_cast<char *>(malloc(head.size));
        if(!readx(buf, head.size, 5000)) {
            free(buf);
            break;
        }

        if(head.bpp == 16) {
            fmt = QImage::Format_RGB16;
        } else if(head.bpp == 32) {
            fmt = QImage::Format_ARGB32;
            convert_ARGB32(buf, head.width*head.height,
                           head.alpha_offset,
                           head.red_offset,
                           head.green_offset,
                           head.blue_offset);
        }

        ret = QImage(reinterpret_cast<uchar *>(buf), head.width, head.height, fmt).copy();
        free(buf);
    } while(false);
    return ret;
}
#endif
