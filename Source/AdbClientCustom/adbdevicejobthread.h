#ifndef ADBDEVICEJOBTHREAD_H
#define ADBDEVICEJOBTHREAD_H

#include <QThread>
#include <logger.h>

class AdbDeviceNode;
class AdbDeviceJobThread : public QThread, Logable
{
    Q_OBJECT
public:
    AdbDeviceNode *node;
    bool needsQuit;

    AdbDeviceJobThread(AdbDeviceNode *node, QObject *parent = nullptr);
    ~AdbDeviceJobThread();
    void run() override;

    inline void stop() {
        needsQuit = true;
    }

    virtual void core_run() = 0;
signals:
    void sigDeviceRefresh(AdbDeviceNode *node);
    void sigDeviceStat(AdbDeviceNode *node, const QString& status);
    void sigDeviceProgress(AdbDeviceNode *node, int progress, int max);
    void sigDeviceLog(AdbDeviceNode *node, const QString& log);
};

#endif // ADBDEVICEJOBTHREAD_H
