#include "adbtracker.h"
#include <QTcpSocket>
#include "adbdevicenode.h"
#include "adbprocess.h"

#define ADB_DEFAULT_PORT 5037

#ifdef Q_OS_WIN
#include <WinSock2.h>
#include <IPHlpApi.h>
#include <TlHelp32.h>
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "iphlpapi.lib")
#else
#include <sys/types.h>
#include <signal.h>
#include <QFile>
#endif

static bool writex(QTcpSocket *socket, const char *str) {
    int n;
	const int len = strlen(str);
    char tmp[8];
    sprintf(tmp, "%04X", len);

    if((n = socket->write(tmp, 4)) != 4) {
        return false;
    }

    if((n = socket->write(str, len)) != len) {
        return false;
    }
    socket->waitForBytesWritten();

    return true;
}

static QByteArray readx(QTcpSocket *socket, const int len) {
    socket->waitForReadyRead(200);
    return socket->read(len);
}

AdbNurse::AdbNurse(AdbTracker *parent, const int pid)
    : QThread(nullptr), Logable("AdbNurse", "Thread of AdbNurse")
{
    needsQuit = false;
    this->parent = parent;
    this->adbPid = pid;
}

void AdbNurse::adb_hanged() const
{
    Logger::Log("### adbHang ###\n", this);
    parent->adbHanged = true;
    if(adbPid != -1) {
		Logger::Log("try kill the adb pid " + std::to_string(adbPid) + "\n", this);
#ifdef Q_OS_WIN
	    const HANDLE h = OpenProcess(PROCESS_TERMINATE, FALSE, adbPid);
        if(h != nullptr) {
            TerminateProcess(h, 0);
        }
#else
        kill(adbPid, SIGKILL);
#endif
    }
}

void AdbNurse::run() {
    QTcpSocket socket;
    QByteArray tmp;

    while(!needsQuit) {
        socket.connectToHost("127.0.0.1", ADB_DEFAULT_PORT);
        if(!socket.waitForConnected(500)) {
            socket.abort();
            Logger::Log("connect adb host fail\n", this);
            adb_hanged();
            break;
        }

        if(!writex(&socket, "host:version") || readx(&socket, 4) != "OKAY") {
            Logger::Log("read host:version resp fail!\n", this);
            adb_hanged();
            break;
        }

        socket.waitForReadyRead(200);
        if((tmp = socket.read(4)).size() != 4) {
            Logger::Log("read version len fail!\n", this);
            adb_hanged();
            break;
        }

        tmp = socket.read(tmp.toInt());
        socket.disconnectFromHost();

        parent->adbHanged = false;

        QThread::msleep(500);
    }
    socket.abort();
	Logger::Log("adbNurse quit\n", this);
}

AdbTracker::AdbTracker(const QString& adb_exe, QObject *parent) :
    QThread(parent), Logable("AdbTracker", "Thread of AdbTracker")
{
	Logger::Log("AdbTracker +" + std::to_string(unsigned long long(this)) + "\n", this);
    this->adb_exe = adb_exe;
    needsQuit = false;
    adbHanged = false;
}

AdbTracker::~AdbTracker() {
	Logger::Log("AdbTracker ~" + std::to_string(unsigned long long(this)) + "\n", this);
}

void AdbTracker::refreshDeviceList(const QByteArray &data) {
    QStringList lines = QString::fromUtf8(data.data(), data.size()).split('\n');
    QList<AdbDeviceNode *> lostDevices;

    deviceList_mutex.lock();
    lostDevices.append(deviceList);

    foreach(const QString& line, lines) {
		const int n = line.indexOf('\t');
        if(n == -1) {
            continue;
        }
        QString serial = line.left(n);
        QString cstate = line.mid(n+1);

        bool found = false;
        foreach(AdbDeviceNode *node, lostDevices) {
            if(node->adb_serial == serial) {
                found = true;
                lostDevices.removeOne(node); // remove from lost list
                node->connect_statname = cstate;
                node->connect_stat = AdbDeviceNode::getStateFromName(cstate); // update new state
                Logger::Log("new state: '"+serial.toStdString()+"'->'"+cstate.toStdString()+"'\n", this);
                break;
            }
        }

        if(!found && !serial.startsWith("usb_")
                && !serial.endsWith("_mdb")) {
			Logger::Log("new device: '" + serial.toStdString() + "'->'" + cstate.toStdString() + "'\n", this);
	        const int index = deviceList.size() + 1;
            AdbDeviceNode *node = AdbDeviceNode::newDeviceNode(serial, index);
            node->connect_statname = cstate;
            node->connect_stat = AdbDeviceNode::getStateFromName(cstate);
            deviceList.append(node);
        }
    }

    foreach(AdbDeviceNode *node, lostDevices) {
        node->connect_statname = "disconnected";
        node->connect_stat = AdbDeviceNode::CS_DISCONNECTED;
		Logger::Log("lost device: '"+node->adb_serial.toStdString()+"'\n", this);
    }

    deviceList_mutex.unlock();
}

#ifndef Q_OS_WIN
static QByteArray getNetstatArg(const QByteArray& line, int pos) {
    if(pos == -1) {
        return QByteArray();
    }
    int r = line.indexOf(' ', pos);
    return line.mid(pos, r == -1 ? -1 : r-pos);
}
#endif

int AdbTracker::getAdbPid(QString &processName) {
    int ret = -1;
#ifdef Q_OS_WIN
    PMIB_TCPTABLE_OWNER_PID pTcpTable = nullptr;
    unsigned long dwSize = 0;
    unsigned long dwRetVal;

	if((dwRetVal = GetExtendedTcpTable(nullptr, &dwSize, FALSE, AF_INET, TCP_TABLE_OWNER_PID_LISTENER, 0)!=NO_ERROR)) {
        pTcpTable = static_cast<PMIB_TCPTABLE_OWNER_PID>(malloc(dwSize));
        if(pTcpTable == nullptr) {
            Logger::Log("Error malloc!\n");
            return -1;
        }
    }

    if ((dwRetVal = GetExtendedTcpTable(pTcpTable, &dwSize, FALSE, AF_INET, TCP_TABLE_OWNER_PID_LISTENER, 0)) == NO_ERROR) {
        for (int i = 0; i < static_cast<int>(pTcpTable->dwNumEntries); i++) {
            if(ntohs(pTcpTable->table[i].dwLocalPort)!=ADB_DEFAULT_PORT) {
                continue;
            }

            if(pTcpTable->table[i].dwState != MIB_TCP_STATE_LISTEN) {
                continue;
            }

            ret = pTcpTable->table[i].dwOwningPid;
            break;
        }
    } else {
		Logger::Log("\tGetExtendedTcpTable failed with " + std::to_string(dwRetVal) + "\n");
    }

    if(pTcpTable!= nullptr) {
        free(pTcpTable);
    }

    if(ret != -1) {
        const LPMODULEENTRY32 module_entry = static_cast<LPMODULEENTRY32>(malloc(sizeof(MODULEENTRY32)));
        const PPROCESSENTRY32 process_entry = static_cast<PPROCESSENTRY32>(malloc(sizeof(PROCESSENTRY32)));
        bool found = false;
        process_entry->dwSize = sizeof(PROCESSENTRY32);

        HANDLE snap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, ret);
        if(snap) {
            if(Module32First(snap, module_entry)) {
                do {
                    if(module_entry->th32ProcessID == ret) {
						std::wstring wdata(module_entry->szExePath, module_entry->szExePath + 260);
	                    const std::string data(wdata.begin(), wdata.end());
						Logger::Log("got module, name="+data+"\n");
                        processName = QString::fromStdString(data);
                        found = true;
                        break;
                    }
                } while(Module32Next(snap, module_entry));
            }
            CloseHandle(snap);
        } else {
            Logger::Log("TH32CS_SNAPMODULE failed\n");
        }

        if(!found) {
            snap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, ret);
            if(snap) {
                if(Process32First(snap, process_entry)) {
                    do {
                        if(process_entry->th32ProcessID == ret) {
							std::wstring wdata(module_entry->szExePath, module_entry->szExePath + 260);
							const std::string data(wdata.begin(), wdata.end());
							Logger::Log("got process");
							//Logger::Log("got process, name=" + data + "\n");
                            processName = QString::fromStdString(data);
                            found = true;
                            break;
                        }
                    } while(Process32Next(snap, process_entry));
                }
                CloseHandle(snap);
            } else {
				Logger::Log("TH32CS_SNAPPROCESS failed\n");
            }
        }

        free(module_entry);
        free(process_entry);
    }
#else
    AdbProcess p;
    QByteArray outstd;
    QByteArray outerr;
    p.exec_cmd("netstat -tlnp", outstd, outerr);
    QList<QByteArray> lines  = outstd.split('\n');

    bool headParsed = false;
    int pos_laddr = -1;
    int pos_state = -1;
    int pos_pid = -1;
    foreach(const QByteArray& line, lines) {
        if(line.isEmpty()) {
            continue;
        }

        if(!headParsed) {
            if(line.startsWith("Proto ")) {
                pos_laddr = line.indexOf("Local Address ");
                pos_state = line.indexOf("State ");
                pos_pid = line.indexOf("PID/Program name");
                headParsed = true;
            }
            continue;
        }

        QByteArray laddr = getNetstatArg(line, pos_laddr);
        QByteArray state = getNetstatArg(line, pos_state);
        QByteArray progname = getNetstatArg(line, pos_pid);
        if(state == "LISTEN" && laddr.endsWith(QString(":%1").arg(ADB_DEFAULT_PORT).toLocal8Bit())) {
            DBG("found target: [%s]\n", progname.data());
            if((pos_pid = progname.indexOf('/')) != -1) {
                ret = progname.left(pos_pid).toInt();
            }
            break;
        }
    }

#endif
	Logger::Log("got adb pid ["+std::to_string(ret)+"]\n");
    return ret;
}

void AdbTracker::run() {
	core_run();
    emit sigDone(this);
}

void AdbTracker::core_run() {
    QTcpSocket socket;
    char tmp[8];
    int n, len;

    int adb_pid;
    QString adbPath;
    bool our_adb = false;

    QProcess::execute(adb_exe + " kill-server");
    while(!needsQuit) {
        our_adb = false;
        socket.connectToHost("127.0.0.1", ADB_DEFAULT_PORT);
        if(!socket.waitForConnected(500)) {
            socket.abort();
            our_adb = true;
			Logger::Log("start our_adb = true\n", this);

            bool success = false;
            for(int retry=0; retry<5; retry++) {
				Logger::Log("retry " + std::to_string(retry + 1) + "\n", this);
                QProcess::execute(adb_exe + " start-server");

                socket.connectToHost("127.0.0.1", ADB_DEFAULT_PORT);
                if(!socket.waitForConnected(500)) {
                    socket.abort();
                    msleep(500);

                    adb_pid = getAdbPid(adbPath);
                    if(adb_pid != -1) {
#ifdef _WIN32
	                    const HANDLE p = OpenProcess(SYNCHRONIZE|PROCESS_TERMINATE, FALSE, adb_pid);
                        if(p != nullptr) TerminateProcess(p, 0);
#else
                        QProcess::execute(QString("kill %1").arg(adb_pid));
#endif
                    }
                    continue;
                }
                success = true;
                break;
            }

            if(!success) {
                emit sigAdbError(tr("connect adb host fail!"), -2);
                return;
            }
        }

        adbPath = tr("unknown");
        adb_pid = getAdbPid(adbPath);
        if(!writex(&socket, "host:track-devices") || readx(&socket, 4) != "OKAY") {
            emit sigAdbError(tr("adb track-devices fail!")
                             + tr("\nConflict process name: %1").arg(adbPath), adb_pid);
            return;
        }

        AdbNurse t(this, adb_pid);
        t.start();

		Logger::Log("start tracking...\n", this);
        while(!needsQuit) {
            socket.waitForReadyRead(200);
            if((n = socket.read(tmp, 4)) != 4) {
                if(n < 0 || adbHanged) {
                    break;
                }
                continue;
            }

            sscanf(tmp, "%04X", &len);
			Logger::Log("got len "+std::to_string(len)+"\n", this);
	        const QByteArray buf = socket.read(len);
            refreshDeviceList(buf);
            emit sigDevListChanged();
        }
        socket.disconnectFromHost();
        Logger::Log("track finished.\n", this);

        foreach(AdbDeviceNode *node, deviceList) {
            node->connect_statname = "disconnected";
            node->connect_stat = AdbDeviceNode::CS_DISCONNECTED;
        }
        emit sigDevListChanged();
        t.stop();
        t.wait();
    }

    if(our_adb) {
        Logger::Log("send kill-server to our adb\n", this);
        QProcess::execute(adb_exe + " kill-server");
    }
}

void AdbTracker::cleanUp() {
	Logger::Log("start cleanUp...\n", this);
    int new_index = 1;
    deviceList_mutex.lock();
    for(int i=0; i<deviceList.size(); i++) {
        AdbDeviceNode *node = deviceList[i];
        if(node->connect_stat == AdbDeviceNode::CS_DISCONNECTED) {
			Logger::Log("cleaned " + std::to_string(unsigned long long(node)) + "\n", this);
            deviceList.removeAt(i--);
            delList.append(node);
        } else {
            node->node_index = new_index++;
        }
    }
    deviceList_mutex.unlock();

    for(int i=0; i<delList.size(); i++) {
        AdbDeviceNode *node = delList[i];
        if(node->jobThread == nullptr) {
			Logger::Log("real free " + std::to_string(unsigned long long(node)) + "\n", this);
            AdbDeviceNode::delDeviceNode(delList.takeAt(i--));
        }
    }
	Logger::Log("end cleanUp...\n", this);
    emit sigDevListChanged();
}

void AdbTracker::freeMem() {
    while(!deviceList.isEmpty()) {
        delete deviceList.takeFirst();
    }

    while(!delList.isEmpty()) {
        delete delList.takeFirst();
    }
}

int AdbTracker::getActiveCount() {
    int count = 0;
    deviceList_mutex.lock();
    foreach(AdbDeviceNode *n, deviceList) {
        if(n->connect_stat == AdbDeviceNode::CS_DEVICE) {
            count ++;
        }
    }
    deviceList_mutex.unlock();
    return count;
}

QList<AdbDeviceNode *> AdbTracker::getDeviceList() {
    QList<AdbDeviceNode *> list;
    deviceList_mutex.lock();
    list.append(deviceList);
    deviceList_mutex.unlock();
    return list;
}

