#include "logger.h"

#include <chrono>
#include <iomanip>
#include <utility>
#include <filesystem>
#include <set>

std::unique_ptr<Logger> Logger::logger_;
const char* Logger::kLogFileName = "Log from ";
const char* kDirectoryName = "Logs";
std::mutex Logger::logger_creation_mutex_;


const LogMessagesContext& Logable::GetContext() const
{
	return context_;
}

Logable::Logable(std::string class_name, std::string thread_name)
{
	context_.class_name = std::move(class_name);
	context_.thread_name = std::move(thread_name);
}


Logger::Logger() :
	log_file_(std::string(kDirectoryName) + "/" + std::string(kLogFileName) + CurrentDate() + ".txt", std::ios::app)
{
	auto start_session_message = "\n\nSession started at " + CurrentDate() + ":\n";
	log_file_.write(start_session_message.c_str(), start_session_message.size());
	log_file_.flush();
}

Logger::~Logger()
{
	auto end_session_message = "Session ended at " + CurrentDate() + ".\n";
	log_file_.write(end_session_message.c_str(), end_session_message.size());
	log_file_.close();
}

void Logger::Log(const std::string& message, const Logable* logable)
{
	TryCreateSingleton();

	if (logable == nullptr)
	{
		logger_->LogMessage(message);
	}
	else
	{
		logger_->LogMessage(logable, message);
	}
}

using namespace std::experimental::filesystem;

struct FileDate
{
	path file_path;
	time_t date;

	friend bool operator<(const FileDate& lhs, const FileDate& rhs)
	{
		return lhs.date < rhs.date;
	}
};

void Logger::ClearLogsFolder(uint32_t save_files_count)
{
	std::set<FileDate> file_dates;

	for (const auto& entry : directory_iterator(kDirectoryName))
	{
		auto name = entry.path().filename().string();

		std::string day{name.data() + 10, 2};
		std::string month{name.data() + 13, 2};
		std::string year{name.data() + 16, 4};
		std::string hour{name.data() + 22, 2};
		std::string min{name.data() + 25, 2};
		std::string sec{name.data() + 28, 2};

		tm file_date
		{
			atoi(sec.c_str()),
			atoi(min.c_str()),
			atoi(hour.c_str()),
			atoi(day.c_str()),
			atoi(month.c_str()),
			atoi(year.c_str()) - 1900,
		};

		file_dates.insert({entry.path(), mktime(&file_date)});
	}

	if (file_dates.size() <= save_files_count)
		return;

	const auto remove_to = std::prev(std::end(file_dates), save_files_count);
	std::for_each(std::begin(file_dates), remove_to, [](const FileDate& file_date)
	              {
		              std::experimental::filesystem::remove(file_date.file_path);
	              });
}

void Logger::TryCreateSingleton()
{
	logger_creation_mutex_.lock();
	if (logger_ == nullptr)
	{
		CreateFolderIfNeeded();
		logger_.reset(new Logger);
	}
	logger_creation_mutex_.unlock();
}

void Logger::LogMessage(const Logable* logable, const std::string& message)
{
	const auto current_time = CurrentDate();
	const auto context = "Class [" + logable->GetContext().class_name +
		"] in thread [" + logable->GetContext().thread_name + "]: ";
	const auto result = ('\t' + current_time + " | " + context + message + '\n');

	WriteString(result);
}

void Logger::LogMessage(const std::string& message)
{
	const auto current_time = CurrentDate();
	const auto result = ('\t' + current_time + " | " + message + '\n');

	WriteString(result);
}

void Logger::WriteString(const std::string& s)
{
	log_file_mutex_.lock();

	log_file_.write(s.c_str(), s.size());
	log_file_.flush();

	log_file_mutex_.unlock();
}

std::string Logger::CurrentDate()
{
	auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

	tm timeinfo;
	localtime_s(&timeinfo, &now);

	char buf[100];
	std::strftime(buf, sizeof(buf), "[%d_%m_%Y  %H-%M-%S]", &timeinfo);
	return buf;
}

void Logger::CreateFolderIfNeeded()
{
	create_directory(kDirectoryName);
}
