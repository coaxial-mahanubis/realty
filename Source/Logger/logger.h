#pragma once
#include <fstream>
#include <memory>
#include <mutex>

struct LogMessagesContext
{
	std::string class_name;
	std::string thread_name;
};

class Logable
{
public:
	const LogMessagesContext& GetContext() const;
	Logable(std::string class_name, std::string thread_name);
private:
	LogMessagesContext context_;
};

class Logger
{
	Logger();
	Logger(Logger&) = delete;
	Logger& operator=(Logger&) = delete;


public:
	~Logger();

	static void Log(const std::string& message, const Logable* logable = nullptr);
	static void ClearLogsFolder(uint32_t save_files_count = 10);

private:
	static const char* kLogFileName;
	static std::unique_ptr<Logger> logger_; // Singleton
	static std::mutex logger_creation_mutex_;

	std::mutex log_file_mutex_;
	std::ofstream log_file_;

	static void TryCreateSingleton();
	static std::string CurrentDate();
	static void CreateFolderIfNeeded();
	
	void LogMessage(const Logable* logable, const std::string& message);
	void LogMessage(const std::string& message);
	void WriteString(const std::string& s);
};