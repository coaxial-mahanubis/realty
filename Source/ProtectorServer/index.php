<?php
/*
 * @brief Скрипт протектор.
 * Помещает в БД записи об использовании программы: id пользователя, количество полученных объявлений, текущие дату и время.
 * Сверяет наличие пользователя и версии программы в чёрном списке.
 * Помогает в анализе статистики.
 * Дополнительная информация:
 *      Классы в php: http://php.net/manual/ru/language.oop5.php
 *      PDO: http://pl1.php.net/manual/ru/book.pdo.php
 * Авторы: Андрей Аргунов и Фёдор Кирилленко
 * @param action Действие, которое должен выполнить скрипт: Check, CreateUser, CreateVersion, AddReport.
 * @param user_id id пользователя.
 * @param version Версия программы пользователя.
 * @param ads_number Количество полученных пользователем объявлений.
*/

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

/*
* @brief Класс для обращений к БД.
*/

class DataBase
{
    static private $db_name_ = 'id6439813_realtorprotector';
    static private $host_ = 'localhost';
    static private $login_ = 'id6439813_youngrealtor';
    static private $password_ = '112358';
    static private $DB_;

    /**
     * DataBase constructor.
     */
    public function __construct()
    {
        DataBase::$DB_ = new PDO('mysql:host=' . DataBase::$host_ . ';dbname=' . DataBase::$db_name_ . ';charset=utf8', DataBase::$login_, DataBase::$password_, NULL);
        DataBase::$DB_->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @brief Проверяет, существует ли в БД запись о пользователе.
     * @param int Id пользователя
     * @return bool True, если пользвоатель есть в базе, false - если отсутствует.
     */
    public function ExistUser($user_id)
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT EXISTS(SELECT 1 FROM users WHERE id =:id LIMIT 1)');
            $statement->bindValue(':id', $user_id, PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_NUM);
            if ($result[0]) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Получает пользователя из БД.
     * @param int Id пользователя
     * @return User Объект пользователя.
     */
    public function SelectUser($user_id)
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT * FROM users WHERE id =:id LIMIT 1');
            $statement->bindValue(':id', $user_id, PDO::PARAM_INT);
            $statement->execute();

            $result = $statement->fetch();

            if (count($result)) {
                $user = new User($result['id'], $result['state'], $result['info'], $result['license_key'], $result['paid_to']);
                return $user;
            } else {
                return new User(0, 'block', "", "", 0);
            }
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Получает инфу о всех пользователях из БД.
     * @return array of User Массив пользователей.
     */
    public function SelectAllUsers()
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT * FROM users');
            $statement->execute();

            $result = $statement->fetchAll();

            $listUsers = array();
            foreach ($result as $user) {
                array_push($listUsers, new User($user['id'], $user['state'], $user['info'], $user['license_key'], $user['paid_to']));
            }
            return $listUsers;

        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }


    /**
     * @brief Обновляет информацию о пользователе в БД.
     * @param User Объект пользователя.
     */
    public function UpdateUser($user)
    {
        try {
            $statement = DataBase::$DB_->prepare('UPDATE `users` SET `state` = :state, `paid_to` = :paid_to WHERE `id` = :id');
            $statement->bindValue(':id', $user->GetId(), PDO::PARAM_INT);
            $statement->bindValue(':state', $user->GetState(), PDO::PARAM_STR);
            $statement->bindValue(':paid_to', $user->GetPaidTo(), PDO::PARAM_STR);
            $statement->execute();
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Добавляет информацию о пользователе в БД.
     * @param User Объект пользователя.
     * @return int Id нового пользователя.
     */
    public function InsertUser($user)
    {
        try {
            $statement = DataBase::$DB_->prepare('INSERT INTO `users` (`state`, `info`, `license_key`) VALUES(:state, :info, :license_key);');
            $statement->bindValue(':state', $user->GetState(), PDO::PARAM_STR);
            $statement->bindValue(':info', $user->GetInfo(), PDO::PARAM_STR);
            $statement->bindValue(':license_key', $user->GetLicenseKey(), PDO::PARAM_STR);
            $statement->execute();
            return DataBase::$DB_->lastInsertId();
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Проверяет, существует ли в БД запись о версии программы.
     * @param int Номер версии.
     * @return bool True, если версия есть в базе, false - если отсутствует.
     */
    public function ExistVersion($number_version)
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT EXISTS(SELECT 1 FROM versions WHERE number_version =:number_version LIMIT 1)');
            $statement->bindValue(':number_version', $number_version, PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_NUM);
            if ($result[0]) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Получает версию программы из БД.
     * @param int Номер версии.
     * @return Version Объект версии.
     */
    public function SelectVersion($number_version)
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT * FROM versions WHERE number_version =:number_version LIMIT 1');
            $statement->bindValue(':number_version', $number_version, PDO::PARAM_INT);
            $statement->execute();

            $result = $statement->fetch();

            if (count($result)) {
                $version = new Version($result['number_version'], $result['active']);
                return $version;
            } else {
                return new Version;
            }
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Обновляет информацию о версии программы в БД.
     * @param Version Объект версии.
     */
    public function UpdateVersion($version)
    {
        try {
            $statement = DataBase::$DB_->prepare('UPDATE versions SET active = :active WHERE number_version = :number_version');
            $statement->bindValue(':number_version', $version->GetNumberVersion(), PDO::PARAM_INT);
            $statement->bindValue(':active', $version->IsActive(), PDO::PARAM_BOOL);
            $statement->execute();
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Добавляет информацию о версии программы в БД.
     * @param Version Объект версии.
     * @return int Номер новой версии.
     */
    public function InsertVersion($version)
    {
        try {
            $statement = DataBase::$DB_->prepare('INSERT INTO versions(active) VALUES(:active)');
            $statement->bindValue(':active', $version->IsActive(), PDO::PARAM_BOOL);
            $statement->execute();
            return DataBase::$DB_->lastInsertId();
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Добавляет отчёт об объявлениях пользователя в БД.
     * @param ReportAboutAds Объект отчёта.
     */
    public function InsertReportAboutAds($report)
    {
        try {
            $statement = DataBase::$DB_->prepare('INSERT INTO reports_about_ads(user_id, number_advertisements) VALUES(:user_id, :ads_number)');
            $statement->bindValue(':user_id', $report->GetUserId(), PDO::PARAM_INT);
            $statement->bindValue(':ads_number', $report->GetNumberAdvertisements(), PDO::PARAM_INT);
            $statement->execute();
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Получает записи о полученных объявлениях из БД для указанного пользователя.
     * @param int Id пользователя
     * @param String Время начала интервала
     * @param String Время конца интервала
     * @return array of ReportAboutAds Массив сессий.
     */
    public function SelectReportsAboutAdsInIntervalByUser($user_id, $from, $to)
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT * FROM `reports_about_ads` WHERE `user_id` = :user_id AND `datetime` >= :begin_time AND `datetime` <= :end_time');
            $statement->bindValue(':user_id', $user_id, PDO::PARAM_INT);
            $statement->bindValue(':begin_time', $from, PDO::PARAM_STR);
            $statement->bindValue(':end_time', $to, PDO::PARAM_STR);
            $statement->execute();

            $result = $statement->fetchAll();

            $listReports = array();
            foreach ($result as $report) {
                array_push($listReports, new ReportAboutAds($report['id'], $report['user_id'], $report['number_advertisements'], $report['datetime']));
            }
            return $listReports;

        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Получает записи о полученных объявлениях из БД.
     * @param String Время начала интервала
     * @param String Время конца интервала
     * @return array of ReportAboutAds Массив сессий.
     */
    public function SelectReportsAboutAdsInInterval($from, $to)
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT * FROM `reports_about_ads` WHERE `datetime` >= :begin_time AND `datetime` <= :end_time');
            $statement->bindValue(':begin_time', $from, PDO::PARAM_STR);
            $statement->bindValue(':end_time', $to, PDO::PARAM_STR);
            $statement->execute();

            $result = $statement->fetchAll();

            $listReports = array();
            foreach ($result as $report) {
                array_push($listReports, new ReportAboutAds($report['id'], $report['user_id'], $report['number_advertisements'], $report['datetime']));
            }
            return $listReports;

        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Добавляет отчёт о звонке пользователя в БД.
     * @param ReportAboutCall Объект отчёта.
     */
    public function InsertReportAboutCall($report)
    {
        try {
            $statement = DataBase::$DB_->prepare('INSERT INTO reports_about_call(user_id, phone) VALUES(:user_id, :phone)');
            $statement->bindValue(':user_id', $report->GetUserId(), PDO::PARAM_INT);
            $statement->bindValue(':phone', $report->GetPhone(), PDO::PARAM_STR);
            $statement->execute();
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Получает записи о звонках из БД.
     * @param int Id пользователя
     * @param String Время начала интервала
     * @param String Время конца интервала
     * @return array of ReportAboutAds Массив сессий.
     */
    public function SelectReportsAboutCallInInterval($user_id, $from, $to)
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT * FROM `reports_about_call` WHERE `user_id` = :user_id AND `datetime` >= :begin_time AND `datetime` <= :end_time');
            $statement->bindValue(':user_id', $user_id, PDO::PARAM_INT);
            $statement->bindValue(':begin_time', $from, PDO::PARAM_STR);
            $statement->bindValue(':end_time', $to, PDO::PARAM_STR);
            $statement->execute();

            $result = $statement->fetchAll();

            $listReports = array();
            foreach ($result as $report) {
                array_push($listReports, new ReportAboutCall($report['id'], $report['user_id'], $report['phone'], $report['datetime']));
            }
            return $listReports;

        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Получает сессию из БД.
     * @param int Id сеанса
     * @return Session Объект сессии.
     */
    public function SelectSession($session_id)
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT * FROM sessions WHERE id =:id LIMIT 1');
            $statement->bindValue(':id', $session_id, PDO::PARAM_INT);
            $statement->execute();

            $result = $statement->fetch();

            if (count($result)) {
                $session = new Session($result['id'], $result['user_id'], $result['begin_time'], $result['end_time']);
                return $session;
            } else {
                return new Session(0, 0, 0, 0);
            }
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Получает сессии из БД.
     * @param int Id пользователя
     * @param String Время начала интервала
     * @param String Время конца интервала
     * @return array of Session Массив сессий.
     */
    public function SelectSessionsInInterval($user_id, $from, $to)
    {
        try {
            $statement = DataBase::$DB_->prepare('SELECT * FROM sessions WHERE `user_id` = :user_id AND `begin_time` >= :begin_time AND `end_time` <= :end_time');
            $statement->bindValue(':user_id', $user_id, PDO::PARAM_INT);
            $statement->bindValue(':begin_time', $from, PDO::PARAM_STR);
            $statement->bindValue(':end_time', $to, PDO::PARAM_STR);
            $statement->execute();

            $result = $statement->fetchAll();

            $listSessions = array();
            foreach ($result as $session) {
                array_push($listSessions, new Session($session['id'], $session['user_id'], $session['begin_time'], $session['end_time']));
            }
            return $listSessions;

        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Вставляет сессию в БД.
     * @param Session Объект сессии
     * @return int Id сессии.
     */
    public function InsertSession($session)
    {
        try {
            $statement = DataBase::$DB_->prepare('INSERT INTO sessions(user_id) VALUES(:user_id)');
            $statement->bindValue(':user_id', $session->GetUserId(), PDO::PARAM_INT);
            $statement->execute();
            return DataBase::$DB_->lastInsertId();
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }

    /**
     * @brief Обновляет сессию в БД.
     * @param Session Объект сессии
     */
    public function UpdateSession($session)
    {
        try {
            $statement = DataBase::$DB_->prepare('UPDATE sessions SET end_time = FROM_UNIXTIME(:end_time) WHERE id = :id');
            $statement->bindValue(':id', $session->GetId(), PDO::PARAM_INT);
            $statement->bindValue(':end_time', time(), PDO::PARAM_INT);
            $statement->execute();
        } catch (PDOException $e) {
            echo 'Ошибка: ' . $e->getMessage();
        }
    }
}

/*
* @brief Класс для проверки, разрешена ли работа программы.
*/

class Checker
{
    /**
     * @brief Проверяет, существует ли версия программы и разрешена ли работа данной версии программы вообще (для любых пользователей).
     * @param int Номер версии.
     * @return bool True, если версия рабочая, false, если версия устарела или не существует.
     */
    static function CheckVersion($version)
    {
        $db = new DataBase();
        if ($db->ExistVersion($version)) {
            if ($db->SelectVersion($version)->IsActive()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @brief Проверяет наличие пользователя и разрешена ли работа данному пользователю.
     * @param int Id пользователя.
     * @return string Состояние пользователя: active, block, corrupt, not_paid.
     */
    static function CheckUser($user_id)
    {
        $db = new DataBase();
        if ($db->ExistUser($user_id)) {
            $user = $db->SelectUser($user_id);
            if (strtotime($user->GetPaidTo()) < time()) {
                return "not_paid";
            }
            return $user->GetState();
        }
        return "Error";
    }
}

/*
* @brief Класс для создания объектов.
*/

class Creator
{
    /**
     * @brief Добавляет нового пользователя.
     * @param string Информация о пользователе.
     * @param string Ключ лицензии.
     * @return int Id созданного пользователя.
     */
    static function AddUser($user_info, $license_key)
    {
        $db = new DataBase();
        return $db->InsertUser(new User(0, 'active', $user_info, $license_key, time()));
    }

    /**
     * @brief Добавляет новую версию программы.
     * @return int Номер версии.
     */
    static function AddVersion()
    {
        $db = new DataBase();
        return $db->InsertVersion(new Version(0, true));
    }

    /**
     * @brief Добавляет отчёт об объявлениях пользователя.
     * @param int Id пользователя.
     * @param int Количество полученных объявлений.
     */
    static function AddReportAboutAds($user_id, $number_advertisements)
    {
        $db = new DataBase();
        $db->InsertReportAboutAds(new ReportAboutAds(0, $user_id, $number_advertisements, 0));
    }

    /**
     * @brief Добавляет отчёт о звонке пользователя.
     * @param int Id пользователя.
     * @param string Номер телефона.
     */
    static function AddReportAboutCall($user_id, $phone)
    {
        $db = new DataBase();
        $db->InsertReportAboutCall(new ReportAboutCall(0, $user_id, $phone, 0));
    }

    /**
     * @brief Создаём запись о начале сессии.
     * @param int Id пользователя.
     * @return int Id сессии.
     */
    static function StartSession($user_id)
    {
        $db = new DataBase();
        return $db->InsertSession(new Session(0, $user_id, 0, 0));
    }

    /**
     * @brief Закрываем сессию.
     * @param int Id пользователя.
     */
    static function StopSession($session_id)
    {
        $db = new DataBase();
        $db->UpdateSession(new Session($session_id, 0, 0, 0));
    }


    /**
     * @brief Шифруем шаблон запроса
     * @param int Id пользователя.
     * @param string Имя сайта.
     * @return string Шифрованное сообщение.
     */
    static function EncryptPattern($user_id, $sitename)
    {
        $db = new DataBase();
        if ($db->ExistUser($user_id)) {
            $license_key = $db->SelectUser($user_id)->GetLicenseKey();
            $plain['time'] = time();
            $plain[$sitename . 'Pattern'] = file_get_contents('./' . $sitename . 'Step1Request.xml');
            $key = substr($license_key, 0, 16);
            return openssl_encrypt(json_encode($plain), 'aes-128-cbc', $key, OPENSSL_RAW_DATA, $key);
        }
        return "Error";
    }

}

/*
* @brief Класс для анализа статистики.
*/

class Supernumerary
{
    /**
     * @brief Максимальное количество обрабатываемых объявлений за раз.
     */
    const received_ad_at_once = 2;

    /**
     * @brief Возвращает количество полученных пользователем новых объявлений за указанный период.
     * @param $user_id
     * @param $from_date
     * @param $to_date
     * @param $db
     * @return int
     */
    static function GetValidAdvertisementsCount($user_id, $from_date, $to_date, $db = null)
    {
        if(is_null($db))
            $db = new DataBase();

        $reports_list = $db->SelectReportsAboutAdsInIntervalByUser($user_id, $from_date, $to_date);

        $received_ads_count = 0;
        foreach ($reports_list as $report)
            $received_ads_count += ($report->GetNumberAdvertisements() > self::received_ad_at_once) ?
                $report->GetNumberAdvertisements() : self::received_ad_at_once;

        return $received_ads_count;
    }

    /**
     * @brief Возвращает количество полученных пользователем новых объявлений за указанный период.
     * @param $from_date
     * @param $to_date
     * @param $db
     * @return array("user", "ads") Максимальное количество объявлений и пользователь, который их получил или null,
     * если в указаный период не было получено объявлений
     */
    static function GetMaxValidAdvertisements($from_date, $to_date, $db = null)
    {
        if(is_null($db))
            $db = new DataBase();

        $reports_list = $db->SelectReportsAboutAdsInInterval($from_date, $to_date);

        if(empty($reports_list))
            return null;

        $max_received_ads_count = array("user" => -1, "ads" => -1);
        $valid_ads_count_by_user = array();

        foreach ($reports_list as $report)
        {
            $user_id = $report->GetUserId();
            $valid_ads_count = ($report->GetNumberAdvertisements() > self::received_ad_at_once) ?
                $report->GetNumberAdvertisements() : self::received_ad_at_once;;
            if(array_key_exists($user_id, $valid_ads_count_by_user))
                $valid_ads_count_by_user[$user_id] += $valid_ads_count;
            else
                $valid_ads_count_by_user[$user_id] = $valid_ads_count;


            if($max_received_ads_count["ads"] < $valid_ads_count_by_user[$user_id]) {

                    $max_received_ads_count["user"] = $user_id;
                    $max_received_ads_count["ads"] = $valid_ads_count_by_user[$user_id];
                }
        }

        return $max_received_ads_count;
    }
}

/*
* @brief Класс, описывающий пользователя.
*/

class User
{
    private $id_;
    private $state_;
    private $info_;
    private $license_key_;
    private $paid_to_;

    /**
     * User constructor.
     * @param int Id пользователя.
     * @param string Состояние пользователя.
     * @param string Информация о пользователе.
     * @param string Ключ лицензии для пользователя.
     * @param int Значение времени в формате timestamp, до которого у пользователя оплачена лицензия.
     */
    public function __construct($id, $state, $info, $license_key, $paid_to)
    {
        $this->id_ = $id;
        $this->state_ = $state;
        $this->info_ = $info;
        $this->license_key_ = $license_key;
        $this->paid_to_ = $paid_to;
    }

    /**
     * @brief Возвращает id пользователя.
     * @return int Id пользователя.
     */
    function GetId()
    {
        return $this->id_;
    }

    /**
     * @brief Сообщает статус пользователя: активен, заблокирован, нужно уничтожить.
     * @return string active, block, corrupt.
     */
    function GetState()
    {
        return $this->state_;
    }

    /**
     * @brief Блокирует пользователя.
     */
    function Block()
    {
        $this->state_ = 'block';
    }

    /**
     * @brief Активирует пользователя.
     */
    function Activate()
    {
        $this->state_ = 'active';
    }

    /**
     * @brief Уничтожает пользователя.
     */
    function Corrupt()
    {
        $this->state_ = 'corrupt';
    }

    /**
     * @return string Возвращает информацию о пользователе.
     */
    public function GetInfo()
    {
        return $this->info_;
    }

    /**
     * @param string Информация о пользователе.
     */
    public function SetInfo($info_)
    {
        $this->info_ = $info_;
    }

    /**
     * @return string Ключ лицензии.
     */
    public function GetLicenseKey()
    {
        return $this->license_key_;
    }

    /**
     * @param string Ключ лицензии.
     */
    public function SetLicenseKey($license_key_)
    {
        $this->license_key_ = $license_key_;
    }

    /**
     * @return int Значение времени в формате timestamp, до которого у пользователя оплачена лицензия.
     */
    public function GetPaidTo()
    {
        return $this->paid_to_;
    }

    /**
     * @param int Значение времени в формате timestamp, до которого у пользователя оплачена лицензия.
     */
    public function SetPaidTo($paid_to_)
    {
        $this->paid_to_ = $paid_to_;
    }
}

/*
* @brief Класс, описывающий версию программы.
*/

class Version
{
    private $number_version_;
    private $active_;

    /**
     * Version constructor.
     * @param int Номер версии
     * @param bool Активна ли версия.
     */
    public function __construct($number_version, $active)
    {
        $this->number_version_ = $number_version;
        $this->active_ = $active;
    }

    /**
     * @brief Возвращает номер версии программы.
     * @return int Номер версии.
     */
    function GetNumberVersion()
    {
        return $this->number_version_;
    }

    /**
     * @brief Сообщает, активна ли версия.
     * @return bool True, если версия активна, false - если заблокирована.
     */
    function IsActive()
    {
        return $this->active_;
    }

    /**
     * @brief Блокирует версию программы.
     */
    function Block()
    {
        $this->active_ = false;
    }

    /**
     * @brief Активирует версию программы.
     */
    function Activate()
    {
        $this->active_ = true;
    }
}

/*
* @brief Класс, описывающий отчёт о получении объявлений.
*/

class ReportAboutAds
{
    private $id_;
    private $user_id_;
    private $number_advertisements_;
    private $datetime_;

    /**
     * Report constructor.
     * @param int Id отчёта.
     * @param int Id пользователя.
     * @param int Количество объявлений.
     * @param int Временная метка.
     */
    public function __construct($id, $user_id, $number_advertisements, $datetime)
    {
        $this->id_ = $id;
        $this->user_id_ = $user_id;
        $this->number_advertisements_ = $number_advertisements;
        $this->datetime_ = $datetime;
    }

    /**
     * @brief Возвращает id отчёта.
     * @return int Id отчёта.
     */
    public function GetId()
    {
        return $this->id_;
    }

    /**
     * @brief Возвращает id пользователя.
     * @return int Id пользователя.
     */
    public function GetUserId()
    {
        return $this->user_id_;
    }

    /**
     * @brief Возвращает количество объявлений, по результатам получения которых был составлен этот отчёт.
     * @return int Количество полученных объявлений.
     */
    public function GetNumberAdvertisements()
    {
        return $this->number_advertisements_;
    }

    /**
     * @brief Возвращает дату и время в UNIX формате, когда был добавлен отчёт.
     * @return int Дата и время добавления отчёта.
     */
    public function getDatetime()
    {
        return $this->datetime_;
    }
}

/*
* @brief Класс, описывающий отчёт о звонке.
*/

class ReportAboutCall
{
    private $id_;
    private $user_id_;
    private $phone_;
    private $datetime_;

    /**
     * Report constructor.
     * @param int Id отчёта.
     * @param int Id пользователя.
     * @param string Номер телефона.
     * @param int Временная метка.
     */
    public function __construct($id, $user_id, $phone, $datetime)
    {
        $this->id_ = $id;
        $this->user_id_ = $user_id;
        $this->phone_ = $phone;
        $this->datetime_ = $datetime;
    }

    /**
     * @brief Возвращает id отчёта.
     * @return int Id отчёта.
     */
    public function GetId()
    {
        return $this->id_;
    }

    /**
     * @brief Возвращает id пользователя.
     * @return int Id пользователя.
     */
    public function GetUserId()
    {
        return $this->user_id_;
    }

    /**
     * @brief Возвращает номер телефона, по которому был сделан звонок.
     * @return string Номер телефона.
     */
    public function GetPhone()
    {
        return $this->phone_;
    }

    /**
     * @brief Возвращает дату и время в UNIX формате, когда был добавлен отчёт.
     * @return int Дата и время добавления отчёта.
     */
    public function getDatetime()
    {
        return $this->datetime_;
    }
}

/*
* @brief Класс, описывающий сеанс работы пользователя.
*/

class Session
{
    private $id_;
    private $user_id_;
    private $begin_time_;
    private $end_time_;

    /**
     * Session constructor.
     * @param int Id сеанса.
     * @param int Id пользователя.
     * @param int Временная метка начала сеанса.
     * @param int Временная метка конца сеанса.
     */
    public function __construct($id, $user_id, $begin_time, $end_time)
    {
        $this->id_ = $id;
        $this->user_id_ = $user_id;
        $this->begin_time_ = $begin_time;
        $this->end_time_ = $end_time;
    }

    /**
     * @brief Возвращает id сеанса.
     * @return int Id сеанса.
     */
    public function GetId()
    {
        return $this->id_;
    }

    /**
     * @brief Возвращает id пользователя.
     * @return int Id пользователя.
     */
    public function GetUserId()
    {
        return $this->user_id_;
    }

    /**
     * @brief Возвращает дату и время в UNIX формате, когда был начат сеанс.
     * @return int Дата и время начала сеанса.
     */
    public function GetBeginTime()
    {
        return $this->begin_time_;
    }

    /**
     * @brief Возвращает дату и время в UNIX формате, когда был закончен сеанс.
     * @return int Дата и время конца сеанса.
     */
    public function GetEndTime()
    {
        return $this->end_time_;
    }

    /**
     * @brief Устанавливает дату и время в UNIX формате, когда был начат сеанс.
     * @param int Дата и время начала сеанса.
     */
    public function SetBeginTime($begin_time)
    {
        $this->begin_time_ = $begin_time;
    }

    /**
     * @brief Устанавливает дату и время в UNIX формате, когда был закончен сеанс.
     * @param int Дата и время конца сеанса.
     */
    public function SetEndTime($end_time)
    {
        $this->end_time_ = $end_time;
    }
}

header('Content-Type: text/html; charset=utf-8');
session_start();

if (!isset($_REQUEST['action'])) {
    echo "Fail";
    exit(1);
}

if ($_REQUEST['action'] == "GetPattern") {
    if (!isset($_REQUEST['user_id']) || !isset($_REQUEST['sitename'])) {
        echo "Error";
        exit(1);
    }
    echo Creator::EncryptPattern($_REQUEST['user_id'], $_REQUEST['sitename']);
    exit(0);
}

if ($_REQUEST['action'] == "CreateUser") {
    if (!isset($_REQUEST['user_info']) || !isset($_REQUEST['license_key'])) {
        echo "Error";
        exit(1);
    }
    echo Creator::AddUser($_REQUEST['user_info'], $_REQUEST['license_key']);
    exit(0);
}

if ($_REQUEST['action'] == "CreateVersion") {
    echo Creator::AddVersion();
    exit(0);
}

if ($_REQUEST['action'] == "Check") {
    if (!isset($_REQUEST['user_id']) || !isset($_REQUEST['version'])) {
        echo "Error";
        exit(1);
    }

    $user_id = $_REQUEST['user_id'];
    $version = $_REQUEST['version'];

    if (!Checker::CheckVersion($version)) {
        echo "UsesOutdateVersion";
        exit(0);
    }

    switch (Checker::CheckUser($user_id)) {
        case 'block':
            echo "Blocked";
            exit(0);
        case 'corrupt':
            echo "Destroyed";
            exit(0);
        case 'not_paid':
            echo "NotPaid";
            exit(0);
        case 'active':
            echo "Working";
            exit(0);
        default:
            echo "Error";
            exit(0);
    }
}

if ($_REQUEST['action'] == "AddReportAboutAds") {
    if (!isset($_REQUEST['user_id']) || !isset($_REQUEST['ads_number'])) {
        echo "Fail";
        exit(1);
    }

    $user_id = $_REQUEST['user_id'];
    $ads_number = $_REQUEST['ads_number'];

    Creator::AddReportAboutAds($user_id, $ads_number);

    echo "Ok";
    exit(0);
}

if ($_REQUEST['action'] == "AddReportAboutCall") {
    if (!isset($_REQUEST['user_id']) || !isset($_REQUEST['phone'])) {
        echo "Fail";
        exit(1);
    }

    $user_id = $_REQUEST['user_id'];
    $phone = $_REQUEST['phone'];

    Creator::AddReportAboutCall($user_id, $phone);

    echo "Ok";
    exit(0);
}

if ($_REQUEST['action'] == "StartSession") {
    if (!isset($_REQUEST['user_id'])) {
        echo "Fail";
        exit(1);
    }

    $user_id = $_REQUEST['user_id'];

    echo Creator::StartSession($user_id);
    exit(0);
}

if ($_REQUEST['action'] == "StopSession") {
    if (!isset($_REQUEST['session_id'])) {
        echo "Fail";
        exit(1);
    }

    $session_id = $_REQUEST['session_id'];

    Creator::StopSession($session_id);

    echo "Ok";
    exit(0);
}

if ($_REQUEST['action'] == "GetListUsers") {
    if (!isset($_SESSION['login']) || $_SESSION['login'] != true) {
        echo "Нет доступа. ";
        exit(1);
    }
    $db = new DataBase();
    $list_users = $db->SelectAllUsers();

    $month_ago = new DateTime();
    $month_ago->modify("-1 month");
    $from_time = $month_ago->format('Y-m-d H:i:s');
    $current_date_time = new DateTime();
    $to_time = $current_date_time->format('Y-m-d H:i:s');

    $max_valid_ads_count = Supernumerary::GetMaxValidAdvertisements($from_time, $to_time);

    ?>
    <!DOCTYPE html>
    <html lang="ru-ru">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="Resources/view.css" media="all">
        <title>Список пользователей</title>
    </head>
    <body id="main_body">
    <img id="top" src="Resources/top.png" alt="">
    <div id="form_container">
        <h1 style="text-indent: 0; text-align: center;">Список пользователей</h1>
        <table id="basic_table">
            <tr id="basic_table_header_row">
                <th id="basic_table_cell">ID</th>
                <th id="basic_table_cell">Состояние</th>
                <th id="basic_table_cell">Ключ</th>
                <th id="basic_table_cell">Оплачено до</th>
                <th id="basic_table_cell">Активность</th>
                <th></th>
                <th></th>
            </tr>
            <?php
            foreach ($list_users as $user) {
                $user_valid_ads_count = Supernumerary::GetValidAdvertisementsCount($user->GetId(), $from_time, $to_time);
                $user_activity = 0;
                if(is_null($max_valid_ads_count))
                    $user_activity = "Не определена";
                else
                    $user_activity = number_format(
                            $user_valid_ads_count / $max_valid_ads_count["ads"], 2);

                ?>
                <tr id="basic_table_row">
                    <th id="basic_table_cell"><?php echo $user->GetId(); ?></th>
                    <th id="basic_table_cell"><?php echo $user->GetState(); ?></th>
                    <th id="basic_table_cell"><?php echo $user->GetLicenseKey(); ?></th>
                    <th id="basic_table_cell"><?php echo $user->GetPaidTo(); ?></th>
                    <th id="basic_table_cell"><?php echo  $user_activity ?></th>
                    <th id="basic_table_image_cell">
                        <a href="index.php?action=EditUser&user_id=<?php echo $user->GetId(); ?>">
                            <img id="basic_table_image_cell_image" src="Resources/edit.png" title="Редактировать">
                        </a>
                    </th>
                    <th id="basic_table_image_cell">
                        <a href="index.php?action=GetSupernumerary&user_id=<?php echo $user->GetId(); ?>">
                            <img id="basic_table_image_cell_image" src="Resources/statistic.png" title="Статистика">
                        </a>
                    </th>

                </tr>
                <?php
            }
            ?>

        </table>
    </div>
    <img id="bottom" src="Resources/bottom.png" alt="">
    </body>
    </html>
    <?php
    exit(0);
}

if ($_REQUEST['action'] == "EditUser") {
    if (!isset($_SESSION['login']) || $_SESSION['login'] != true) {
        echo "Нет доступа. ";
        exit(1);
    }

    if (!isset($_REQUEST['user_id'])) {
        echo "Fail";
        exit(1);
    }

    $db = new DataBase();
    $user = $db->SelectUser($_REQUEST['user_id']);

    if (isset($_POST['state'])) {
        switch ($_POST['state']) {
            case "active":
                $user->Activate();
                break;
            case "block":
                $user->Block();
                break;
            case "corrupt":
                $user->Corrupt();
                break;
        }
        $user->SetPaidTo($_POST['year'] . "-" . $_POST['month'] . "-" . $_POST['day'] . " 00:00:00");
        $db->UpdateUser($user);
    }

    $time = strtotime($user->GetPaidTo());
    $day = date('d', $time);
    $month = date('m', $time);
    $year = date('Y', $time);

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Пользователь №<?= $user->GetId() ?></title>
        <link rel="stylesheet" type="text/css" href="Resources/view.css" media="all">
        <script type="text/javascript" src="Resources/view.js"></script>
        <script type="text/javascript" src="Resources/calendar.js"></script>
    </head>
    <body id="main_body">

    <img id="top" src="Resources/top.png" alt="">
    <div id="form_container">

        <h1 style="text-indent: 0;">Редактирование пользователя</h1>
        <form id="form_58838" class="appnitro" method="post"
              action="index.php?action=EditUser&user_id=<?= $user->GetId() ?>">
            <div class="form_description">
                <h2>Пользователь №<?= $user->GetId() ?></h2>
                <p></p>
            </div>
            <ul>
                <li id="li_2">
                    <label class="description" for="state">Состояние: </label>
                    <div>
                        <select class="element select medium" id="state" name="state">
                            <option value="active" <?= ($user->GetState() == "active" ? "selected=\"selected\"" : "") ?>>
                                Активен
                            </option>
                            <option value="block" <?= ($user->GetState() == "block" ? "selected=\"selected\"" : "") ?>>
                                Заблокирован
                            </option>
                            <option value="corrupt" <?= ($user->GetState() == "corrupt" ? "selected=\"selected\"" : "") ?>>
                                Уничтожен
                            </option>
                        </select>
                    </div>
                    <p class="guidelines" id="guide_2">
                        <small>Уничтожайте пользователя только при наличии веских причин. При необходимости временно
                            сделать его неактивным выбирайте пункт "Заблокирован".
                        </small>
                    </p>
                </li>
                <li id="li_1">
                    <label class="description" for="date">Лицензия оплачена до: </label>
                    <span>
			<input id="day" name="day" class="element text" size="2" maxlength="2" value="<?= $day ?>" type="text"> /
			<label for="day">DD</label>
		</span>
                    <span>
			<input id="month" name="month" class="element text" size="2" maxlength="2" value="<?= $month ?>"
                   type="text"> /
			<label for="month">MM</label>
		</span>
                    <span>
	 		<input id="year" name="year" class="element text" size="4" maxlength="4" value="<?= $year ?>" type="text">
			<label for="year">YYYY</label>
		</span>

                    <span id="calendar_1">
			<img id="cal_img_1" class="datepicker" src="Resources/calendar.gif" alt="Pick a date.">
		</span>
                    <script type="text/javascript">
                        Calendar.setup({
                            inputField: "element_1_3",
                            baseField: "element_1",
                            displayArea: "calendar_1",
                            button: "cal_img_1",
                            ifFormat: "%B %e, %Y",
                            onSelect: selectEuropeDate
                        });
                    </script>

                </li>

                <li class="buttons">
                    <input id="saveForm" class="button_text" type="submit" name="submit" value="Сохранить"/>
                </li>
            </ul>
        </form>
    </div>
    <img id="bottom" src="Resources/bottom.png" alt="">
    </body>
    </html>
    <?php
    exit(0);
}

if ($_REQUEST['action'] == "Login") {
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Авторизация</title>
        <link rel="stylesheet" type="text/css" href="Resources/view.css" media="all">
        <script type="text/javascript" src="Resources/view.js"></script>

    </head>
    <body id="main_body">
    <img id="top" src="Resources/top.png" alt="">
    <div id="form_container">
        <h1 style="text-indent: 0;">Авторизация</h1>

        <?php
        $form = "<form id=\"form_58838\" class=\"appnitro\"  method=\"post\" action=\"index.php?action=Login\">
                    <div class=\"form_description\">
                        <h2>Вход в панель управления</h2>
                        <p></p>
                    </div>
                    <ul >

                        <li id=\"li_1\" >
                            <label class=\"description\" for=\"password\">Пароль </label>
                            <div>
                                <input id=\"password\" name=\"password\" class=\"element text medium\" type=\"password\" maxlength=\"255\" value=\"\"/>
                            </div>
                        </li>

                        <li class=\"buttons\">
                            <input type=\"hidden\" name=\"login\" value=\"58838\" />

                            <input id=\"saveForm\" class=\"button_text\" type=\"submit\" name=\"submit\" value=\"Войти\" />
                        </li>
                    </ul>
                </form>";

        if (isset($_SESSION['login']) && $_SESSION['login'] == true) {
            //авторизованы
            if (isset($_POST['logout'])) {
                //выходим
                session_unset();
                session_destroy();
                ?>
                <h2>До свидания.</h2>
                <?php
            } else {
                ?>
                <form id="form_58838" class="appnitro" method="post" action="index.php?action=Login">
                    <div class="form_description">
                        <h2>Вы авторизованы</h2>
                        <p></p>
                    </div>
                    <ul>
                        <li class="buttons">
                            <input type="hidden" name="logout" value="58838"/>
                            <label class="description" for="saveForm">Вы можете </label>
                            <input id="saveForm" class="button_text" type="submit" name="submit" value="Выйти"/>
                        </li>
                    </ul>
                </form>
                <?php
            }
        } else {
            //неавторизованы
            if (isset($_POST['login'])) {
                //попробовали авторизоваться
                if (md5($_POST['password']) == "2da666eff5f8f461879635d53675c2c2") {
                    //пароль верный
                    $_SESSION['login'] = true;
                    ?>
                    <h2>Вы успешно авторизовались.</h2>
                    <?php
                } else {
                    //пароль неверный
                    ?>
                    <h2>Что-то пошло не так. Можете попробовать ещё раз.</h2>
                    <?php
                    echo $form;
                }
            } else {
                //ещё не пробовали авторизовываться
                echo $form;
            }
        }

        ?>
    </div>
    <img id="bottom" src="Resources/bottom.png" alt="">
    </body>
    </html>
    <?php
    exit(0);
}

if ($_REQUEST['action'] == "GetSupernumerary") {
    if (!isset($_SESSION['login']) || $_SESSION['login'] != true) {
        echo "Нет доступа. ";
        exit(1);
    }

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Статистика пользователя</title>
        <link rel="stylesheet" type="text/css" href="Resources/view.css" media="all">
        <script type="text/javascript" src="Resources/view.js"></script>
        <script type="text/javascript" src="Resources/calendar.js"></script>
    </head>
    <body id="main_body">

    <img id="top" src="Resources/top.png" alt="">
    <div id="form_container">

        <h1 style="text-indent: 0;">Статистика
            пользователя<?= (isset($_REQUEST['user_id']) ? " №" . $_REQUEST['user_id'] : "") ?></h1>
        <?php
        $db = new DataBase();
        if (isset($_REQUEST['supernumerary'])) {
            //форма отправлена
            $date_from = $_REQUEST['date_from_y'] . "-" . $_REQUEST['date_from_m'] . "-" . $_REQUEST['date_from_d'];
            $date_to = $_REQUEST['date_to_y'] . "-" . $_REQUEST['date_to_m'] . "-" . $_REQUEST['date_to_d'];
            $time_from = $_REQUEST['time_from_h'] . "-" . $_REQUEST['time_from_m'] . "-" . $_REQUEST['time_from_s'];
            $time_to = $_REQUEST['time_to_h'] . "-" . $_REQUEST['time_to_m'] . "-" . $_REQUEST['time_to_s'];
            $from = $date_from . " " . $time_from;
            $to = $date_to . " " . $time_to;
            echo "<h2>С " . $from . " по " . $to . "</h2>";
            $db = new DataBase();
            $list_sessions = $db->SelectSessionsInInterval($_REQUEST['user_id'], $from, $to);
            echo "<p>Пользователь запускал программу " . count($list_sessions) . " раз</p>";
            $result_time = new DateTime("2000-01-01 00:00:00");
            $setResultTime = false;
            $withoutEnd = 0;
            foreach ($list_sessions as $session) {
                if ($session->GetBeginTime() != "0000-00-00 00:00:00" && $session->GetEndTime() != "0000-00-00 00:00:00") {
                    $interval = date_diff(date_create($session->GetBeginTime()), date_create($session->GetEndTime()));
                    $result_time->add($interval);
                } else {
                    ++$withoutEnd;
                }
            }
            $result_interval = date_diff(new DateTime("2000-01-01 00:00:00"), $result_time);
            echo "<p>Суммарно провёл в программе " . $result_interval->format("%y лет %m месяцев %d дней, %h часов %i минут %s секунд") . " и имеет " . $withoutEnd . " незавершённых сессий.</p>";
            $list_reports_about_ads = $db->SelectReportsAboutAdsInIntervalByUser($_REQUEST['user_id'], $from, $to);
            $numAds = 0;
            foreach ($list_reports_about_ads as $report) {
                $numAds += $report->GetNumberAdvertisements();
            }
            echo "<p>За это время он получил " . $numAds . " объявлений.</p>";
            $list_reports_about_call = $db->SelectReportsAboutCallInInterval($_REQUEST['user_id'], $from, $to);
            echo "<p>И совершил " . count($list_reports_about_call) . " звонков прямо из программы.</p>";
            //получить инфу о запусках проги, звонках, полученных объявлениях
        } else {
            //отобразить форму
            ?>

            <form id="form_58838" class="appnitro" method="post" action="index.php?action=GetSupernumerary">
                <div class="form_description">
                    <h2>Статистика пользователя</h2>
                    <p>Выберите промежуток времени, за который хотите получить статистику использования программы
                        пользователем.</p>
                </div>
                <ul>

                    <li id="li_5">
                        <label class="description" for="user_id">Пользователь № </label>
                        <div>
                            <select class="element select medium" id="user_id" name="user_id">
                                <?php
                                $users = $db->SelectAllUsers();
                                foreach ($users as $user) {
                                    echo "<option value=\"" . $user->GetId() . "\"" . (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] == $user->GetId() ? " selected=\"selected\"" : "") . ">" . $user->GetId() . "</option>";
                                }
                                ?>

                            </select>
                        </div>
                        <p class="guidelines" id="guide_5">
                            <small>Выберите пользователя.</small>
                        </p>
                    </li>
                    <li id="li_1">
                        <label class="description" for="element_1">Дата начала интервала </label>
                        <span>
			<input id="element_1_1" name="date_from_m" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_1_1">MM</label>
		</span>
                        <span>
			<input id="element_1_2" name="date_from_d" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_1_2">DD</label>
		</span>
                        <span>
	 		<input id="element_1_3" name="date_from_y" class="element text" size="4" maxlength="4" value="" type="text">
			<label for="element_1_3">YYYY</label>
		</span>

                        <span id="calendar_1">
			<img id="cal_img_1" class="datepicker" src="Resources/calendar.gif" alt="Pick a date.">
		</span>
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField: "element_1_3",
                                baseField: "element_1",
                                displayArea: "calendar_1",
                                button: "cal_img_1",
                                ifFormat: "%B %e, %Y",
                                onSelect: selectDate
                            });
                        </script>
                        <p class="guidelines" id="guide_1">
                            <small>С какой даты подсчитать статистику?</small>
                        </p>
                    </li>
                    <li id="li_2">
                        <label class="description" for="element_2">Время начала интервала </label>
                        <span>
			<input id="element_2_1" name="time_from_h" class="element text " size="2" type="text" maxlength="2"
                   value="00"/> :
			<label>HH</label>
		</span>
                        <span>
			<input id="element_2_2" name="time_from_m" class="element text " size="2" type="text" maxlength="2"
                   value="00"/> :
			<label>MM</label>
		</span>
                        <span>
			<input id="element_2_3" name="time_from_s" class="element text " size="2" type="text" maxlength="2"
                   value="00"/>
			<label>SS</label>
		</span>
                        <p class="guidelines" id="guide_2">
                            <small>С какого времени подсчитать статистику?</small>
                        </p>
                    </li>
                    <li id="li_3">
                        <label class="description" for="element_3">Дата конца интервала </label>
                        <span>
			<input id="element_3_1" name="date_to_m" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_3_1">MM</label>
		</span>
                        <span>
			<input id="element_3_2" name="date_to_d" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_3_2">DD</label>
		</span>
                        <span>
	 		<input id="element_3_3" name="date_to_y" class="element text" size="4" maxlength="4" value="" type="text">
			<label for="element_3_3">YYYY</label>
		</span>

                        <span id="calendar_3">
			<img id="cal_img_3" class="datepicker" src="Resources/calendar.gif" alt="Pick a date.">
		</span>
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField: "element_3_3",
                                baseField: "element_3",
                                displayArea: "calendar_3",
                                button: "cal_img_3",
                                ifFormat: "%B %e, %Y",
                                onSelect: selectDate
                            });
                        </script>
                        <p class="guidelines" id="guide_3">
                            <small>До какой даты подсчитать статистику?</small>
                        </p>
                    </li>
                    <li id="li_4">
                        <label class="description" for="element_4">Время конца интервала </label>
                        <span>
			<input id="element_4_1" name="time_to_h" class="element text " size="2" type="text" maxlength="2"
                   value="23"/> :
			<label>HH</label>
		</span>
                        <span>
			<input id="element_4_2" name="time_to_m" class="element text " size="2" type="text" maxlength="2"
                   value="59"/> :
			<label>MM</label>
		</span>
                        <span>
			<input id="element_4_3" name="time_to_s" class="element text " size="2" type="text" maxlength="2"
                   value="59"/>
			<label>SS</label>
		</span>
                        <p class="guidelines" id="guide_4">
                            <small>До какого времени подсчитать статистику?</small>
                        </p>
                    </li>

                    <li class="buttons">
                        <input type="hidden" name="supernumerary" value="58838"/>

                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Подсчитать"/>
                    </li>
                </ul>
            </form>

            <?php
        }
        ?>
    </div>
    <img id="bottom" src="Resources/bottom.png" alt="">
    </body>
    </html>
    <?php

    exit(0);
}

if ($_REQUEST['action'] == "VisualComparison") {
    if (!isset($_SESSION['login']) || $_SESSION['login'] != true) {
        echo "Нет доступа. ";
        exit(1);
    }

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Использование программы</title>
        <link rel="stylesheet" type="text/css" href="Resources/view.css" media="all">
        <script type="text/javascript" src="Resources/view.js"></script>
        <script type="text/javascript" src="Resources/calendar.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.css"/>
        <link rel="stylesheet" href="https://rawgithub.com/Caged/d3-tip/master/examples/example-styles.css"/>
        <link rel="stylesheet" href="Resources/timeline-chart.css"/>
    </head>
    <body id="main_body">

    <?php
    $db = new DataBase();
    if (isset($_REQUEST['visualcomparison'])) {
        ?>
        <img id="top" src="Resources/top.png" alt="" style="width:90%;">
        <div id="form_container" style="width:90%;">

            <h1 style="text-indent: 0;">Использование программы</h1>
            <?php
            //форма отправлена
            $date_from = $_REQUEST['date_from_y'] . "-" . $_REQUEST['date_from_m'] . "-" . $_REQUEST['date_from_d'];
            $date_to = $_REQUEST['date_to_y'] . "-" . $_REQUEST['date_to_m'] . "-" . $_REQUEST['date_to_d'];
            $time_from = $_REQUEST['time_from_h'] . "-" . $_REQUEST['time_from_m'] . "-" . $_REQUEST['time_from_s'];
            $time_to = $_REQUEST['time_to_h'] . "-" . $_REQUEST['time_to_m'] . "-" . $_REQUEST['time_to_s'];
            $from = $date_from . " " . $time_from;
            $to = $date_to . " " . $time_to;
            echo "<h2>С " . $from . " по " . $to . "</h2>";
            //print_r($_REQUEST['user_id']);
            ?>
            <section flex flex-full-center>
                <div id="chart"></div>
            </section>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.16/d3.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/d3-tip/0.6.7/d3-tip.min.js"></script>
            <script src="Resources/timeline-chart.js"></script>
            <link rel="stylesheet" href="Resources/style-timeline-page.css"/>
            <script id="code">
                'use strict';
                const element = document.getElementById('chart');
                <?php
                $db = new DataBase();
                if (is_array(!$_REQUEST['user_id'])) {
                    $_REQUEST['user_id'] = array($_REQUEST['user_id']);
                }
                echo "const data = [";
                for ($i = 0; $i < count($_REQUEST['user_id']); ++$i) {
                    echo "{label: 'Пользователь №" . $_REQUEST['user_id'][$i] . "', data: [";
                    $list_sessions = $db->SelectSessionsInInterval($_REQUEST['user_id'][$i], $from, $to);
                    for ($j = 0; $j < count($list_sessions); ++$j) {
                        if ($list_sessions[$j]->GetBeginTime() == "0000-00-00 00:00:00" ||
                            $list_sessions[$j]->GetEndTime() == "0000-00-00 00:00:00")
                        {
                            continue;
                        }
                        $date_from = date_create($list_sessions[$j]->GetBeginTime());
                        $date_to = date_create($list_sessions[$j]->GetEndTime());
                        echo "{ type: TimelineChart.TYPE.INTERVAL, ";
                        echo "from: new Date(";
                        echo $date_from->format("U000");
                        echo "), to: new Date(";
                        echo $date_to->format("U000");
                        echo ")}";
                        if ($j != count($list_sessions) - 1) {
                            echo ", ";
                        }
                    }
                    $list_reports_about_call = $db->SelectReportsAboutCallInInterval($_REQUEST['user_id'][$i], $from, $to);
                    if (count($list_reports_about_call)>0) {
                        echo ", ";
                    }
                    for ($j = 0; $j < count($list_reports_about_call); ++$j) {
                        if ($list_reports_about_call[$j]->getDatetime() == "0000-00-00 00:00:00")
                        {
                            continue;
                        }
                        $date = date_create($list_reports_about_call[$j]->getDatetime());
                        echo "{ type: TimelineChart.TYPE.POINT, ";
                        echo "at: new Date(";
                        echo $date->format("U000");
                        echo ")}";
                        if ($j != count($list_reports_about_call) - 1) {
                            echo ", ";
                        }
                    }
                    echo "]}";
                    if ($i != count($_REQUEST['user_id']) - 1) {
                        echo ", ";
                    }
                }
                echo "];";
                ?>
                const timeline = new TimelineChart(element, data, {
                    //enableLiveTimer: true,
                        intervalMinWidth: 8, // minimum width for an interval element
                        //tip: undefined,
                        textTruncateThreshold: 30,
                        enableLiveTimer: false,
                        timerTickInterval: 1000,
                        hideGroupLabels: false,
                    tip: function (d) {
                        return d.at || `${d.from}<br>${d.to}`;
                    }
                }).onVizChange(e => console.log(e));
            </script>
            <p>Жёлтые интервалы - сеансы работы в программе.</p>
            <p>Красные точки - звонки из программы.</p>
        </div>
        <img id="bottom" src="Resources/bottom.png" alt="" style="width: 90%;">
        <?php
    } else {
        //отобразить форму
        ?>
        <img id="top" src="Resources/top.png" alt="">
        <div id="form_container">

            <h1 style="text-indent: 0;">Использование программы</h1>
            <form id="form_58838" class="appnitro" method="post" action="index.php?action=VisualComparison">
                <div class="form_description">
                    <h2>Использование программы</h2>
                    <p>Выберите промежуток времени, за который хотите построить графики использования программы
                        пользователями.</p>
                </div>
                <ul>

                    <li id="li_5">
                        <label class="description" for="user_id">Пользователи № </label>
                        <div>
                            <select class="element select medium" id="user_id" name="user_id[]" size="5" multiple>
                                <?php
                                $users = $db->SelectAllUsers();
                                foreach ($users as $user) {
                                    echo "<option value=\"" . $user->GetId() . "\"" . (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] == $user->GetId() ? " selected=\"selected\"" : "") . ">" . $user->GetId() . "</option>";
                                }
                                ?>

                            </select>
                        </div>
                        <p class="guidelines" id="guide_5">
                            <small>Выберите пользователей.</small>
                        </p>
                    </li>
                    <li id="li_1">
                        <label class="description" for="element_1">Дата начала интервала </label>
                        <span>
			<input id="element_1_1" name="date_from_m" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_1_1">MM</label>
		</span>
                        <span>
			<input id="element_1_2" name="date_from_d" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_1_2">DD</label>
		</span>
                        <span>
	 		<input id="element_1_3" name="date_from_y" class="element text" size="4" maxlength="4" value="" type="text">
			<label for="element_1_3">YYYY</label>
		</span>

                        <span id="calendar_1">
			<img id="cal_img_1" class="datepicker" src="Resources/calendar.gif" alt="Pick a date.">
		</span>
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField: "element_1_3",
                                baseField: "element_1",
                                displayArea: "calendar_1",
                                button: "cal_img_1",
                                ifFormat: "%B %e, %Y",
                                onSelect: selectDate
                            });
                        </script>
                        <p class="guidelines" id="guide_1">
                            <small>С какой даты рисовать график?</small>
                        </p>
                    </li>
                    <li id="li_2">
                        <label class="description" for="element_2">Время начала интервала </label>
                        <span>
			<input id="element_2_1" name="time_from_h" class="element text " size="2" type="text" maxlength="2"
                   value="00"/> :
			<label>HH</label>
		</span>
                        <span>
			<input id="element_2_2" name="time_from_m" class="element text " size="2" type="text" maxlength="2"
                   value="00"/> :
			<label>MM</label>
		</span>
                        <span>
			<input id="element_2_3" name="time_from_s" class="element text " size="2" type="text" maxlength="2"
                   value="00"/>
			<label>SS</label>
		</span>
                        <p class="guidelines" id="guide_2">
                            <small>С какого времени подсчитать статистику?</small>
                        </p>
                    </li>
                    <li id="li_3">
                        <label class="description" for="element_3">Дата конца интервала </label>
                        <span>
			<input id="element_3_1" name="date_to_m" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_3_1">MM</label>
		</span>
                        <span>
			<input id="element_3_2" name="date_to_d" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_3_2">DD</label>
		</span>
                        <span>
	 		<input id="element_3_3" name="date_to_y" class="element text" size="4" maxlength="4" value="" type="text">
			<label for="element_3_3">YYYY</label>
		</span>

                        <span id="calendar_3">
			<img id="cal_img_3" class="datepicker" src="Resources/calendar.gif" alt="Pick a date.">
		</span>
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField: "element_3_3",
                                baseField: "element_3",
                                displayArea: "calendar_3",
                                button: "cal_img_3",
                                ifFormat: "%B %e, %Y",
                                onSelect: selectDate
                            });
                        </script>
                        <p class="guidelines" id="guide_3">
                            <small>До какой даты рисовать график?</small>
                        </p>
                    </li>
                    <li id="li_4">
                        <label class="description" for="element_4">Время конца интервала </label>
                        <span>
			<input id="element_4_1" name="time_to_h" class="element text " size="2" type="text" maxlength="2"
                   value="23"/> :
			<label>HH</label>
		</span>
                        <span>
			<input id="element_4_2" name="time_to_m" class="element text " size="2" type="text" maxlength="2"
                   value="59"/> :
			<label>MM</label>
		</span>
                        <span>
			<input id="element_4_3" name="time_to_s" class="element text " size="2" type="text" maxlength="2"
                   value="59"/>
			<label>SS</label>
		</span>
                        <p class="guidelines" id="guide_4">
                            <small>До какого времени подсчитать статистику?</small>
                        </p>
                    </li>

                    <li class="buttons">
                        <input type="hidden" name="visualcomparison" value="58838"/>

                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Показать"/>
                    </li>
                </ul>
            </form>
        </div>
        <img id="bottom" src="Resources/bottom.png" alt="">
        <?php
    }
    ?>

    </body>
    </html>
    <?php

    exit(0);
}

if ($_REQUEST['action'] == "OverallAssessment") {
    if (!isset($_SESSION['login']) || $_SESSION['login'] != true) {
        echo "Нет доступа. ";
        exit(1);
    }

    var_dump(Supernumerary::GetMaxValidAdvertisements("0000-00-00 00:00:00", "0000-00-00 00:00:00"));

    exit(0);
}


echo "Fail";
exit(2);