#pragma once
#include <QMenu>


class ActiveAdContextMenu : public QMenu
{
Q_OBJECT
public:
	explicit ActiveAdContextMenu(QWidget* parent = nullptr) : QMenu(QStringLiteral("��������"), parent)
	{
		addAction(QStringLiteral("���������"), this, &ActiveAdContextMenu::Call);
		addAction(QStringLiteral("� ��������"), this, &ActiveAdContextMenu::Browser);
		addAction(QStringLiteral("���������"), this, &ActiveAdContextMenu::Save);
	}

	Q_SIGNAL void Call();
	Q_SIGNAL void Browser();
	Q_SIGNAL void Save();
};
