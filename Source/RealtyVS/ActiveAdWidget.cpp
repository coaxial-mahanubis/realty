#include "ActiveAdWidget.h"
#include "StylesManager.h"

ActiveAdWidget::ActiveAdWidget(QWidget* parent): QWidget(parent)
{
	_ui.setupUi(this);
	setStyleSheet(StylesManager::GetAdWidgetStyleSheets(AdWidget::State::kSelected,false));
	
	InstallEventFilters();

	connect(_menu, &ActiveAdContextMenu::Call, this, &ActiveAdWidget::Call);
	connect(_menu, &ActiveAdContextMenu::Browser, this, &ActiveAdWidget::Browser);
	connect(_menu, &ActiveAdContextMenu::Save, this, &ActiveAdWidget::SaveToFile);
}

void ActiveAdWidget::ResetSelected(const AdData& data)
{
	_data = data;
	DataToGui();
}

void ActiveAdWidget::Call() const
{
	if (_data)
	{
		emit CallExecuted(_data.value().phone_number.toStdString());
		::Call(_data.value());
	}
		
}

void ActiveAdWidget::Browser() const
{
	if (_data)
		InBrowser(_data.value());
}

void ActiveAdWidget::SaveToFile()
{
	if (_data)
		Save(_data.value(), this);
}

void ActiveAdWidget::InstallEventFilters()
{
	_ui.square_edit->installEventFilter(this);
	_ui.cost_edit->installEventFilter(this);
	_ui.phone_edit->installEventFilter(this);
	_ui.address_edit->installEventFilter(this);
	_ui.rooms_edit->installEventFilter(this);
	_ui.owner_edit->installEventFilter(this);
	_ui.address_edit->installEventFilter(this);
	_ui.url_edit->installEventFilter(this);
	_ui.description_edit->installEventFilter(this);
	_ui.description_edit->viewport()->installEventFilter(this);
}

void ActiveAdWidget::DataToGui() const
{
	const auto data = _data.value();
	_ui.square_edit->setText(QString::number(data.square));
	_ui.cost_edit->setText(QString::number(data.cost));
	_ui.phone_edit->setText(data.phone_number);
	_ui.address_edit->setText(data.address);
	_ui.rooms_edit->setText(QString::number(data.room_count));
	_ui.owner_edit->setText(data.owner_name);
	_ui.address_edit->setText(data.address);
	_ui.url_edit->setText(data.url);
	_ui.description_edit->setPlainText(data.description);
}

void ActiveAdWidget::contextMenuEvent(QContextMenuEvent* event)
{
	_menu->exec(event->globalPos());
}

void ActiveAdWidget::mouseDoubleClickEvent(QMouseEvent* event)
{
	Call();
}

bool ActiveAdWidget::eventFilter(QObject* watched, QEvent* event)
{
		if (event->type() == QEvent::MouseButtonDblClick)
	{
		const auto dbl_click = dynamic_cast<QMouseEvent*>(event);
		if (dbl_click->button() == Qt::LeftButton)
		{
			Call();
			return false;
		}
	}

	return QObject::eventFilter(watched, event);
}
