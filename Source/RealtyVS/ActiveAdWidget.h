#pragma once
#include "ui_ActiveWidget.h"
#include <optional>
#include "AdDataTools.h"
#include "ActiveAdContextMenu.h"


class ActiveAdWidget : public QWidget
{
	Q_OBJECT
public:
	explicit ActiveAdWidget(QWidget* parent = nullptr);
	Q_SLOT void ResetSelected(const AdData& data);

	Q_SLOT void Call() const;
	Q_SLOT void Browser() const;
	Q_SLOT void SaveToFile();

	void contextMenuEvent(QContextMenuEvent* event) override;
	void mouseDoubleClickEvent(QMouseEvent* event) override;
	bool eventFilter(QObject* watched, QEvent* event) override;

	Q_SIGNAL void CallExecuted(const std::string& phone) const;

private:
	Ui::ActiveAd _ui{};
	ActiveAdContextMenu* _menu{new ActiveAdContextMenu(this)};
	
	std::optional<AdData> _data{};

	void InstallEventFilters();
	void DataToGui() const;
};

