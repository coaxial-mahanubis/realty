#pragma once
#include <QMenu>

class AdContextMenu : public QMenu
{
Q_OBJECT
public:
	explicit AdContextMenu(QWidget* parent = nullptr) : QMenu(QStringLiteral("��������"), parent)
	{
		addAction(QStringLiteral("�������"), this, &AdContextMenu::Select)->font().setBold(true);
		addAction(QStringLiteral("���������"), this, &AdContextMenu::Call);
		_protected_act = addAction(QStringLiteral("���������"), this, &AdContextMenu::Protect);
		_protected_act->setCheckable(true);
		addAction(QStringLiteral("� ��������"), this, &AdContextMenu::Browser);
		addAction(QStringLiteral("���������"), this, &AdContextMenu::Save);
		addAction(QStringLiteral("�������"), this, &AdContextMenu::Delete);
	}

	bool IsProtectedChecked() const {return _protected_act->isChecked();}
	void SetProtectionState(const bool state) const {_protected_act->setChecked(state);}

	Q_SIGNAL void Select();
	Q_SIGNAL void Call();
	Q_SIGNAL void Protect();
	Q_SIGNAL void Browser();
	Q_SIGNAL void Save();
	Q_SIGNAL void Delete();

private:
	QAction* _protected_act{};
};
