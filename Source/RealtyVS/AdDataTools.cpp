#include "AdDataTools.h"
#include <QMessageBox>
#include "phone-controller.h"



void Call(const AdData& data)
{
	//static PhoneController phone;
	PhoneController::Call(data.phone_number.toStdString());
}

void Save(const AdData& data, QWidget* file_dialog_parent)
{
	auto file_name = QFileDialog::getSaveFileName(file_dialog_parent, QString::fromLocal8Bit("���������..."), "",
	                                              QString::fromLocal8Bit("����� (*.txt)"));

	if (file_name.isEmpty())
	{
		return;
	}

	QFile save_file(file_name);
	if (!save_file.open(QIODevice::Append | QIODevice::Text))
	{
		QMessageBox msg_box;
		msg_box.setWindowTitle(QString::fromLocal8Bit("��������..."));
		msg_box.setText(QString::fromLocal8Bit("�� ������� ������� ����:\n") + file_name);
		msg_box.show();
	}

	save_file.write(data.ToString().toLocal8Bit());
	save_file.close();
}
