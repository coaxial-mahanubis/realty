#pragma once
#include "ad-data.h"
#include <QDesktopServices>
#include <QFileDialog>
#include "phone-controller.h"

void Call(const AdData& data);
inline void InBrowser(const AdData& data){QDesktopServices::openUrl(QUrl(data.url));}
void Save(const AdData& data, QWidget* file_dialog_parent);