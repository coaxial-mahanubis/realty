#include "AdWidget.h"
#include <QTime>
#include "AdContextMenu.h"
#include "gsl_custom.h"
#include "AdDataTools.h"
#include "StylesManager.h"

AdWidget::AdWidget(QWidget* parent): QWidget(parent)
{
	_ui.setupUi(this);
}

void AdWidget::InstallIventFilters()
{
	_ui.square_edit->installEventFilter(this);
	_ui.cost_edit->installEventFilter(this);
	_ui.phone_edit->installEventFilter(this);
	_ui.address_edit->installEventFilter(this);
	_ui.address_edit->viewport()->installEventFilter(this);
}

AdWidget::AdWidget(AdData ad_data, const UPtrList::iterator location, const State state, QWidget* parent):
	QWidget(parent),
	_ad(std::move(ad_data)),
	_location(location)
{
	_ui.setupUi(this);
	_state = state;
	setStyleSheet(StylesManager::GetAdWidgetStyleSheets(_state, false));
	_ui.site_time_label->setText(QUrl(_ad.url).host() + QTime::currentTime().toString(", hh:mm"));
	DataToGui();

	InstallIventFilters();

	connect(_menu, &AdContextMenu::Select, this, [this]() { SetSelected(); });
	connect(_menu, &AdContextMenu::Call, this, &AdWidget::Call);
	connect(_menu, &AdContextMenu::Protect, this, [this]() { SetState(_state); });
	connect(_menu, &AdContextMenu::Browser, this, &AdWidget::Browser);
	connect(_menu, &AdContextMenu::Save, this, &AdWidget::SaveToFile);
	connect(_menu, &AdContextMenu::Delete, this, &AdWidget::Delete);
}

void AdWidget::SetSelected()
{
	setStyleSheet(StylesManager::GetAdWidgetStyleSheets(_state, IsProtected()));
	SetState(State::kSelected);
	emit Select(_ad);
}


void AdWidget::contextMenuEvent(QContextMenuEvent* event)
{
	_menu->exec(event->globalPos());
}

void AdWidget::mouseDoubleClickEvent(QMouseEvent* event)
{
	SetSelected();
	//_ui.cost_edit->setText(QString::number(event->globalPos().x()));
}

bool AdWidget::eventFilter(QObject* watched, QEvent* event)
{
	if (event->type() == QEvent::MouseButtonDblClick)
	{
		const auto dbl_click = dynamic_cast<QMouseEvent*>(event);
		if (dbl_click->button() == Qt::LeftButton)
		{
			SetSelected();
			//_ui.cost_edit->setText(QString::number(dbl_click->globalPos().x()));
			return false;
		}
	}

	return QObject::eventFilter(watched, event);
}

void AdWidget::Call()
{
	SetSelected();
	emit CallExecuted(_ad.phone_number.toStdString());
	::Call(_ad);
}

void AdWidget::Browser() const
{
	InBrowser(_ad);
}

void AdWidget::SaveToFile()
{
	Save(_ad, this);
}

void AdWidget::SetState(const State state)
{
	_state = state;
	setStyleSheet(StylesManager::GetAdWidgetStyleSheets(_state, IsProtected()));
}

void AdWidget::DataToGui() const
{
	_ui.square_edit->setText(QString::number(_ad.square));
	_ui.cost_edit->setText(QString::number(_ad.cost));
	_ui.phone_edit->setText(_ad.phone_number);
	_ui.address_edit->setPlainText(_ad.address);
}
