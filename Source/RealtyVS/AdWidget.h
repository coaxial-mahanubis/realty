#pragma once
#include "ui_AdWidget.h"
#include "ad-data.h"
#include "AdContextMenu.h"


class AdWidget : public QWidget
{
Q_OBJECT
public:
	enum class State
	{
		kOld,
		kNew,
		kSelected,
	};

	using UPtrList = std::list<std::unique_ptr<AdWidget>>;

	explicit AdWidget(QWidget* parent = nullptr);
	explicit AdWidget(AdData ad_data, UPtrList::iterator location, State state = State::kNew,
	                  QWidget* parent = nullptr);
	void SetSelected();
	UPtrList::iterator GetIterToMe() const { return _location; }

	Q_SIGNAL void Select(const AdData&);
	Q_SLOT void Call();
	Q_SLOT void Browser() const;
	Q_SLOT void SaveToFile();
	Q_SIGNAL void Delete() const;
	Q_SIGNAL void CallExecuted(const std::string& phone) const;


	void SetState(State state);
	State GetState() const { return _state; }
	void SetProtection(const bool protect) const {_menu->SetProtectionState(protect);};
	bool IsProtected() const { return _menu->IsProtectedChecked(); }

protected:
	void contextMenuEvent(QContextMenuEvent* event) override;
	void mouseDoubleClickEvent(QMouseEvent* event) override;
	bool eventFilter(QObject* watched, QEvent* event) override;

private:
	Ui::AdWidget _ui{};
	State _state{};

	AdContextMenu* _menu{new AdContextMenu(this)};

	AdData _ad{};
	UPtrList::iterator _location{};

	void InstallIventFilters();
	void DataToGui() const;
};
