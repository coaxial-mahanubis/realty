#include "RealGui.h"
#include <QScrollBar>
#include <QTimer>
#include "StylesManager.h"
#include <QSound>
#include "filter-settings.h"


RealGui::RealGui(QWidget* parent)
	: QMainWindow(parent)
{
	_ui.setupUi(this);
	_ui.debug_button->setVisible(false);
	_ui.selected_widget->hide();
	_ad_widgets_layout->SetAddDirection(TableFlowLayout::Where::kFront);
	_ads_group_box->setObjectName("_ads_group_box");
	_ads_group_box->setLayout(_ad_widgets_layout);
	_ads_group_box->setAlignment(Qt::AlignHCenter);
	_ui.scrollArea->setWidget(_ads_group_box);

	setStyleSheet(StylesManager::GetRealGuiStyleSheets());

	// Start-Stop
	connect(_ui.start_stop_button, &QToolButton::clicked, this, [this](bool checked)
	{
		if (checked)
		emit Start();
		else
		emit Stop();
	});

	// Filter dialog
	connect(_ui.filter_button, &QToolButton::clicked, this, [this]
	{
		_filter_dialog->MoveToCenter(geometry().center());
		_filter_dialog->setVisible(true);
		_filter_dialog->show();
	});
	connect(_filter_dialog, &FilterDialog::NewFilterReady, this, &RealGui::NewFilterReady);

	// Clear all
	connect(_ui.clear_all_button, &QToolButton::clicked, this, [this]()
	{
		auto it = _ad_widgets.begin();
		while (it != _ad_widgets.end())
		{
			if ((*it)->IsProtected())
			{
				++it;
				continue;
			}

			const auto detet_it = it++;
			if ((*detet_it)->GetState() == AdWidget::State::kSelected)
			{
				_selected_ad_iter = {};
				_ui.selected_widget->hide();
			}

			_ad_widgets.erase(detet_it);
		}
	});

	// Lower - to delete
	connect(_ui.debug_button, &QPushButton::clicked, this, [this]()
	{
		static uint32_t i{};

		std::vector<AdData> datas(5);


		for (auto& data : datas)
		{
			data = AdData
			{
				kFull,
				i,
				3,
				i++,
				2,
				"http://ya.ru",
				"+79261746483",
				"asd asd das gwrgerg dvdrg afweq sdverwfsdfvv",
				"Akphevl",
				"a;sdfv aovnaovd\nadnqnaiknfasdfn\noaasfbasodfboi"
			};
		}

		AddAds(datas);
	});

	connect(_ui.selected_widget, &ActiveAdWidget::CallExecuted, this, &RealGui::CallExecuted);
}

void RealGui::AddAds(const std::vector<AdData>& ad_datas)
{
	if (ad_datas.empty())
		return;

	QSound::play(":/Sounds/Resources/notification.wav");

	for (auto& ad : _ad_widgets)
		if (ad->GetState() == AdWidget::State::kNew)
			ad->SetState(AdWidget::State::kOld);

	for (const auto& ad_data : ad_datas)
	{
		_ad_widgets.emplace_front(std::unique_ptr<AdWidget>{});
		auto& widget_uptr = _ad_widgets.front();
		widget_uptr.reset(new AdWidget(ad_data, _ad_widgets.begin()));
		_ad_widgets_layout->addWidget(widget_uptr.get());
		connect(widget_uptr.get(), &AdWidget::Select, _ui.selected_widget, &ActiveAdWidget::ResetSelected);
		connect(widget_uptr.get(), &AdWidget::Select, this, &RealGui::WidgetWantSelect);
		connect(widget_uptr.get(), &AdWidget::Delete, this, &RealGui::WidgetWantDelete);
		connect(widget_uptr.get(), &AdWidget::CallExecuted, this, &RealGui::CallExecuted);
	}

	_ad_widgets.front()->SetSelected();
}

void RealGui::WidgetWantSelect()
{
	if (const auto ad_widget = qobject_cast<AdWidget*>(sender()))
	{
		_ui.selected_widget->show();
		if (_selected_ad_iter && _selected_ad_iter.value() != ad_widget->GetIterToMe())
			(*_selected_ad_iter.value())->SetState(AdWidget::State::kOld);
		_selected_ad_iter = ad_widget->GetIterToMe();
	}
}

void RealGui::WidgetWantDelete()
{
	if (const auto ad_widget = qobject_cast<AdWidget*>(sender()))
	{
		if (ad_widget->IsProtected())
			return;

		if (ad_widget->GetState() == AdWidget::State::kSelected)
		{
			_ui.selected_widget->hide();
			_selected_ad_iter = {};
		}

		auto iter = ad_widget->GetIterToMe();
		QTimer::singleShot(40, this, [this, iter]()
		{
			_ad_widgets.erase(iter);
		});
	}
}
