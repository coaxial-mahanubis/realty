#pragma once
#include <QtWidgets/QMainWindow>
#include "ui_RealGui.h"
#include "AdWidget.h"
#include "TableFlowLayout.h"
#include <QGroupBox>
#include "ad-data.h"
#include "filter-settings.h"

class RealGui : public QMainWindow
{
Q_OBJECT

public:
	explicit RealGui(QWidget* parent = Q_NULLPTR);
	void AddAds(const std::vector<AdData>& ad_datas);

	Q_SIGNAL void Start();
	Q_SIGNAL void Stop();

	Q_SIGNAL void NewFilterReady(Filter);
	Q_SIGNAL void CallExecuted(const std::string& phone) const;


private:
	Ui::RealGuiClass _ui{};
	QGroupBox* _ads_group_box{new QGroupBox{QStringLiteral("����������"), this}};
	TableFlowLayout* _ad_widgets_layout{new TableFlowLayout(4, 2, _ads_group_box)};
	AdWidget::UPtrList _ad_widgets{};
	FilterDialog* _filter_dialog{new FilterDialog(this)};

	std::optional<AdWidget::UPtrList::iterator> _selected_ad_iter{};
	Q_SLOT void WidgetWantSelect();
	Q_SLOT void WidgetWantDelete();
};
