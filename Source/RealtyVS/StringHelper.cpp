#include "StringHelper.h"
#include "windows.h"
#include <vector>


StringHelper::StringHelper()
= default;


StringHelper::~StringHelper()
= default;

std::string StringHelper::Cp866ToCp1251(const std::string& cp866_string) {
	auto s = cp866_string;
	for (auto c = const_cast<char*>(s.c_str()); *c != 0; ++c) {
		if (*c > -129 && *c < -80)
			*c += 64;
		else if (*c > -33 && *c < -16)
			*c += 16;
	}
	return s;
}

std::string StringHelper::Cp1251ToUtf8(const char *str) {
	std::string res;
	const int result_u = MultiByteToWideChar(1251, 0, str, -1, nullptr, 0);
	if (!result_u) { return {}; }
	const auto ures = new wchar_t[result_u];
	if (!MultiByteToWideChar(1251, 0, str, -1, ures, result_u)) {
		delete[] ures;
		return {};
	}
	const auto result_c = WideCharToMultiByte(65001, 0, ures, -1, nullptr, 0, nullptr, nullptr);
	if (!result_c) {
		delete[] ures;
		return {};
	}
	const auto cres = new char[result_c];
	if (!WideCharToMultiByte(65001, 0, ures, -1, cres, result_c, nullptr, nullptr)) {
		delete[] cres;
		return {};
	}
	delete[] ures;
	res.append(cres);
	delete[] cres;
	return res;
}

bool StringHelper::IsValidUtf8(const char * string) {
	if (!string) { return true; }
	const auto* bytes = reinterpret_cast<const unsigned char *>(string);
	unsigned int cp;
	int num;
	while (*bytes != 0x00) {
		if ((*bytes & 0x80) == 0x00) {
			// U+0000 to U+007F 
			cp = (*bytes & 0x7F);
			num = 1;
		}
		else if ((*bytes & 0xE0) == 0xC0) {
			// U+0080 to U+07FF 
			cp = (*bytes & 0x1F);
			num = 2;
		}
		else if ((*bytes & 0xF0) == 0xE0) {
			// U+0800 to U+FFFF 
			cp = (*bytes & 0x0F);
			num = 3;
		}
		else if ((*bytes & 0xF8) == 0xF0) {
			// U+10000 to U+10FFFF 
			cp = (*bytes & 0x07);
			num = 4;
		}
		else { return false; }
		bytes += 1;
		for (auto i = 1; i < num; ++i) {
			if ((*bytes & 0xC0) != 0x80) { return false; }
			cp = (cp << 6) | (*bytes & 0x3F);
			bytes += 1;
		}
		if ((cp > 0x10FFFF) ||
			((cp <= 0x007F) && (num != 1)) ||
			((cp >= 0xD800) && (cp <= 0xDFFF)) ||
			((cp >= 0x0080) && (cp <= 0x07FF) && (num != 2)) ||
			((cp >= 0x0800) && (cp <= 0xFFFF) && (num != 3)) ||
			((cp >= 0x10000) && (cp <= 0x1FFFFF) && (num != 4))) {
			return false;
		}
	}
	return true;
}

std::wstring StringHelper::StringToWstring(const std::string& str) {
	std::wstring converted_string;
	int requiredSize = MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, nullptr, 0);
	if (requiredSize > 0) {
		std::vector<wchar_t> buffer(requiredSize);
		MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, &buffer[0], requiredSize);
		converted_string.assign(buffer.begin(), buffer.end() - 1);
	}
	return converted_string;
}