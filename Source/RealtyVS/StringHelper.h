#pragma once
#include <string>

class StringHelper
{
public:
	StringHelper();
	~StringHelper();
	static std::string Cp866ToCp1251(const std::string& cp866_string);
	static std::string Cp1251ToUtf8(const char* str);
	static bool IsValidUtf8(const char* string);
	static std::wstring StringToWstring(const std::string& str);
};

