#include "StylesManager.h"
#include <QFileDialog>

QString StylesManager::GetRealGuiStyleSheets()
{
	return GetInstance()._real_gui_ss;
}

QString StylesManager::GetAdWidgetStyleSheets(const AdWidget::State state, const bool protect)
{
	auto stated_ss = GetInstance()._ad_widget_ss;

	if(state == AdWidget::State::kSelected)
	{
		stated_ss = stated_ss.replace("__back__", "#252700");
		stated_ss = stated_ss.replace("__front__", "#fffccb");
		stated_ss = stated_ss.replace("__light__", "#ffff38");
		stated_ss = stated_ss.replace("__dark__", "#64643d");
		return stated_ss;
	}

	if (protect)
	{
		stated_ss = stated_ss.replace("__back__", "#18181a");
		stated_ss = stated_ss.replace("__front__", "#d1d5f1");
		stated_ss = stated_ss.replace("__light__", "#1e82e6");
		stated_ss = stated_ss.replace("__dark__", "#1a3a6d");
		return stated_ss;
	}

	if(state == AdWidget::State::kNew)
	{
		stated_ss = stated_ss.replace("__back__", "#002702");
		stated_ss = stated_ss.replace("__front__", "#e1f1e1");
		stated_ss = stated_ss.replace("__light__", "#1de621");
		stated_ss = stated_ss.replace("__dark__", "#405441");
		return stated_ss;
	}

	if(state == AdWidget::State::kOld)
	{
		stated_ss = stated_ss.replace("__back__", "#18181a");
		stated_ss = stated_ss.replace("__front__", "#f1f1f1");
		stated_ss = stated_ss.replace("__light__", "#1e82e6");
		stated_ss = stated_ss.replace("__dark__", "#2d2d30");
		return stated_ss;
	}

	return stated_ss;
}

StylesManager::StylesManager()
{
	LoadStyleSheets();
}

void StylesManager::LoadStyleSheets()
{
	LoadRealGuiStyleSheets();
	LoadAdWidgetStyleSheets();
}

void StylesManager::LoadRealGuiStyleSheets()
{
	QFile real_gui_ss_file(":/StyleSheets/Resources/RealGui.qss");
	real_gui_ss_file.open(QIODevice::ReadOnly | QIODevice::Text);
	_real_gui_ss = real_gui_ss_file.readAll();
	_real_gui_ss = _real_gui_ss.replace("__back__", "#18181a");
	_real_gui_ss = _real_gui_ss.replace("__front__", "#f1f1f1");
	_real_gui_ss = _real_gui_ss.replace("__light__", "#1e82e6");
	_real_gui_ss = _real_gui_ss.replace("__dark__", "#2d2d30");
	real_gui_ss_file.close();
}

void StylesManager::LoadAdWidgetStyleSheets()
{
	QFile ad_widget_ss_file(":/StyleSheets/Resources/AdWidget.qss");
	ad_widget_ss_file.open(QIODevice::ReadOnly | QIODevice::Text);
	_ad_widget_ss = ad_widget_ss_file.readAll();
	ad_widget_ss_file.close();
}
