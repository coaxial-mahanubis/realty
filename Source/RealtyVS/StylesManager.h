#pragma once
#include "AdWidget.h"

class StylesManager
{
public:
	StylesManager(const StylesManager&) = delete;
	StylesManager& operator=(const StylesManager&) = delete;
	StylesManager(const StylesManager&&) = delete;
	StylesManager& operator=(const StylesManager&&) = delete;

	static StylesManager& GetInstance()
	{
		static StylesManager instance;
		return instance;
	}

	static QString GetRealGuiStyleSheets();
	static QString GetAdWidgetStyleSheets(AdWidget::State state, bool protect);


private:
	StylesManager();
	~StylesManager() = default;

	void LoadStyleSheets();
	void LoadRealGuiStyleSheets();
	void LoadAdWidgetStyleSheets();
	// ss - style sheets
	QString _real_gui_ss;
	QString _ad_widget_ss;
};
