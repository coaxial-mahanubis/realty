#include "TableFlowLayout.h"

TableFlowLayout::TableFlowLayout(QWidget* parent): QLayout(parent)
{
}

TableFlowLayout::TableFlowLayout(const int margin, const int spacing, QWidget* parent)
{
	setMargin(margin);
	setSpacing(spacing);
}

TableFlowLayout::~TableFlowLayout()
{
	for (auto& item : _items)
		delete item;
	_items.clear();
}

void TableFlowLayout::addItem(QLayoutItem* item)
{
	switch (_add_direction)
	{
	case Where::kFront:
		AddFront(item);
		break;
	case Where::kBack:
		AddBack(item);
		break;
	default: ;
	}
}

Qt::Orientations TableFlowLayout::expandingDirections() const
{
	return Qt::Vertical;
}

bool TableFlowLayout::hasHeightForWidth() const
{
	return false;
}

int TableFlowLayout::count() const
{
	return _items.size();
}

QLayoutItem* TableFlowLayout::itemAt(const int index) const
{
	if (index >= 0 && index < _items.size())
		return *(std::next(_items.begin(), index));
	return nullptr;
}

QLayoutItem* TableFlowLayout::takeAt(const int index)
{
	if (index >= 0 && index < _items.size())
	{
		const auto it = std::next(_items.begin(), index);
		const auto item = *it;
		_items.erase(it);
		return item;
	}
	return nullptr;
}

QSize TableFlowLayout::minimumSize() const
{
	const auto min_width = GetMinItemWidth() * _min_columns_count + spacing() * (_min_columns_count - 1);
	const auto rows_count = GetRowsCount();
	const auto min_height = GetMinItemHeight() * rows_count + spacing() * (rows_count - 1);
	return {min_width, min_height};
}

QSize TableFlowLayout::sizeHint() const
{
	const auto width = (_items.empty() ? 100 : _items.front()->sizeHint().width()) * _columns_count +
		spacing() * (_columns_count - 1);
	const auto rows_count = GetRowsCount();
	const auto height = (_items.empty() ? 100 : _items.front()->sizeHint().height()) * rows_count +
		spacing() * (rows_count - 1);
	return {width, height};
}

void TableFlowLayout::setGeometry(const QRect& rect)
{
	QLayout::setGeometry(rect);
	if (_items.empty())
		return;

	const auto min_width = GetMinItemWidth();
	const auto min_height = GetMinItemHeight();
	_columns_count = (rect.width() + spacing()) / (min_width + spacing());
	_columns_count = std::max({_columns_count, _min_columns_count});
	const auto item_width = (rect.width() - spacing() * (_columns_count - 1)) / _columns_count;
	auto x{rect.x()};
	auto y{rect.y()};
	auto it = _items.begin();
	for (uint32_t i = 0; i < _items.size(); ++i, ++it)
	{
		if (i > 0 && i % _columns_count == 0)
		{
			x = rect.x();
			y += min_height + spacing();
		}

		auto item = *it;
		item->setGeometry({x, y, item_width, min_height});
		x += item_width + spacing();
	}
}

void TableFlowLayout::SetMinColumnCount(const int count)
{
	_min_columns_count = count;
}

void TableFlowLayout::SetAddDirection(const Where where)
{
	_add_direction = where;
}

void TableFlowLayout::AddFront(QLayoutItem* item)
{
	_items.push_front(item);
}

void TableFlowLayout::AddBack(QLayoutItem* item)
{
	_items.push_back(item);
}


int TableFlowLayout::GetMinItemWidth() const
{
	const auto it = std::max_element(_items.begin(), _items.end(), [](auto left, auto right)
	{
		return left->minimumSize().width() < right->minimumSize().width();
	});
	if (it != _items.end())
	{
		const QLayoutItem* item = *it;
		return item->minimumSize().width();
	}
	return 0;
}

int TableFlowLayout::GetMinItemHeight() const
{
	const auto it = std::max_element(_items.begin(), _items.end(), [](auto left, auto right)
	{
		return left->minimumSize().height() < right->minimumSize().height();
	});
	if (it != _items.end())
	{
		const QLayoutItem* item = *it;
		return item->minimumSize().height();
	}
	return 0;
}

int TableFlowLayout::GetRowsCount() const
{
	return _items.size() / _columns_count + ((_items.size() % _columns_count) % 1);
}
