#pragma once
#include <QLayout>

class TableFlowLayout : public QLayout
{
public:
	enum class Where
	{
		kFront,
		kBack,
	};

	explicit TableFlowLayout(QWidget* parent = nullptr);
	explicit TableFlowLayout(int margin, int spacing, QWidget* parent = nullptr);
	~TableFlowLayout();

	void addItem(QLayoutItem* item) override;
	Qt::Orientations expandingDirections() const override;
	bool hasHeightForWidth() const override;
	int count() const override;
	QLayoutItem* itemAt(int index) const override;
	QLayoutItem* takeAt(int index) override;
	QSize minimumSize() const override;
	QSize sizeHint() const override;
	void setGeometry(const QRect& rect) override;
	void SetMinColumnCount(int count);
	void SetAddDirection(Where where);

private:
	enum class SizeType
	{
		kMinimum,
		kSizeHint,
	};

	std::list<QLayoutItem*> _items{};
	Where _add_direction{Where::kBack};
	static const int kDefaultColumnsCount = 3;
	int _min_columns_count{kDefaultColumnsCount};

	void AddFront(QLayoutItem* item);
	void AddBack(QLayoutItem* item);
	int GetMinItemWidth() const;
	int GetMinItemHeight() const;
	int _columns_count{kDefaultColumnsCount};
	int GetRowsCount() const;
};

