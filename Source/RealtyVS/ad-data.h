#pragma once
#include <QString>

enum AdDataState : uint8_t
{
	kNull = 0, //���������� �� ��������
	kStep1 = 1, //�������� ����� ����������, �������������� �� �������� ������
	kStep2 = 2, //�������� ����� ����������, �������������� �� �������� ����������
	kStep3 = 3, //������� ����� �������� ��� ����� �� ���������� �������
	kFull = 4, //�������� ��, ���������� �� ������� ������ ���������
	kForRemoval = 5, //���������� ������ ���� �������
};

struct AdData
{
	AdDataState state{kNull};
	uint64_t id{ 0 };
	uint8_t room_count{ 0 };
	uint32_t square{ 0 };
	uint32_t cost{ 0 };
	QString url;
	QString phone_number;
	QString address;
	QString owner_name;
	QString description;

	QString ToString() const
	{
		QString ad_info;
		ad_info.append(
			"\n\n***********************************************************" +
			QString::fromLocal8Bit("\n������: ") + QString::number(room_count) +
			QString::fromLocal8Bit("\n�������: ") + QString::number(square) +
			QString::fromLocal8Bit("\n���������: ") + QString::number(cost) +
			QString::fromLocal8Bit("\n��������: ") + QString(description) +
			QString::fromLocal8Bit("\n���: ") + QString(owner_name) +
			QString::fromLocal8Bit("\n�������: ") + QString(phone_number) +
			QString::fromLocal8Bit("\n�����: ") + QString(address) +
			QString::fromLocal8Bit("\n����: ") + QString(url));
		return ad_info;
	}
};