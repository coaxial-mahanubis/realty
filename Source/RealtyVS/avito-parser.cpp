﻿#include "avito-parser.h"
#include <QString>
#include <QRegExp>
#include <vector>
#include <nlohmann_json/json.hpp>

void AvitoParser::ParseStep1(const QByteArray& reply_data, const quint32 max_count)
{
	checkFullAds();
	const auto reply_string = reply_data.toStdString(); //QString::fromUtf8(reply_data).toLocal8Bit().constData();
	const auto json_text_initial_data = SearchFirstGroupMatch(reply_string, R"RAW(window\.__initialData__ = (\{.*\}) \|\|)RAW");
	if (json_text_initial_data.empty()) return;
	auto json_initial_data = nlohmann::json::parse(json_text_initial_data);
	auto json_articles = json_initial_data["/search/items"_json_pointer];
	Logger::Log(std::to_string(json_articles.size()) + " ads are received from Avito", this);
	json_articles.erase(std::remove_if(json_articles.begin(), json_articles.end(), [](auto& article)
	{
		//data-item-premium="1"
		return article["/type"_json_pointer] == "vip" || article["/value/services/0"_json_pointer] == "highlight";
	}), json_articles.end());
	for (auto i = 0; i < (max_count< json_articles.size()?max_count: json_articles.size()); i++)
	{
		AdData ad_data;
		//url
		ad_data.url = QString::fromStdString("https://m.avito.ru" + json_articles[i]["/value/uri_mweb"_json_pointer].get<std::string>());
		if (itAdded(ad_data.url)) continue;
		//id
		ad_data.id = GetNumberAds();
		//room_count
		auto title = json_articles[i]["/value/title"_json_pointer].get<std::string>();
		title = QString::fromStdString(title).toLocal8Bit().toStdString();
		auto match = SearchFirstGroupMatch(title, R"(([\d]+)-к)");
		if (!match.empty()) ad_data.room_count = std::stoi(match);
		//square
		match = SearchFirstGroupMatch(title, R"(([\d]+(.[\d]+)?) м)");
		if (!match.empty()) ad_data.square = std::stoi(match);
		//cost
		match = SearchAll(json_articles[i]["/value/price"_json_pointer].get<std::string>(), R"([\d\s]*)").front();
		match.erase(std::remove(match.begin(), match.end(), ' '), match.end());
		if (!match.empty()) ad_data.cost = std::stoi(match);
		//phone_number
		//не отображается на странице поиска
		//address
		ad_data.address = QString::fromStdString(json_articles[i]["/value/address"_json_pointer]);
		//owner_name
		//не отображается на странице поиска
		//description
		//не отображается на странице поиска
		ad_data.state = AdDataState::kStep1;
		AddAdData(ad_data);
		Logger::Log("For url " + ad_data.url.toStdString()
			+ "\n\tid: " + std::to_string(ad_data.id)
			+ "\n\troom_count: " + std::to_string(ad_data.room_count)
			+ "\n\tsquare: " + std::to_string(ad_data.square)
			+ "\n\tcost: " + std::to_string(ad_data.cost)
			+ "\n\taddress: " + ad_data.address.toStdString()
			, this);
	}
}

void AvitoParser::ParseStep2(const QString url, const QByteArray& reply_data)
{
	std::vector<AdData> ads = GetAds();
	const std::string reply_string = reply_data.toStdString(); 
	const std::string json_text_initial_data = SearchFirstGroupMatch(reply_string, R"RAW(window\.__initialData__ = (\{.*\}) \|\|)RAW");
	auto json_initial_data = nlohmann::json::parse(json_text_initial_data);
	auto json_article = json_initial_data["/item/item"_json_pointer];
	for (auto& ad_data: ads)
	{
		if (ad_data.url == url)
		{
			//phone_number
			std::string match = SearchFirstGroupMatch(reply_string, R"RAW(href="tel:(.*?)")RAW");
			ad_data.phone_number = QString::fromLocal8Bit(match.c_str());
			ad_data.phone_number = QString::fromStdString(ConvertPhoneToCommonFormat(ad_data.phone_number.toStdString()));
			//owner_name
			ad_data.owner_name = QString::fromStdString(json_article["/seller/name"_json_pointer]);
			//description
			ad_data.description = QString::fromStdString(json_article["/description"_json_pointer]);
			ad_data.state = AdDataState::kStep3;
			Logger::Log("For url " + ad_data.url.toStdString()
				+ "\n\towner_name: " + ad_data.owner_name.toStdString()
				+ "\n\tdescription: " + ad_data.description.toStdString()
				+ "\n\tphone_number: " + ad_data.phone_number.toStdString()
				, this);
		}
	}
	SetAds(ads);
}

void AvitoParser::ParseStep3(const QString url, const QByteArray& reply_data)
{

}
