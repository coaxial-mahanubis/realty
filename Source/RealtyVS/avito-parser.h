#pragma once
#include "parser.h"


class AvitoParser : public Parser
{
public:
	void ParseStep1(const QByteArray& reply_data, const quint32 max_count) override;
	void ParseStep2(QString url, const QByteArray& reply_data) override; 
	void ParseStep3(QString url, const QByteArray& reply_data) override; 
};
