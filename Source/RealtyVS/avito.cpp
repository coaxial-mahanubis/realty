#include "avito.h"
#include "avito-parser.h"
#include <sstream>
#include <QString>

Avito::Avito(const Filter* filter): RealtySite(filter)
{
	SetParser(new AvitoParser);
	LoadPatternFromServer("Avito");
	LoadPatternFromFile("AvitoStep2Request.xml");
	LoadPatternFromFile("AvitoStep3Request.xml");
}

void Avito::ApplyStep1RequestProperties(Request& request, const RequestPattern& pattern) const
{
	pattern.AddUrlPart(request, "city", QString::number(GetFilter()->city).toStdString());
	pattern.AddUrlPart(request, "category", QString::number(GetFilter()->category).toStdString());
	pattern.AddUrlPart(request, "type", QString::number(GetFilter()->type).toStdString());
	pattern.AddUrlPart(request, "term", "");
	if (!(GetFilter()->category == Filter::kRoom && GetFilter()->type == Filter::kGet))
	{
		std::vector<unsigned char> selected_items;
		for (unsigned char i = 0; i < 11; i++)
		{
			if (GetFilter()->rooms[i])
				selected_items.emplace_back(i);
		}
		if (selected_items.size() == 1)
		{
			pattern.AddUrlPart(request, "rooms[" + QString::number(selected_items.front()).toStdString() + "]", "1");
		}
		else
			if (selected_items.size() > 1)
			{
				std::vector<std::string> items;
				std::vector<std::string> values;
				items.emplace_back("rooms[array]");
				values.emplace_back("1");
				for (auto& item : selected_items)
				{
					items.emplace_back("rooms[" + QString::number(item).toStdString() + "_in_array]");
					values.emplace_back("1");
				}
				pattern.AddParameter(request, items, values);
			}
	}
	//url.replace(QRegExp(R"(/$)"), "");
	if (!GetFilter()->search_text.isEmpty()) pattern.AddParameter(request, "search_text", GetFilter()->search_text.toStdString());
	if (GetFilter()->max_price != 0)
	{
		pattern.AddParameter(request, "min_price", QString::number(GetFilter()->min_price).toStdString());
		pattern.AddParameter(request, "max_price", QString::number(GetFilter()->max_price).toStdString());
	}
	if (GetFilter()->only_names)
	{
		pattern.AddParameter(request, "only_names", "1");
	}
	if (GetFilter()->only_photo)
	{
		pattern.AddParameter(request, "only_photo", "1");
	}
	request.AddHeader("Referer", request.Address());
}

void Avito::ApplyStep3RequestProperties(Request& request, AdData& ad) const
{
	request.AddHeader("Referer", ad.url);
}

void Avito::ApplyStep2RequestProperties(Request& request, const std::string& url) const
{
	request.SetAddress(QString::fromStdString(url));
}