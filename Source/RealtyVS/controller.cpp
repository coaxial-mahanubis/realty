#include "controller.h"
#include <QMessageBox>
#include <QInputDialog>

Controller::Controller()
	:
	Logable("Controller", "Main thread")
{
	switch(protector_.CheckRightWork())
	{
	case Protector::UserState::kWorking:
		break;
	case Protector::UserState::kNotPaid:
		QMessageBox::warning(nullptr, QString::fromLocal8Bit("��������� �� ����� ���������� ������"),
			QString::fromLocal8Bit("��� ���������� �������� ���������. ����� ����� ������ �� ������ ") + QString::number(protector_.GetUserId()) + QString::fromLocal8Bit(". �������� ��� ��� ������."));
		exit(0);
	case Protector::UserState::kBlocked:
		QMessageBox::warning(nullptr, QString::fromLocal8Bit("��������� �� ����� ���������� ������"),
			QString::fromLocal8Bit("�� �������������."));
		exit(0);
	case Protector::UserState::kDestroyed:
		QMessageBox::warning(nullptr, QString::fromLocal8Bit("��������� �� ����� ���������� ������"),
			QString::fromLocal8Bit("��������� �������������� ������."));
		exit(0);
	case Protector::UserState::kJustCreated:
		QMessageBox::warning(nullptr, QString::fromLocal8Bit("��������� �� ����� ���������� ������"),
			QString::fromLocal8Bit("��� ���������� �������� ���������. ����� ����� ������ �� ������ ")+QString::number(protector_.GetUserId())+ QString::fromLocal8Bit(". �������� ��� ��� ������."));
		exit(0);
	case Protector::UserState::kUsesOutdateVersion:
		QMessageBox::warning(nullptr, QString::fromLocal8Bit("��������� �� ����� ���������� ������"),
			QString::fromLocal8Bit("�� ����������� ������������ ������ ���������, ����������, �������� ���������. "));
		exit(0);
	case Protector::UserState::kError:
	default:
		QMessageBox::warning(nullptr, QString::fromLocal8Bit("��������� �� ����� ���������� ������"),
			QString::fromLocal8Bit("��������� �������������� ������, ����������, �������� � ��� �������������. "));
		exit(0);	
	}

	requester_ = new Requester(&filter_); //������ ���� ��������� ����� �������� ����������� ����� ��������

	qRegisterMetaType<AdData>("AdData");
	qRegisterMetaType<std::vector<AdData>>("std::vector<AdData>");

	Logger::Log("Showing main window", this);

	connect(requester_, &Requester::GotData, this, &Controller::ReceiveData);
	connect(requester_, &Requester::GotAdsNumber, &protector_, &Protector::SendReportAboutAds, Qt::QueuedConnection);

	connect(&ads_gui_, &RealGui::Start, [this]()
	{
		connect(this, &Controller::NeedData, requester_, &Requester::RequestData, Qt::QueuedConnection);
		emit NeedData();
	});
	connect(&ads_gui_, &RealGui::Stop, [this]()
	{
		disconnect(this, &Controller::NeedData, requester_, &Requester::RequestData);
	});

	connect(&ads_gui_, &RealGui::NewFilterReady, this, [this](const Filter filter) { filter_ = filter; });
	connect(&ads_gui_, &RealGui::CallExecuted, &protector_, &Protector::SendReportAboutCall);

	requests_thread_ = new QThread(this);
	Logger::Log("Move requester to another thread.", this);
	requester_->moveToThread(requests_thread_);
	requests_thread_->start();

	ads_gui_.show();

	PhoneController::Create();
}

Controller::~Controller()
{
	PhoneController::Destroy();
	requests_thread_->terminate();
	requests_thread_->wait();
	delete requests_thread_;
}

void Controller::ReceiveData(std::vector<AdData> reply_data)
{
	Logger::Log(std::to_string(reply_data.size()) + " ads are received", this);

	if (!reply_data.empty())
	{
		ads_gui_.AddAds(reply_data);
	}
	emit NeedData();
}
