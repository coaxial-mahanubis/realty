#pragma once
#include <QThread>
#include <QTimer>
#include <logger.h>
#include "requester.h"
#include "phone-controller.h"
#include "protector.h"
#include "RealGui.h"

class Controller : public QObject, Logable
{
	Q_OBJECT

	static const uint32_t kRequestsInterval = 2000; // ����������� ����� ���������

	QThread* requests_thread_; // ����� ��� �������� ��������
	Requester* requester_; // ���������� � ����� � ��� �������
	Filter filter_; // ������, ������� ����� ����������� � ��������
	RealGui ads_gui_; // ����������� ���������
	Protector protector_; // ������ ���������
public:
	Controller();
	~Controller();

private slots:
	void ReceiveData(std::vector<AdData> reply_data); // ������: Requester::GotData

signals :
	void NeedData();
};
