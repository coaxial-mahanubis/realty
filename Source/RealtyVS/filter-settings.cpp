#include "filter-settings.h"
#include <QShortcut>
#include <QToolButton>
#include "StylesManager.h"


FilterDialog::FilterDialog(QWidget* parent)
	: QDialog(parent)
{
	_ui.setupUi(this);
	setAttribute(Qt::WA_TranslucentBackground, true);
	setWindowFlag(Qt::FramelessWindowHint, true);
	setWindowModality(Qt::ApplicationModal);

	for (uint32_t i = 0; i < Filter::kMaxRoomsCount; i++)
	{
		auto list_button = new QToolButton;
		list_button->setMaximumSize({32, 24});
		list_button->setMinimumSize({32, 24});
		list_button->setCheckable(true);
		list_button->setChecked(true);
		list_button->setText(QString::number(i));

		if(i == 0)
			list_button->setText("C");

		if(i == Filter::kMaxRoomsCount - 1)
			list_button->setText(">9");

		_ui.rooms_count_layout->addWidget(list_button, 0, i, Qt::AlignCenter);
	}

	connect(_ui.accept_button, &QPushButton::clicked, this, &FilterDialog::NewSettingsAccepted);
	connect(_ui.accept_button, &QPushButton::clicked, this, &FilterDialog::hide);
	connect(_ui.cancel_button, &QPushButton::clicked, this, &FilterDialog::hide);

	new QShortcut(QKeySequence(Qt::Key_Escape), _ui.cancel_button, SLOT(click()));
	new QShortcut(QKeySequence(Qt::Key_Return), _ui.accept_button, SLOT(click()));
}

FilterDialog::~FilterDialog()
= default;

void FilterDialog::MoveToCenter(const QPoint center)
{
	auto temp_geometry = geometry();
	const auto new_top_left_pos = center - QPoint(size().width() / 2, size().height() / 2);
	temp_geometry.moveTopLeft(new_top_left_pos);
	setGeometry(temp_geometry);
}

void FilterDialog::SetSettings(const Filter& filter) const
{
	_ui.category_combo->setCurrentIndex(filter.category);
	_ui.city_combo->setCurrentIndex(filter.city);
	_ui.type_combo->setCurrentIndex(filter.type);
	_ui.min_price_spin->setValue(filter.min_price);
	_ui.max_price_spin->setValue(filter.max_price);

	for (auto i = 0; i < Filter::kMaxRoomsCount; i++)
	{
		dynamic_cast<QToolButton*>(_ui.rooms_count_layout->itemAt(i)->widget())->setChecked(filter.rooms[i]);
	}

	_ui.description_search_button->setChecked(!filter.only_names);
	_ui.only_photo_button->setChecked(filter.only_photo);
	_ui.search_text->setText(filter.search_text);
}

void FilterDialog::NewSettingsAccepted()
{
	Filter filter;
	filter.category = static_cast<Filter::Category>(_ui.category_combo->currentIndex());
	filter.city = static_cast<Filter::City>(_ui.city_combo->currentIndex());
	filter.type = static_cast<Filter::AdType>(_ui.type_combo->currentIndex());
	filter.min_price = _ui.min_price_spin->value();
	filter.max_price = _ui.max_price_spin->value();

	for (auto i = 0; i < Filter::kMaxRoomsCount; i++)
	{
		filter.rooms[i] = dynamic_cast<QToolButton*>(_ui.rooms_count_layout->itemAt(i)->widget())->isChecked();
	}

	filter.only_names = !_ui.description_search_button->isChecked();
	filter.only_photo = _ui.only_photo_button->isChecked();
	filter.search_text = _ui.search_text->toPlainText();

	emit NewFilterReady(filter);
}
