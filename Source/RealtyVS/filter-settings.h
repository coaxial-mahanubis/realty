#pragma once
#include <QDialog>
#include "ui_filter-settings.h"
#include "filter.h"

class FilterDialog : public QDialog
{
	Q_OBJECT

public:
	explicit FilterDialog(QWidget *parent = Q_NULLPTR);
	~FilterDialog();

	void MoveToCenter(QPoint center);
	
	void SetSettings(const Filter& filter) const;
	
	Q_SIGNAL void NewFilterReady(Filter filter);
private:
	Q_SLOT void NewSettingsAccepted();

	Ui::FilterSettings _ui{};
};
