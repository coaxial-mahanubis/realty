#pragma once
#include <QString>

struct Filter
{
	static const auto kMaxRoomsCount = 11;

	enum Category : uint8_t
	{
		kApartment = 0,
		kRoom = 1,
	};

	enum City : uint8_t
	{
		kMoscow = 0,
		kOblast = 1,
		kSaintPetersburg = 2,
		kEkaterinburg = 3,
		kNovosibirsk = 4
	};

	enum AdType : uint8_t
	{
		kGet = 0,
		kRent = 1,
	};

	Category category{ kApartment };
    QString search_text;
	bool only_names{ false };
	City city{ kMoscow };
    bool only_photo{ true };
	AdType type{ kRent };
    bool rooms[kMaxRoomsCount]{};
    quint32 min_price{ 0 };
    quint32 max_price{ 0 };
	quint32 max_count{ 5 };
};
