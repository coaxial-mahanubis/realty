#include <QApplication>
#include <QIcon>
#include "controller.h"


int main(int argc, char *argv[])
{
	Logger::ClearLogsFolder(4);
    QApplication a(argc, argv);
	Controller controller;

    return a.exec();
}
