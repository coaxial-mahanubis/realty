#include "parser.h"
#include <regex>
#include <cctype>

Parser::Parser() :
	Logable("Parser", "Thread of receiving and data processing")
{
}

bool Parser::itAdded(const std::string& url) const
{
	for(auto& ad: ads_)
	{
		if (ad.url.toStdString() == url) return true;
	}
	return false;
}

bool Parser::itAdded(const QString& url) const
{
	for (auto& ad : ads_)
	{
		if (ad.url == url) return true;
	}
	return false;
}

void Parser::AddAdData(AdData& ad_data)
{
	if (!itAdded(ad_data.url))
		ads_.emplace_back(ad_data);
}

unsigned Parser::GetNumberAds() const
{
	return ads_.size();
}

std::vector<std::string> Parser::GetUrls() const
{
	std::vector<std::string> urls;
	for (auto& ad: ads_)
	{
		urls.emplace_back(ad.url.toStdString());
	}
	return urls;
}

std::vector<std::string> Parser::GetUrlsWithState(const AdDataState state) const
{
	std::vector<std::string> urls;
	for (auto& ad : ads_)
	{
		if (ad.state == state)
			urls.emplace_back(ad.url.toStdString());
	}
	return urls;
}

std::vector<std::string> Parser::GetPhones() const
{
	std::vector<std::string> phones;
	for (auto& ad : ads_)
	{
		phones.emplace_back(ad.phone_number.toStdString());
	}
	return phones;
}

std::vector<AdData> Parser::GetAds() const
{
	return ads_;
}

std::vector<AdData> Parser::GetAdsWithState(const AdDataState state) const
{
	std::vector<AdData> ads;
	for (auto& ad : ads_)
	{
		if (ad.state == state)
			ads.emplace_back(ad);
	}
	return ads;
}

std::vector<AdData> Parser::GetAdsWithUrls(const std::vector<std::string> urls) const
{
	std::vector<AdData> ads;
	for (auto& ad : ads_)
	{
		for (auto& url: urls)
		{
			if (ad.url.toStdString() == url)
			{
				ads.emplace_back(ad);
				break;
			}
		}
		
	}
	return ads;
}

void Parser::checkFullAds()
{
	for (auto& ad : ads_)
	{
		if (ad.state == AdDataState::kStep3)
			ad.state = AdDataState::kFull;
	}
}

void Parser::SetAds(const std::vector<AdData>& ad_datas)
{
	ads_ = ad_datas;
}

std::vector<std::string> Parser::SearchAll(std::string str, const std::string& pattern)
{
	std::vector<std::string> results;

	const std::regex regexp(pattern);
	const auto results_begin =
		std::sregex_iterator(str.begin(), str.end(), regexp);
	const auto results_end = std::sregex_iterator();

	for (std::sregex_iterator i = results_begin; i != results_end; ++i) {
		results.emplace_back(i->str());
	}
	return results;
}

/*std::string Parser::SearchFullMatch(std::string str, const std::string pattern)
{
	std::cmatch match;
	const std::regex regexp(pattern);
	std::regex_search(str.c_str(), match, regexp);
	return match[0].str();
}*/

std::string Parser::SearchFirstGroupMatch(const std::string& str, const std::string& pattern)
{
	std::cmatch match;
	const std::regex regexp(pattern);
	if (!std::regex_search(str.c_str(), match, regexp))
	{
		return std::string();
	}
	if (match.size()>1)
	{
		return match[1].str();
	}
	return match[0].str();
}

std::string Parser::SearchGroupMatch(const std::string& str, const std::string& pattern, const int numGroup)
{
	std::cmatch match;
	const std::regex regexp(pattern);
	if (!std::regex_search(str.c_str(), match, regexp))
	{
		return std::string();
	}
	if (match.size()>numGroup)
	{
		return match[numGroup].str();
	}
	return match[0].str();
}


std::string Parser::ConvertPhoneToCommonFormat(const std::string& phone)
{
	std::string result;
	for (auto& ch : phone)
	{
		if (std::isdigit(static_cast<unsigned char>(ch)))
		{
			result += ch;
		}
	}
	if (result.empty())
	{
		return phone;
	}
	//��������� ������� � �����
	auto i = 0;
	if (result.size() > 7)
	{
		i = 1;
		while (i < result.size() && i < 9)
		{
			result.insert(result.begin() + i, ' ');
			i += 4;
		}
	}
	else
	{
		i = 3;
	}
	while (i < result.size())
	{
		result.insert(result.begin() + i, ' ');
		i += 3;
	}
	if (result.front() != '8')
	{
		result = '+' + result;
	}
	return result;
}
