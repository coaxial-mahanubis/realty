#pragma once
#include "ad-data.h"
#include  <vector>
#include <QRegExp>
#include <logger.h>

class Parser: public Logable
{
public:
	Parser();
	virtual ~Parser() = default;
	//�������� ������� ����������� ����� �������, �.�. ��������� ����� ��������� ���������� ���������� ��� ������ ������. 
	/**
	 * \brief ������� ������ �������� � ������������ ������, ��������� � ������ ����������.
	 * \param reply_data ������ ���� QByteArray � ������� �� ������.
	 * \param max_count ������������ ���������� ���������� ����������.
	 */
	virtual void ParseStep1(const QByteArray& reply_data, const quint32 max_count) = 0; 
	/**
	* \brief ������� ������ �������� � �����������, ��������� � ������ ����������.
	* \param url ������ �� ����������, ������ � �������� ��������������, ���� �������� �� ���������� ����.
	* \param reply_data ������ ���� QByteArray � ������� �� ������.
	*/
	virtual void ParseStep2(const QString url, const QByteArray& reply_data) = 0; 
	/**
	* \brief ������� ������ ����� �� ajax ������, �������� ����������� ������, ������� �� ���� �������� �� ���������� ���� �����.
	* \brief ��� ����� - ������ ����� ��������. ��� ��� - ����� ��������.
	* \param url ������ �� ����������, ������ � �������� ��������������, ���� �������� �� ���������� ����.
	* \param reply_data ������ ���� QByteArray � ������� �� ������.
	*/
	virtual void ParseStep3(const QString url, const QByteArray& reply_data) = 0; 
	std::vector<std::string> GetUrls() const;
	std::vector<std::string> GetUrlsWithState(const AdDataState state) const;
	std::vector<std::string> GetPhones() const;
	std::vector<AdData> GetAds() const;
	std::vector<AdData> GetAdsWithState(const AdDataState state) const;
	std::vector<AdData> GetAdsWithUrls(const std::vector<std::string> urls) const;
	void checkFullAds();

	void SetAds(const std::vector<AdData>& ad_datas);
	static std::vector<std::string> SearchAll(std::string str, const std::string& pattern);
	//static std::string SearchFullMatch(std::string str, const std::string pattern);
protected:
	void AddAdData(AdData& ad_data);
	bool itAdded(const std::string& url) const;
	bool itAdded(const QString& url) const;
	unsigned GetNumberAds() const;
	static std::string SearchFirstGroupMatch(const std::string& str, const std::string& pattern);
	static std::string SearchGroupMatch(const std::string& str, const std::string& pattern, const int numGroup);
	static std::string ConvertPhoneToCommonFormat(const std::string& phone);
private:
	std::vector<AdData> ads_;
};
