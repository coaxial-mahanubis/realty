#include "phone-controller.h"
#include <QEventLoop>


std::unique_ptr<PhoneController> PhoneController::phone_controller_;
std::mutex PhoneController::phone_controller_creation_mutex_;

PhoneController::PhoneController(): Logable("PhoneControl", "Main thread"), 
								adb_tracker_("./adb/adb.exe", this)
{
	adb_tracker_.start(); //����� �������� �� ������������� ������������
	connect(&adb_tracker_, &AdbTracker::sigDone, this, &PhoneController::TrackerDone);
	connect(&adb_tracker_, &AdbTracker::sigAdbError, this, &PhoneController::TrackerError);
	connect(&adb_tracker_, &AdbTracker::sigDevListChanged, this, &PhoneController::TrackerDevListChanged);
}


PhoneController::~PhoneController()
= default;

void PhoneController::Create()
{
	TryCreateSingleton();
}

void PhoneController::Destroy()
{
	if (phone_controller_ != nullptr)
	{
		disconnect(&phone_controller_->adb_tracker_, &AdbTracker::sigDone, &*phone_controller_, &PhoneController::TrackerDone);
		disconnect(&phone_controller_->adb_tracker_, &AdbTracker::sigAdbError, &*phone_controller_, &PhoneController::TrackerError);
		disconnect(&phone_controller_->adb_tracker_, &AdbTracker::sigDevListChanged, &*phone_controller_, &PhoneController::TrackerDevListChanged);
		phone_controller_->adb_tracker_.stop();
		QEventLoop loop;
		connect(&phone_controller_->adb_tracker_, &AdbTracker::finished, &loop, &QEventLoop::quit);
		loop.exec();
		phone_controller_.release();
	}
}

void PhoneController::Call(const std::string& number)
{
	TryCreateSingleton();
	phone_controller_->StartIntentCall(number);
}

void PhoneController::TryCreateSingleton()
{
	phone_controller_creation_mutex_.lock();
	if (phone_controller_ == nullptr)
	{
		phone_controller_.reset(new PhoneController);
	}
	phone_controller_creation_mutex_.unlock();
}

void PhoneController::StartIntentCall(const std::string& number)
{
	const auto result = adb_client_.adb_cmd("shell:am start -a android.intent.action.CALL -d tel:\"" + QString::fromStdString(number)+"\"");
	Logger::Log(("Adb returned the message: " + QString::fromLocal8Bit(result)).toStdString(), this);
}

void PhoneController::TrackerDone(QObject *p) const
{
	Logger::Log(QString("Adb a tracker is successfully started.").toStdString(), this);
}

void PhoneController::TrackerError(const QString& hint, const int pid) const
{
	Logger::Log("Tracker adb error. PID: " + std::to_string(pid) + "\nMessage: " + hint.toStdString(), this);
}

void PhoneController::TrackerDevListChanged() const
{
	Logger::Log(QString("The list of the connected devices is changed. ").toStdString(), this);
}
