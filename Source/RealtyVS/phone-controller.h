#pragma once
#include <QObject>
#include <logger.h>
#include "adbtracker.h"
#include "adbclient.h"

class PhoneController : public QObject, Logable
{
	Q_OBJECT
private:
	PhoneController();
	PhoneController(PhoneController&) = delete;
	PhoneController& operator=(PhoneController&) = delete;
	static std::unique_ptr<PhoneController> phone_controller_; // Singleton
	static std::mutex phone_controller_creation_mutex_;
	AdbTracker adb_tracker_;
	AdbClient adb_client_;

	static void TryCreateSingleton();
	void StartIntentCall(const std::string& number);

private slots:
	void TrackerDone(QObject *p) const;
	void TrackerError(const QString& hint, int pid) const;
	void TrackerDevListChanged() const;

public:
	~PhoneController();
	static void Create();
	static void Destroy();

public slots:
	static void Call(const std::string& number);
};

