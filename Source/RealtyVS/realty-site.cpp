#include "realty-site.h"
#include <ctime>
#include <string>


RealtySite::RealtySite(const Filter* filter):
parser_(nullptr),
filter_(filter)
{
}

Request RealtySite::GetStep1Request() const
{
	Request request = pattern_step1_request_.GetDefaultRequest(); 
	ApplyStep1RequestProperties(request, pattern_step1_request_);
	return request;
}

Request RealtySite::GetStep2Request(const std::string& url) const
{
	Request request = pattern_step2_request_.GetDefaultRequest();
	ApplyStep2RequestProperties(request, url);
	return request;
}

Request RealtySite::GetStep3Request(AdData& ad) const
{
	Request request = pattern_step3_request_.GetDefaultRequest();
	ApplyStep3RequestProperties(request, ad);
	return request;
}

void RealtySite::SetParser(Parser* parser)
{
	parser_ = parser;
}

const Filter* RealtySite::GetFilter() const
{
	return filter_;
}

void RealtySite::LoadPatternFromFile(const std::string& filename)
{
	RequestPattern pattern;
	pattern.LoadPatternFromFile(filename);
	if (pattern.GetName() == "step1")
		pattern_step1_request_ = pattern;
	if (pattern.GetName() == "step2")
		pattern_step2_request_ = pattern;
	if (pattern.GetName() == "step3")
		pattern_step3_request_ = pattern;
}

void RealtySite::LoadPatternFromServer(const std::string& sitename)
{
	RequestPattern pattern;
	pattern.LoadPatternFromServer(sitename);
	if (pattern.GetName() == "step1")
		pattern_step1_request_ = pattern;
	if (pattern.GetName() == "step2")
		pattern_step2_request_ = pattern;
	if (pattern.GetName() == "step3")
		pattern_step3_request_ = pattern;
}

Parser* RealtySite::GetParser() const
{
	return parser_;
}

quint32 RealtySite::GetMaxCount() const
{
	return filter_->max_count;
}
