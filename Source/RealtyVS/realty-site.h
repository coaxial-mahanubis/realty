#pragma once
#include "request.h"
#include "parser.h"
#include "filter.h"
#include "request-pattern.h"

class RealtySite
{
	Parser* parser_;
	const Filter* filter_;

public:
	explicit RealtySite(const Filter* filter);
	virtual ~RealtySite() = default;
	Request GetStep1Request() const; // ���������� ������ � ����������� ��������
	Request GetStep2Request(const std::string& url) const;
	Request GetStep3Request(AdData& ad) const;
	RequestPattern pattern_step1_request_;
	RequestPattern pattern_step2_request_;
	RequestPattern pattern_step3_request_;
	Parser* GetParser() const;
	quint32 GetMaxCount() const;
protected:
	
	/**
	 * \brief ��������� � ������� ���������, ����������� ��� ������� ����� ��������� ���������� (��������� ������).
	 * \param request ����������� ������.
	 * \param pattern ������, �� �������� ����������� ������.
	 */
	virtual void ApplyStep1RequestProperties(Request& request, const RequestPattern& pattern) const = 0;
	/**
	 * \brief ��������� � ������� ���������, ����������� ��� ������� ����� ��������� ���������� (�������� ����������).
	 * \param request ����������� ������.
	 * \param url ������ �� ���������� (������������ ��� �������������).
	 */
	virtual void ApplyStep2RequestProperties(Request& request, const std::string& url) const = 0;
	/**
	 * \brief ��������� � ������� ���������, ����������� ��� �������� ����� ��������� ���������� (����� �������� ��� ����� � ����� ��� ���).
	 * \param request ����������� ������.
	 * \param ad ��������� � ����������� �� ����������.
	 */
	virtual void ApplyStep3RequestProperties(Request& request, AdData& ad) const = 0;
	
	void SetParser(Parser* parser);
	

	const Filter* GetFilter() const; // ��� Set �.�. ������� � ������� ������

	void LoadPatternFromFile(const std::string& filename); // �������� ������� ������� �� �����.
	void LoadPatternFromServer(const std::string& sitename);
};
