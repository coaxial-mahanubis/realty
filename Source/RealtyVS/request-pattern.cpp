#include "request-pattern.h"
#include "tinyxml2.h"
#include <ctime>
#include <fstream>
#include <time.h>
#include "protector.h"

std::vector<std::string> RequestPattern::user_agents;

RequestPattern::RequestPattern()
{
	if (RequestPattern::user_agents.empty())
	{
		std::ifstream file("Requests\\User-Agent.txt");
		if (file.is_open())
		{
			std::string line;
			while (std::getline(file, line))
			{
				RequestPattern::user_agents.push_back(line);
			}
		}
		else
		{
			throw std::runtime_error("The User-Agent.txt file can't be open.");
		}
		std::srand(time(nullptr));
	}
}


RequestPattern::~RequestPattern()
= default;


void RequestPattern::LoadPattern(tinyxml2::XMLDocument& doc)
{
	// Structure of the XML file:
// - request (name, type)
// - - url
// - - parts_url
// - - - part
// - - - - name_in_filter
// - - - - value
// - - - - - value_in_filter
// - - - - - value_in_request
// - - - - ...
// - - - ...
// - - headers
// - - - header
// - - - - name
// - - - - value
// - - - ...
// - - parameters
// - - - parameter
// - - - - name_in_filter
// - - - - name_in_request
// - - - - method
// - - - - value
// - - - - - value_in_filter
// - - - - - value_in_request
// - - - - ...
// - - - ...

	tinyxml2::XMLElement *tmp_item;
	char* tmp_string;

	// ��������� ������ ������� ��������� (Notebook)
	tinyxml2::XMLElement *request_item = doc.FirstChildElement("request");
	if (request_item == nullptr) return;
	name_ = request_item->Attribute("name");
	const std::string type = request_item->Attribute("type");
	if (type == "get")
		type_ = Request::Type::kGet;
	else
		if (type == "post")
			type_ = Request::Type::kPost;
		else
			type_ = Request::Type::kUnnecessary;
	url_ = request_item->FirstChildElement("url")->GetText();


	tinyxml2::XMLElement *parts_url_item = request_item->FirstChildElement("parts_url");
	if (parts_url_item != nullptr)
	{

		for (tinyxml2::XMLElement *part_url_item = parts_url_item->FirstChildElement("part");
			part_url_item != nullptr;
			part_url_item = part_url_item->NextSiblingElement("part"))
		{
			parts_of_url_.emplace_back();

			tmp_item = part_url_item->FirstChildElement("name_in_filter");
			tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
			parts_of_url_.back().name_in_filter = tmp_string == nullptr ? "" : tmp_string;

			for (tinyxml2::XMLElement *value_item = part_url_item->FirstChildElement("value");
				value_item != nullptr;
				value_item = value_item->NextSiblingElement("value"))
			{
				parts_of_url_.back().values.emplace_back();

				tmp_item = value_item->FirstChildElement("value_in_filter");
				tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
				parts_of_url_.back().values.back().in_filter = tmp_string == nullptr ? "" : tmp_string;

				tmp_item = value_item->FirstChildElement("value_in_request");
				tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
				parts_of_url_.back().values.back().in_request = tmp_string == nullptr ? "" : tmp_string;

			}
		}
	}
	tinyxml2::XMLElement *headers_item = request_item->FirstChildElement("headers");
	if (headers_item != nullptr)
	{
		for (tinyxml2::XMLElement *header_item = headers_item->FirstChildElement("header");
			header_item != nullptr;
			header_item = header_item->NextSiblingElement("header"))
		{
			headers_.emplace_back();
			tmp_item = header_item->FirstChildElement("name_in_filter");
			tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
			headers_.back().name.in_filter = tmp_string == nullptr ? "" : tmp_string;
			tmp_item = header_item->FirstChildElement("name_in_request");
			tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
			headers_.back().name.in_request = tmp_string == nullptr ? "" : tmp_string;
			for (tinyxml2::XMLElement *value_item = header_item->FirstChildElement("value");
				value_item != nullptr;
				value_item = value_item->NextSiblingElement("value"))
			{
				headers_.back().values.emplace_back();
				tmp_item = value_item->FirstChildElement("value_in_filter");
				tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
				headers_.back().values.back().in_filter = tmp_string == nullptr ? "" : tmp_string;
				tmp_item = value_item->FirstChildElement("value_in_request");
				tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
				headers_.back().values.back().in_request = tmp_string == nullptr ? "" : tmp_string;
			}
		}
	}
	tinyxml2::XMLElement *parameters_item = request_item->FirstChildElement("parameters");
	if (parameters_item != nullptr)
	{
		for (tinyxml2::XMLElement *parameter_item = parameters_item->FirstChildElement("parameter");
			parameter_item != nullptr;
			parameter_item = parameter_item->NextSiblingElement("parameter"))
		{
			parameters_.emplace_back();
			tmp_item = parameter_item->FirstChildElement("name_in_filter");
			tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
			parameters_.back().name.in_filter = tmp_string == nullptr ? "" : tmp_string;
			tmp_item = parameter_item->FirstChildElement("name_in_request");
			tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
			parameters_.back().name.in_request = tmp_string == nullptr ? "" : tmp_string;
			tmp_item = parameter_item->FirstChildElement("multiplier");
			tmp_string = tmp_item == nullptr ? "1" : tmp_item->GetText();
			parameters_.back().multiplier = tmp_string == nullptr ? 1 : std::stoi(tmp_string);
			for (tinyxml2::XMLElement *value_item = parameter_item->FirstChildElement("value");
				value_item != nullptr;
				value_item = value_item->NextSiblingElement("value"))
			{
				parameters_.back().values.emplace_back();
				tmp_item = value_item->FirstChildElement("value_in_filter");
				tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
				parameters_.back().values.back().in_filter = tmp_string == nullptr ? "" : tmp_string;
				tmp_item = value_item->FirstChildElement("value_in_request");
				tmp_string = tmp_item == nullptr ? "" : tmp_item->GetText();
				parameters_.back().values.back().in_request = tmp_string == nullptr ? "" : tmp_string;
			}
		}
	}
}

void RequestPattern::LoadPatternFromServer(const std::string& sitename)
{
	tinyxml2::XMLDocument doc;
	auto pattern_xml = Protector::GetPattern(sitename);
	doc.Parse(pattern_xml.c_str(), pattern_xml.size());
	if (doc.Error())
		throw std::runtime_error("Incorrect pattern " + sitename + ".");

	LoadPattern(doc);
}

void RequestPattern::LoadPatternFromFile(const std::string& filename)
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile(("Requests\\" + filename).c_str());
	if (doc.Error())
		throw std::runtime_error("Incorrect file "+filename+".");

	LoadPattern(doc);
}

Request RequestPattern::GetDefaultRequest() const
{
	Request request;
	request.SetType(type_);
	request.SetAddress(QString::fromStdString(url_));
	/*for (auto& header: headers_)
	{
		request.AddHeader(QString::fromStdString(header.name), QString::fromStdString(header.value));
	}*/
	for (auto& header : headers_)
	{
		if (header.name.in_filter.empty())
			request.AddHeader(QString::fromStdString(header.name.in_request), QString::fromStdString(header.values.front().in_request));
	}
	request.AddHeader("User-Agent", QString::fromStdString(user_agents[std::rand() % user_agents.size()]));
	for (auto& parameter : parameters_)
	{
		if (parameter.name.in_filter.empty())
			request.AddParam(QString::fromStdString(parameter.name.in_request), QString::fromStdString(parameter.values.front().in_request));
	}
	return request;
}


std::string RequestPattern::GetName() const
{
	return name_;
}

void RequestPattern::AddParameter(Request& request, const std::string& name_in_filter, const std::string&
                                  value_in_filter) const
{
	for (auto& parameter : parameters_)
	{
		if (parameter.name.in_filter == name_in_filter)
		{
			if (parameter.values.empty())
			{
				QString val = QString::fromStdString(value_in_filter);
				if (parameter.multiplier!=1)
				{
					val = QString::number(std::stoi(value_in_filter) * parameter.multiplier);
				}
				request.AddParam(QString::fromStdString(parameter.name.in_request), val);
				return;
			}
			for (auto& value : parameter.values)
			{
				if (value.in_filter == value_in_filter)
				{
					QString val = QString::fromStdString(value.in_request);
					if (parameter.multiplier != 1)
					{
						val = QString::number(std::stoi(value.in_request) * parameter.multiplier);
					}
					request.AddParam(QString::fromStdString(parameter.name.in_request), val);
					return;
				}
			}
		}
	}
}

void RequestPattern::AddHeader(Request& request, const std::string& name_in_filter, const std::string& value_in_filter) const
{
	for (auto& header : headers_)
	{
		if (header.name.in_filter == name_in_filter)
		{
			if (header.values.empty())
			{
				request.AddHeader(QString::fromStdString(header.name.in_request), QString::fromStdString(value_in_filter));
				return;
			}
			for (auto& value : header.values)
			{
				if (value.in_filter == value_in_filter)
				{
					request.AddHeader(QString::fromStdString(header.name.in_request), QString::fromStdString(value.in_request));
					return;
				}
			}
		}
	}
}

void RequestPattern::AddParameter(Request& request, const std::vector<std::string>& names_in_filter, const std::vector<std::string>
                                  & values_in_filter) const
{
	if (names_in_filter.size() != values_in_filter.size())
		throw std::runtime_error("The quantity of names of parameters and amount of values does not match.");
	std::vector<std::string> names_in_request;
	std::vector<std::string> values_in_request;
	for (auto i = 0; i < names_in_filter.size(); ++i) //��� ������� ����� ��������� � �������
	{
		for (auto& parameter : parameters_) //��������� �� ���� ��������� ���������� �������
		{
			if (parameter.name.in_filter == names_in_filter[i]) //���� ���, ������� ������������� ������� ����� ��������� � �������
			{
				/*if (parameter.values.size() == 1)
				{
				}*/
				for (auto& value : parameter.values) //��������� ��������� ��������
				{
					if (value.in_filter == values_in_filter[i]) //���� ������������
					{
						//���������� �������� �������� � ������
						auto successfull = false;
						for (auto j = 0; j < names_in_request.size(); ++j) //��� ������� ����� ��������� � �������
						{
							if (names_in_request[j] == parameter.name.in_request)
							{
								values_in_request[j] += value.in_request;
								successfull = true;
								break;
							}
						}
						if (!successfull)
						{
							names_in_request.emplace_back(parameter.name.in_request);
							values_in_request.emplace_back(value.in_request);
						}
						//��������
						break;
					}
				}
				break;
			}
		}
	}
	for (auto i = 0; i < names_in_request.size(); ++i) //��� ������� ����� ��������� � �������
	{
		request.AddParam(QString::fromStdString(names_in_request[i]), QString::fromStdString(values_in_request[i]));
	}
}

void RequestPattern::AddUrlPart(Request& request, const std::string& name_in_filter, const std::string& value_in_filter) const
{
	for (auto& part : parts_of_url_)
	{
		if (part.name_in_filter == name_in_filter)
		{
			if (part.values.size() == 1)
			{
				request.SetAddress(request.Address() + "/" + QString::fromStdString(part.values[0].in_request));
				return;
			}
			for (auto& value : part.values)
			{
				if (value.in_filter == value_in_filter)
				{
					request.SetAddress(request.Address() + "/" + QString::fromStdString(value.in_request));
					return;
				}
			}
		}
	}
}

void RequestPattern::AddConcatenatedUrlPart(Request& request, const std::vector<std::string>& names_in_filter, const std::vector<std::string>& values_in_filter, const std::string& separator, const std::string& postfix) const
{
	std::string url_part;
	for (auto i = 0; i < names_in_filter.size(); ++i)
	{
		for (auto& part : parts_of_url_)
		{
			if (part.name_in_filter == names_in_filter[i])
			{
				if (part.values.size() == 1)
				{
					url_part += part.values[0].in_request + separator;
					continue;
				}
				for (auto& value : part.values)
				{
					if (value.in_filter == values_in_filter[i])
					{
						url_part += value.in_request + separator;
					}
				}
			}
		}
	}
	if (!url_part.empty())
	{
		url_part = url_part.substr(0, url_part.length() - 1);
		request.SetAddress(request.Address() + "/" + QString::fromStdString(url_part + postfix));
	}
}

bool RequestPattern::IsNecessary() const
{
	return !(type_ == Request::Type::kUnnecessary);
}
