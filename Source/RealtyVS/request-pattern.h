#pragma once
#include <string>
#include <vector>
#include "request.h"
#include "tinyxml2.h"

class RequestPattern
{
public:
	RequestPattern();
	~RequestPattern();
	void LoadPatternFromServer(const std::string& sitename);
	void LoadPatternFromFile(const std::string& filename); // �������� ������� ������� �� �����.
	Request GetDefaultRequest() const;
	std::string GetName() const;
	void AddParameter(Request& request, const std::string& name_in_filter, const std::string& value_in_filter) const;
	void AddHeader(Request& request, const std::string& name_in_filter, const std::string& value_in_filter) const;
	void AddParameter(Request& request, const std::vector<std::string>& names_in_filter,
	                  const std::vector<std::string>& values_in_filter) const;
	void AddUrlPart(Request& request, const std::string& name_in_filter, const std::string& value_in_filter) const;
	void AddConcatenatedUrlPart(Request & request, const std::vector<std::string>& names_in_filter, const std::vector<std::string>& values_in_filter, const std::string& separator, const std::string & postfix) const;
	bool IsNecessary() const;
private:
	struct Property
	{
		std::string in_filter;
		std::string in_request;
	};
	struct PartOfUrl
	{
		std::string name_in_filter;
		std::vector<Property> values;
	};
	struct Header
	{
		Property name;
		std::vector<Property> values;
	};
	struct Parameter
	{
		Property name;
		std::vector<Property> values;
		int multiplier{ 1 };
	};
	
	std::string name_;
	std::string url_;
	Request::Type type_{ Request::Type::kGet };
	std::vector<PartOfUrl> parts_of_url_;
	std::vector<Header> headers_;
	std::vector<Parameter> parameters_;
	static std::vector<std::string> user_agents;

	void LoadPattern(tinyxml2::XMLDocument& doc);
};

