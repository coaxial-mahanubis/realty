#include "request-sender.h"
#include <QTimer>
#include <QEventLoop>
#include <QThread>
#include <QNetworkProxy>
#include <QNetworkReply>

//#define NETWORK_SHOW_SEND_REQUESTS

RequestSender::RequestSender(const qint64 maxWaitTime /*= 35000*/):
	Logable("RequestSender", "Thread of receiving and data processing"),
	 add_headings_to_answer_(true)
{
	SetMaxWaitTime(maxWaitTime);
	error_ = NoError;
	//cookie_ = new QNetworkCookieJar;cookie_(nullptr),
}

RequestSender::~RequestSender()
= default;

void RequestSender::SetProxy(const QNetworkProxy& proxy)
{
	proxy_ = proxy;
}

QByteArray RequestSender::SendRequest(Request& request)
{
	reprocess_ = 0;
	switch (request.GetType())
	{
	case Request::Type::kGet:
		return SendRequest(request, true);
	case Request::Type::kPost:
		return SendRequest(request, false);
	default:
		return QByteArray();
	}
}

/*QByteArray RequestSender::Get(Request& request)
{
	return SendRequest(request, true);
}

QByteArray RequestSender::Post(Request& request)
{
	return SendRequest(request, false);
}*/

QByteArray RequestSender::GetWhileSuccess(Request& request, const int maxCount /*= 2*/)
{
	return SendWhileSuccess(request, maxCount, true);
}

QByteArray RequestSender::PostWhileSuccess(Request& request, const int maxCount /*= 2*/)
{
	return SendWhileSuccess(request, maxCount, false);
}

void RequestSender::SetMaxWaitTime(const qint64 max)
{
	max_wait_time_ = max;
}

qint64 RequestSender::MaxWaitTime() const
{
	return max_wait_time_;
}

RequestSender::RequestError RequestSender::Error() const
{
	return error_;
}

bool RequestSender::IsAddHeadingsToAnswer() const
{
	return add_headings_to_answer_;
}

void RequestSender::SetAddHeadingsToAnswer(const bool add_headings_to_answer)
{
	add_headings_to_answer_ = add_headings_to_answer;
}

QByteArray RequestSender::SendRequest(Request& request, const bool getRequest)
{
	QSharedPointer<QNetworkAccessManager> manager(new QNetworkAccessManager);
	manager->setProxy(proxy_);
	//if (cookie_ != nullptr) 
	//manager->setCookieJar(cookie_);


	QNetworkReply* reply = getRequest ? manager->get(request.GetRequest()) :
		manager->post(request.GetRequest(false), request.Data());

#if defined(NETWORK_SHOW_SEND_REQUESTS)
	if (getRequest)
		qDebug() << "[GET] " << request.GetRequest().url().toString();
	else
		qDebug() << "[POST]" << request.GetRequest(false).url().toString() << request.Data();
#endif

	QTimer timer;
	timer.setInterval(max_wait_time_);
	timer.setSingleShot(true);
	QObject::connect(&timer, &QTimer::timeout, reply, &QNetworkReply::abort);
	QEventLoop loop;
	QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
	timer.start();
	loop.exec();

	QByteArray data;

	if (reply->isFinished() && reply->error() == QNetworkReply::NoError)
	{
		QString location = reply->header(QNetworkRequest::LocationHeader).toString();
		if (!location.isEmpty() && reprocess_ < 2)
		{
			Logger::Log("Do a re-query, because the server returned the location header: " + location.toStdString(), this);
			++reprocess_;
			if (location[0]=='/')
			{
				QUrl url = request.Url();
				const QString site = url.host();
				const auto new_address = site + location;
				request.SetAddress(new_address);
			}
			else
			{
				request.SetAddress(location);
			}
			data = SendRequest(request, getRequest);
		}
		else
		{
			data = reply->readAll();
			Logger::Log(QString::number(data.size()).toStdString() +" bytes of data received", this);
			if (add_headings_to_answer_)
			{
				data += "\n\n";
				QList<QByteArray> raw_headers = reply->rawHeaderList();
				for (auto& raw_header : raw_headers)
				{
					data += "\n\n" + QString::fromLocal8Bit(raw_header) + ": " + QString::fromLocal8Bit(reply->rawHeader(raw_header));
				}
			}
			error_ = RequestSender::NoError;
		}
	}
	else
	{
		Logger::Log("The server returned an error: " + reply->errorString().toStdString(), this);
		error_ = RequestSender::HasError;
		data += reply->errorString();
	}

	reply->deleteLater();

#if defined(NETWORK_SHOW_SEND_REQUESTS)
	qDebug() << "[ANSWER]" << data;
#endif

	//cookie_ = manager->cookieJar();


	return data;
}

QByteArray RequestSender::SendWhileSuccess(Request& request, const int maxCount /*= 2*/, const bool getRequest /*= true*/)
{
	if (maxCount < 0)
		throw QString(__LINE__ + " " __FILE__);

	int c = 0;
	QByteArray answer;

	while (c < maxCount)
	{
		c++;
		answer = SendRequest(request);//getRequest ? Get(request) : Post(request);

		if (Error() == NoError)
			break;

		Logger::Log("Error sending request. Error code: " + QString::number(static_cast<int>(Error())).toStdString() + ". Resend the request after 2 seconds. \n", this);
		QThread::currentThread()->msleep(2000);
	}

	return answer;
}
