#ifndef REQUESTSENDER_H
#define REQUESTSENDER_H

#include <QObject>
#include "request.h"
#include <QNetworkCookieJar>
#include <logger.h>
#include <QNetworkProxy>

class RequestSender : public QObject, public Logable
{
    Q_OBJECT
public:

    enum RequestError
    {
        NoError,
        HasError
    };

	explicit RequestSender(qint64 maxWaitTime = 35000);
    ~RequestSender();

    void SetProxy(const QNetworkProxy& proxy);

	QByteArray SendRequest(Request& request);
    /*QByteArray Get(Request& request);
    QByteArray Post(Request& request);*/
    QByteArray GetWhileSuccess(Request& request, int maxCount = 2);
    QByteArray PostWhileSuccess(Request& request, int maxCount = 2);

    void SetMaxWaitTime(qint64 max);

    qint64 MaxWaitTime() const;
    RequestError Error() const;


	bool IsAddHeadingsToAnswer() const;
	void SetAddHeadingsToAnswer(const bool add_headings_to_answer);
private:
    QByteArray SendRequest(Request& request, const bool getRequest);
    QByteArray SendWhileSuccess(Request& request, int maxCount = 2, bool getRequest = true);

    qint64 max_wait_time_{};
    RequestError error_;
    QNetworkProxy proxy_;
	//QNetworkCookieJar* cookie_;
	bool add_headings_to_answer_;

	uint8_t reprocess_{ 0 };
};

#endif // REQUESTSENDER_H
