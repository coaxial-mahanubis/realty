#include "request.h"
#include <QString>
#include <QUrlQuery>
#include <QNetworkRequest>

Request::Request(const QString& address /*= QString()*/): Logable("Request", "Thread of receiving and data processing")
{
	SetAddress(address);
}

Request::Request(const Request& other)
	: Logable("Request", "Thread of receiving and data processing"),
	address_(other.address_),
	params_(other.params_),
	headers_(other.headers_),
	type_(other.type_)
{
}

Request::Request(Request&& other) noexcept
	: Logable("Request", "Thread of receiving and data processing"),
	address_(std::move(other.address_)),
	params_(std::move(other.params_)),
	headers_(std::move(other.headers_)),
	type_(other.type_)
{
}

Request& Request::operator=(const Request& other)
{
	if (this == &other)
		return *this;
	address_ = other.address_;
	params_ = other.params_;
	headers_ = other.headers_;
	type_ = other.type_;
	return *this;
}

Request& Request::operator=(Request&& other) noexcept
{
	if (this == &other)
		return *this;
	address_ = std::move(other.address_);
	params_ = std::move(other.params_);
	headers_ = std::move(other.headers_);
	type_ = other.type_;
	return *this;
}

QString Request::Address() const
{
	return address_;
}

void Request::SetAddress(QString address)
{
	for (const QPair<QString, QString>& value : QUrlQuery(QUrl(address)).queryItems())
		AddParam(value.first, value.second);
	address_ = address;
}

void Request::AddParam(QString name, QVariant value)
{
	params_[name] = value.toString();
}

bool Request::RemoveParam(QString name)
{
	if (!params_.contains(name))
		return false;
	params_.remove(name);
	return true;
}

QStringList Request::ParamsNames() const
{
	return params_.keys();
}

QMap<QString, QString> Request::Params() const
{
	return params_;
}

void Request::AddHeader(QString name, QVariant value)
{
	headers_[name] = value.toString();
}

bool Request::RemoveHeader(QString name)
{
	if (!headers_.contains(name))
		return false;
	headers_.remove(name);
	return true;
}

QStringList Request::HeadersNames() const
{
	return headers_.keys();
}

QMap<QString, QString> Request::Headers() const
{
	return headers_;
}

QUrl Request::Url(const bool for_get_request /*= true*/) const
{
	QUrl url(Address());
	if (for_get_request)
		url.setQuery(Data());
	const auto url_string = url.toString().toStdString();
	Logger::Log("Send request to: " + url_string, this);
	return url;
}

QNetworkRequest Request::GetRequest(const bool for_get_request /*= true*/) const
{
	//QString url = Url(for_get_request).toString();
	QNetworkRequest r(Url(for_get_request));

	//r.setHeader(QNetworkRequest::UserAgentHeader, "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0");
	//r.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	//r.setRawHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");

	if (!for_get_request)
	{
		r.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
	}

	foreach(QString headerName, HeadersNames())
	{
		r.setRawHeader(headerName.toLocal8Bit(), headers_[headerName].toLocal8Bit());
	}

	return r;
}

QByteArray Request::Data() const
{
	auto b = params_.begin();
	const auto e = params_.end();

	QByteArray byte_array_data;

	while (b != e)
	{
		byte_array_data.append(b.key());
		if(!b.value().isEmpty())
		{
			byte_array_data.append('=');
			byte_array_data.append(b.value());
		}
		byte_array_data.append('&');

		++b;
	}

	byte_array_data.chop(1);

	return byte_array_data;
}

Request::Type Request::GetType() const
{
	return type_;
}

void Request::SetType(const Type type)
{
	type_ = type;
}
