#ifndef REQUEST_H
#define REQUEST_H

#include <QObject>
#include <QNetworkReply>
#include <QNetworkProxy>
#include <QUrlQuery>
#include <logger.h>

class Request : public QObject, public Logable
{
	Q_OBJECT

public:
	enum Type : unsigned char
	{
		kGet = 0,
		kPost = 1,
		kUnnecessary = 2
	};

    explicit Request(const QString& address = QString());
	Request(const Request& other);
	Request(Request&& other) noexcept;
	Request& operator=(const Request& other);
	Request& operator=(Request&& other) noexcept;


	QString Address() const;
    void SetAddress(QString address);

    void AddParam(QString name, QVariant value);
    bool RemoveParam(QString name);

    QStringList ParamsNames() const;
    QMap<QString, QString> Params() const;

    void AddHeader(QString name, QVariant value);
    bool RemoveHeader(QString name);

    QStringList HeadersNames() const;
    QMap<QString, QString> Headers() const;

    QUrl Url(bool for_get_request = true) const;
    QNetworkRequest GetRequest(bool for_get_request = true) const;
    QByteArray Data() const;

	Type GetType() const;
	void SetType(const Type type);
private:
    QString address_;
    QMap<QString, QString> params_;
    QMap<QString, QString> headers_;
	Type type_{ kGet };
};

#endif // REQUEST_H
