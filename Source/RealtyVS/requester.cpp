#include "requester.h"
#include "avito.h"
#include "youla.h"
#include <vector>
#include <QString>
#include "yandex.h"

Requester::Requester(const Filter* filter) :
	Logable("Requester", "Thread of receiving and data processing")
{
	sites_.emplace_back(new Avito(filter));
	sites_.emplace_back(new Youla(filter));
	sites_.emplace_back(new Yandex(filter));
}

void Requester::RequestData()
{
	std::vector<AdData> ads;
	for(auto& site: sites_)
	{
		auto request(site->GetStep1Request());
		auto reply_data = request_sender_.SendRequest(request);
		site->GetParser()->ParseStep1(reply_data, site->GetMaxCount());
		std::vector<std::string> urls = site->GetParser()->GetUrlsWithState(AdDataState::kStep1);
		for (auto& url: urls)
		{
			request = site->GetStep2Request(url);
			reply_data = request_sender_.SendRequest(request);
			site->GetParser()->ParseStep2(QString(url.c_str()), reply_data);
		}

		std::vector<AdData> site_ads = site->GetParser()->GetAdsWithState(AdDataState::kStep2);
		for (auto& site_ad : site_ads)
		{
			request = site->GetStep3Request(site_ad);
			reply_data = request_sender_.SendRequest(request);
			site->GetParser()->ParseStep3(site_ad.url, reply_data);
		}

		site_ads=site->GetParser()->GetAdsWithState(AdDataState::kStep3);
		ads.insert(ads.end(), site_ads.begin(), site_ads.end());
		Logger::Log(std::to_string(site_ads.size()) + " ads received", this);
	}
	Logger::Log(std::to_string(ads.size()) + " ads in total it is received", this);
	std::reverse(ads.begin(), ads.end());
	emit GotData(ads);
	emit GotAdsNumber(ads.size());
}
