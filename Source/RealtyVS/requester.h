#pragma once
#include <vector>
#include "request-sender.h"
#include "realty-site.h"
#include <memory>
#include <logger.h>
#include <QObject>

class Requester : public QObject, Logable
{
	Q_OBJECT

	using RealtySiteUPtr = std::unique_ptr<RealtySite>;

	std::vector<RealtySiteUPtr> sites_;
	RequestSender request_sender_;

public:
	explicit Requester(const Filter* filter);

public slots:
	void RequestData(); // ������: Controller::NeedData

signals:
	void GotData(std::vector<AdData>);
	void GotAdsNumber(uint8_t ads_number);
};
