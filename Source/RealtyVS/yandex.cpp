#include "yandex.h"
#include "yandex-parser.h"


Yandex::Yandex(const Filter* filter) : RealtySite(filter)
{
	SetParser(new YandexParser);
	LoadPatternFromServer("Yandex");
	LoadPatternFromFile("YandexStep2Request.xml");
	LoadPatternFromFile("YandexStep3Request.xml");
}

void Yandex::ApplyStep1RequestProperties(Request& request, const RequestPattern& pattern) const
{
	pattern.AddUrlPart(request, "city", QString::number(GetFilter()->city).toStdString());
	pattern.AddUrlPart(request, "type", QString::number(GetFilter()->type).toStdString());
	pattern.AddUrlPart(request, "category", QString::number(GetFilter()->category).toStdString());
	if (!(GetFilter()->category == Filter::kRoom && GetFilter()->type == Filter::kGet))
	{
		std::vector<unsigned char> selected_items;
		for (unsigned char i = 0; i < 11; i++)
		{
			if (GetFilter()->rooms[i])
				selected_items.emplace_back(i);
		}
		if (selected_items.size() == 1)
		{
			pattern.AddUrlPart(request, "rooms[" + QString::number(selected_items.front()).toStdString() + "]", "1");
		}
		else
			if (selected_items.size() > 1)
			{
				std::vector<std::string> items;
				std::vector<std::string> values;
				for (auto& item : selected_items)
				{
					items.emplace_back("rooms[" + QString::number(item).toStdString() + "_in_array]");
					values.emplace_back("1");
					if (item >= 4)
					{
						break;
					}
				}
				pattern.AddConcatenatedUrlPart(request, items, values, ",", "-komnatnie");
			}
	}
	if (!GetFilter()->search_text.isEmpty()) pattern.AddParameter(request, "search_text", GetFilter()->search_text.toStdString());
	if (GetFilter()->min_price != 0)
	{
		pattern.AddParameter(request, "min_price", QString::number(GetFilter()->min_price).toStdString());
	}
	if (GetFilter()->max_price != 0)
	{
		pattern.AddParameter(request, "max_price", QString::number(GetFilter()->max_price).toStdString());
	}
	request.AddHeader("Referer", request.Address());
}

void Yandex::ApplyStep3RequestProperties(Request& request, AdData& ad) const
{
	request.AddHeader("Referer", ad.url);
}

void Yandex::ApplyStep2RequestProperties(Request& request, const std::string& url) const
{
	request.SetAddress(QString::fromStdString(url));
}