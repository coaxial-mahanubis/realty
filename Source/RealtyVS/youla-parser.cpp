#include "youla-parser.h"
#include <string>
#include <vector>
#include <QRegExp>
#include <nlohmann_json/json.hpp>
#include <complex>
#include "filter.h"

void YoulaParser::ParseStep1(const QByteArray& reply_data, const quint32 max_count)
{
	checkFullAds();
	const std::string reply_string = QString::fromUtf8(reply_data).toLocal8Bit().constData();
	std::vector<std::string> articles = SearchAll(reply_string, R"RAW(<li\s+class="product_item[\s\S]*?<\/li>)RAW");
	Logger::Log(std::to_string(articles.size()) + " ads are received from Youla", this);
	for (auto i = 0; i < (max_count<articles.size() ? max_count : articles.size()); i++)
	{
		AdData ad_data;
		//url
		std::string match = SearchFirstGroupMatch(articles[i], R"RAW(<a\s+href="(.*?)")RAW");
		ad_data.url = "https://youla.ru" + QString::fromStdString(match);
		if (itAdded(ad_data.url)) continue;
		//id
		ad_data.id = GetNumberAds();
		//room_count
		match = SearchFirstGroupMatch(articles[i], R"RAW((\d{1,2}) ������)RAW");
		if (!match.empty()) ad_data.room_count = std::stoi(match);
		//square
		match = SearchFirstGroupMatch(articles[i], R"RAW(([\d]+(.[\d]+)?) �)RAW");
		if (!match.empty()) ad_data.square = std::stoi(match);
		//cost
		//match = SearchGroupMatch(articles[i], R"RAW((\&quot\;|\")price(\&quot\;|\"):(\d+)})RAW", 3);
		match = SearchFirstGroupMatch(articles[i], R"RAW(&quot;price&quot;:(\d+)\})RAW");
		match.erase(std::remove(match.begin(), match.end(), ' '), match.end());
		if (!match.empty()) ad_data.cost = std::stoi(match)/100;
		//phone_number
		//�� ������������ �� �������� ������
		//address
		//�� ������������ �� �������� ������
		//owner_name
		//�� ������������ �� �������� ������
		//description
		//�� ������������ �� �������� ������
		ad_data.state = AdDataState::kStep1;
		AddAdData(ad_data);
		Logger::Log("For url " + ad_data.url.toStdString()
			+ "\n\tid: " + std::to_string(ad_data.id)
			+ "\n\troom_count: " + std::to_string(ad_data.room_count)
			+ "\n\tsquare: " + std::to_string(ad_data.square)
			+ "\n\tcost: " + std::to_string(ad_data.cost)
			, this);
	}
}

void YoulaParser::ParseStep2(const QString url, const QByteArray& reply_data)
{
	std::vector<AdData> ads = GetAds();
	const std::string reply_string = reply_data.toStdString();
	const std::string json_text_initial_data = SearchFirstGroupMatch(reply_string, R"RAW(window\.__YOULA_STATE__ = (\{.*\});)RAW");
	nlohmann::json json_initial_data;
	try
	{
		json_initial_data = nlohmann::json::parse(json_text_initial_data);
	}
	catch (...)
	{
		for (auto& ad_data : ads)
		{
			if (ad_data.url == url)
			{
				ad_data.state = AdDataState::kForRemoval;
				return;
			}
		}
	}
	auto json_article = json_initial_data["/entities/products/0"_json_pointer];
	for (auto& ad_data : ads)
	{
		if (ad_data.url == url)
		{
			try {
				//phone_number
				ad_data.phone_number = QString::fromStdString(ConvertPhoneToCommonFormat(json_article["/owner/displayPhoneNum"_json_pointer].get<std::string>()));
				//address
				ad_data.address = QString::fromStdString(json_article["/location/description"_json_pointer].get<std::string>());
				//owner_name
				ad_data.owner_name = QString::fromStdString(json_article["/owner/name"_json_pointer].get<std::string>());
				//description
				ad_data.description = QString::fromStdString(json_article["/description"_json_pointer].get<std::string>());
				ad_data.state = AdDataState::kStep3;
			}
			catch(...)
			{
				ad_data.state = AdDataState::kForRemoval;
			}


			Logger::Log("For url " + ad_data.url.toStdString()
				+ "\n\towner_name: " + ad_data.owner_name.toStdString()
				+ "\n\tdescription: " + ad_data.description.toStdString()
				+ "\n\taddress: " + ad_data.address.toStdString()
				+ "\n\tphone_number: " + ad_data.phone_number.toStdString()
				, this);
		}
	}
	SetAds(ads);
}

void YoulaParser::ParseStep3(const QString url, const QByteArray& reply_data)
{

}
