#pragma once
#include "parser.h"
#include <QByteArray>

class YoulaParser : public Parser
{
	void ParseStep1(const QByteArray& reply_data, const quint32 max_count) override;
	void ParseStep2(const QString url, const QByteArray& reply_data) override;
	void ParseStep3(const QString url, const QByteArray& reply_data) override;
};
