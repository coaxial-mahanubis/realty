#include "youla.h"
#include "youla-parser.h"
#include <QString>

Youla::Youla(const Filter* filter): RealtySite(filter)
{
	SetParser(new YoulaParser);
	LoadPatternFromServer("Youla");
	LoadPatternFromFile("YoulaStep2Request.xml");
	LoadPatternFromFile("YoulaStep3Request.xml");
}

void Youla::ApplyStep1RequestProperties(Request& request, const RequestPattern& pattern) const
{
	pattern.AddUrlPart(request, "city", QString::number(GetFilter()->city).toStdString());
	pattern.AddHeader(request, "city", QString::number(GetFilter()->city).toStdString());
	pattern.AddUrlPart(request, "category", QString::number(GetFilter()->category).toStdString());
	pattern.AddParameter(request, "term", "");

	std::vector<unsigned char> selected_items;
	for (unsigned char i = 0; i < 11; i++)
	{
		if (GetFilter()->rooms[i]) pattern.AddParameter(request, "rooms[" + QString::number(i).toStdString() + "]", "1");
	}

	//url.replace(QRegExp(R"(/$)"), "");
	if (!GetFilter()->search_text.isEmpty()) pattern.AddParameter(request, "search_text", GetFilter()->search_text.toStdString());
	if (GetFilter()->max_price != 0)
	{
		pattern.AddParameter(request, "min_price", QString::number(GetFilter()->min_price).toStdString());
		pattern.AddParameter(request, "max_price", QString::number(GetFilter()->max_price).toStdString());
	}
	request.AddHeader("Referer", request.Address());
}

void Youla::ApplyStep3RequestProperties(Request& request, AdData& ad) const
{
	request.AddHeader("Referer", ad.url);
}

void Youla::ApplyStep2RequestProperties(Request& request, const std::string& url) const
{
	request.SetAddress(QString::fromStdString(url));
}


