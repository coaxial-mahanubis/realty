#pragma once
#pragma once
#include "realty-site.h"

class Youla : public RealtySite
{
public:
	Youla(const Filter* filter);
	void ApplyStep1RequestProperties(Request& request, const RequestPattern& pattern) const override;
	void ApplyStep3RequestProperties(Request& request, AdData& ad) const override;
	void ApplyStep2RequestProperties(Request& request, const std::string& url) const override;
};